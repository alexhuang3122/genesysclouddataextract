﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic.FileIO;
using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Extensions;
using PureCloudPlatform.Client.V2.Model;

namespace GenesysCloudDataExtract
{
    class ConversationsDetails
    {

        static void Main(string[] args)
        {
            ApiTest();
            AppSettingsSection appSettings;

            // Get alternate Configuration filepath
            string altConfigFile = "";
            if (Environment.GetCommandLineArgs().Length > 1) altConfigFile = Environment.GetCommandLineArgs()[1];
            if ((altConfigFile.Length > 0) && (File.Exists(altConfigFile)))
            {
                ExeConfigurationFileMap configFile = new ExeConfigurationFileMap();
                configFile.ExeConfigFilename = Path.Combine(Environment.CurrentDirectory, altConfigFile);
                System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFile, ConfigurationUserLevel.None);
                appSettings = (AppSettingsSection)config.AppSettings;
            }
            else if ((altConfigFile.Length > 0) && !(File.Exists(altConfigFile)))
            {
                throw (new Exception("Specified Configuration file does not exist!"));
            }
            else appSettings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings;


            string strLogFileName = (appSettings.Settings["logfilename"] != null)? (appSettings.Settings["logfilename"].Value) : "";
            FileStream filestream;
            StreamWriter streamwriter;

            if (!((strLogFileName is null) || (strLogFileName.Length < 1)))
            {
                strLogFileName += "_" + DateTime.Now.ToString("s").Replace(":", "").Replace("-", "") + ".log";
                filestream = new FileStream(strLogFileName, FileMode.Create);
                streamwriter = new StreamWriter(filestream);
                streamwriter.AutoFlush = true;
                Console.SetOut(streamwriter);
                Console.SetError(streamwriter);
            }
            else
            {
                streamwriter = null;
            }


            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("Starting Serco GC Data Extract - " + DateTime.Now.ToString("s"));
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Getting Config Options...");
            string region = (appSettings.Settings["region"] != null) ? (appSettings.Settings["region"].Value) : "";
            Console.WriteLine("region: " + region);
            string clientId = (appSettings.Settings["clientId"] != null) ? (appSettings.Settings["clientId"].Value) : "";
            //Console.WriteLine("clientId: " + clientId);
            string clientSecret = (appSettings.Settings["clientSecret"] != null) ? (appSettings.Settings["clientSecret"].Value) : "";
            //Console.WriteLine("clientSecret: " + clientSecret);
            string intervalFilter = (appSettings.Settings["intervalFilter"] != null) ? (appSettings.Settings["intervalFilter"].Value) : "";
            Console.WriteLine("intervalFilter: " + intervalFilter);
            string dbconnstring = (appSettings.Settings["dbconnstring"] != null) ? (appSettings.Settings["dbconnstring"].Value) : "";
            //Console.WriteLine("dbconnstring: " + dbconnstring);
            string useConvJobId = (appSettings.Settings["useConvJobId"] != null) ? (appSettings.Settings["useConvJobId"].Value) : "";
            Console.WriteLine("useConvJobId: " + useConvJobId);
            string useUserJobId = (appSettings.Settings["useUserJobId"] != null) ? (appSettings.Settings["useUserJobId"].Value) : "";
            Console.WriteLine("useUserJobId: " + useUserJobId);
            // Timezone configuration yet to be implemented
            string timeZone = (appSettings.Settings["timeZone"] != null) ? (appSettings.Settings["timeZone"].Value) : "";
            Console.WriteLine("timeZone: " + timeZone);
            if (!((timeZone is null) || (timeZone.Length < 1))) timeZone = "Australia/Melbourne";
            
            string extracontactlistfields = (appSettings.Settings["extracontactlistfields"] != null) ? (appSettings.Settings["extracontactlistfields"].Value) : "";
            if (!((extracontactlistfields is null) || (extracontactlistfields.Length < 1))) Console.WriteLine("extracontactlistfields: " + extracontactlistfields);
            else Console.WriteLine("No configured extracontactlistfields, using standard list of Contact List Fields...");

            string logMode = (appSettings.Settings["logMode"] != null) ? (appSettings.Settings["logMode"].Value) : "";
            List<String> allowedLogModes = new List<string>() { "Standard", "Debug" };
            if (!((logMode is null) || (logMode.Length < 1)) || !(allowedLogModes.Contains(logMode))) logMode = "Standard";
            Console.WriteLine("logMode: " + logMode);
            
            string taskstorun = (appSettings.Settings["taskstorun"] != null) ? (appSettings.Settings["taskstorun"].Value) : "";
            List<String> lstTasks = null;
            if (!((taskstorun is null) || (taskstorun.Length < 1))) lstTasks = taskstorun.Split(',').ToList();
            else lstTasks = null;
            Console.WriteLine("taskstorun: " + taskstorun);
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            List<string> extracontactlistfieldslst = new List<string>();
            if (!((extracontactlistfields is null) || (extracontactlistfields.Length < 1))) extracontactlistfieldslst = extracontactlistfields.Split(',').ToList();


            string daystoextract = (appSettings.Settings["daystoextract"] != null) ? (appSettings.Settings["daystoextract"].Value) : "";
            int iDaysToExtract;
            if(!(int.TryParse(daystoextract, out iDaysToExtract))) iDaysToExtract = -3;
            if ((intervalFilter is null) || (intervalFilter.Length < 1)) intervalFilter = DateTime.Today.AddDays(iDaysToExtract).ToString("s") + "/" + DateTime.Today.AddDays(1).ToString("s");
            string strIntervalStart = intervalFilter.Split('/')[0];
            DateTime dIntervalStartUTC = DateTime.Parse(strIntervalStart);
            DateTime dOrigIntervalStartUTC = DateTime.Parse(strIntervalStart);
            string strIntervalEnd = intervalFilter.Split('/')[1];
            DateTime dIntervalEndUTC = DateTime.Parse(strIntervalEnd);
            List<string> onedayIntervalFilters = new List<string>();
            do
            {
                onedayIntervalFilters.Add(dIntervalStartUTC.ToString("s") + "/" + dIntervalStartUTC.AddDays(1).ToString("s"));
                dIntervalStartUTC = dIntervalStartUTC.AddDays(1);
            } while (dIntervalStartUTC < dIntervalEndUTC);

            PureCloudRegionHosts regionHost;
            if (!Enum.TryParse(region, out regionHost))
            {
                regionHost = PureCloudRegionHosts.ap_northeast_1;
            }
            Console.WriteLine("Selected PureCloud Region: " + regionHost);
            PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.setBasePath(regionHost);

            var retryConfig = new ApiClient.RetryConfiguration
            {
                MaxRetryTimeSec = 10
            };

            PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.RetryConfig = retryConfig;

            // PureCloudNow Admin (NA)
            //string clientId = "bc3a689e-75cb-46bf-b5db-00b96895d7f4";
            //string clientSecret = "-IJkHUvQ21wPGtj0rddl-4g9f3MjlRQSsp7lrgMQMzg";

            // PureCloud APAC
            //string clientId = "393d20dc-07a1-4024-b81a-b64927cf9b0e";
            //string clientSecret = "zyl5EZsXrTgjnlnSEx8CcwHMceCvOihKRjwTHsZP8BE";


            Console.WriteLine(DateTime.Now.ToString("s") + " - Testing Database Access...");
            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            // Check the Database schema version and update Stored Procedures if necessary

            Console.WriteLine("------------------------------------------------------------------------\n\n");
            if (lstTasks is null || lstTasks.Contains("UpdateIntervals"))
            {
                PopulateIntervals(dbconnstring);
            }
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            if (lstTasks is null || lstTasks.Contains("UpdateStoredProcedures"))
            {
                sql.CommandText = File.ReadAllText(@"scripts\sp_gciCleanData.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_gciIVRSurveyResults.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_gciOutboundAttempts.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_AgentActivityLog.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_IAgentQueueStats.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_InteractionSummary.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_IWrkGrpQueueStats.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_ScoringDetail_viw.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_scoringsummary_viw.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_vcssSurveyScoringDetail.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_vcssSurveyScoringSummary.sql");
                sql.ExecuteNonQuery();
                sql.CommandText = File.ReadAllText(@"scripts\sp_gciTaskTracker.sql");
                sql.ExecuteNonQuery();
            }
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Retrieving Access Token...");
            var accessTokenInfo = PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.PostToken(clientId, clientSecret);
            Console.WriteLine("Access token: " + accessTokenInfo.AccessToken);
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Creating API Objects...");
            ConversationsApi convApi = new ConversationsApi();
            AnalyticsApi analApi = new AnalyticsApi();
            RecordingApi recApi = new RecordingApi();
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Dictionary<String, String> lsUsers = new Dictionary<string, string>();
            Dictionary<String, String> lsOrgPresences = new Dictionary<string, string>();

            try
            {
                //ApiTest();

                if (lstTasks is null || lstTasks.Contains("ExtractConfigurationData"))
                {
                    PopulateUsers(dbconnstring, logMode);

                    PopulateQueues(dbconnstring, logMode);

                    PopulateWrapupCodes(dbconnstring, logMode);

                    PopulateSkills(dbconnstring, logMode);

                    PopulateEvaluations(dbconnstring, logMode);

                    PopulateSystemPresences(dbconnstring, logMode);

                    PopulateOrgPresences(dbconnstring, logMode);

                    PopulateOutboundCampaigns(dbconnstring, logMode);

                    PopulateOutboundContactLists(dbconnstring, extracontactlistfieldslst, logMode);

                    PopulateFlows(dbconnstring, logMode);

                    PopulateEdges(dbconnstring, logMode);

                    PopulatePhones(dbconnstring, logMode);

                    PopulateOutboundMappings(dbconnstring, logMode);

                    PopulateOutboundCampaignRules(dbconnstring, logMode);
                }

                if (lstTasks is null || lstTasks.Contains("PopulateUserStatusDetails") || lstTasks.Contains("PopulateUserStatusDetails"))
                {
                    lsUsers = GetUsers();
                    lsOrgPresences = GetOrgPresences();
                }
                // Populate Transactional Data that requires a date range
                foreach (string onedayFilter in onedayIntervalFilters)
                {
                    Console.WriteLine(DateTime.Now.ToString("s") + " - Populating Data for " + onedayFilter);

                    if (lstTasks is null || lstTasks.Contains("PopulateConversations")) PopulateConversations(dbconnstring, onedayFilter, useConvJobId, logMode);

                    if (lstTasks is null || lstTasks.Contains("PopulateConversationAggregates")) PopulateConversationAggregates(dbconnstring, onedayFilter, useConvJobId, logMode);

                    if (lstTasks is null || lstTasks.Contains("PopulateQueueAggregates")) PopulateQueueAggregates(dbconnstring, onedayFilter, useConvJobId, logMode);

                    if (lstTasks is null || lstTasks.Contains("PopulateUserStatusDetails")) PopulateUserStatusDetails(dbconnstring, onedayFilter, lsUsers, lsOrgPresences, useUserJobId, logMode);

                    if (lstTasks is null || lstTasks.Contains("PopulateUserStatusAggregates")) PopulateUserStatusAggregates(dbconnstring, onedayFilter, lsUsers, lsOrgPresences, logMode);

                    Console.WriteLine(DateTime.Now.ToString("s") + " - Finish Populating Data for " + onedayFilter);

                }
            }
            catch (ApiException e)
            {
                Console.WriteLine("Error while pulling data...");
                Console.WriteLine(e.ToString());
                //System.Environment.Exit(10);
                throw (e);
            }

            if (lstTasks is null || lstTasks.Contains("CleanData"))
            {
                // Modify data in raw tables to a more friendly format
                CleanData(dbconnstring, dOrigIntervalStartUTC, dIntervalEndUTC, logMode);
            }

            if (lstTasks is null || lstTasks.Contains("TransformIntermediateData"))
            {
                // Transform data to Intermediate tables
                TransformIntermediateData(dbconnstring, dOrigIntervalStartUTC, dIntervalEndUTC, logMode);
            }

            if (lstTasks is null || lstTasks.Contains("TransformPureConnectData"))
            {
                // Transform data to PureConnect Tables
                TransformPureConnectData(dbconnstring, dOrigIntervalStartUTC, dIntervalEndUTC, logMode);
            }       

            Console.WriteLine("All tasks completed successfully.");

            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("End of Serco GC Data Extract - " + DateTime.Now.ToString("s"));
            Console.WriteLine("------------------------------------------------------------------------\n\n");
            if (!(streamwriter is null)) streamwriter.Close();
            //Console.WriteLine("(hit a key to continue)");
            //Console.ReadKey();
        }
        


        public static void ApiTest()
        {
            PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.setBasePath(PureCloudRegionHosts.ap_southeast_1);
            var retryConfig = new ApiClient.RetryConfiguration
            {
                MaxRetryTimeSec = 10
            };
            PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.RetryConfig = retryConfig;

            string clientId = "7b58ca4d-0926-42d3-976c-0a7f1e74ac00";
            string clientSecret = "hGq7WsGn7ciz65VVWVwbW3gacMf6AiU2JUTYC2mqct0";

            var accessTokenInfo = PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.PostToken(clientId, clientSecret);
            Console.WriteLine("Access token: " + accessTokenInfo.AccessToken);

            // Sample function to demonstrate using the PureCloud API
            QualityApi qualApi = new QualityApi();
            EvaluationAggregationQuery body = new EvaluationAggregationQuery();
            body.Interval = "2021-03-01T00:00:00/2021-04-14T00:00:00";
            body.Granularity = "PT1H";
            //body.Filter = new EvaluationAggregateQueryFilter();
            body.GroupBy = new List<EvaluationAggregationQuery.GroupByEnum>();
            body.Metrics = new List<EvaluationAggregationQuery.MetricsEnum>();
            body.Metrics.Add(EvaluationAggregationQuery.MetricsEnum.Ototalscore);
            body.GroupBy.Add(EvaluationAggregationQuery.GroupByEnum.Conversationid);
            //body.Filter.Clauses = new List<EvaluationAggregateQueryClause>();
            //body.Filter.Predicates = new List<EvaluationAggregateQueryPredicate>();
            //body.Filter.Type = EvaluationAggregateQueryFilter.TypeEnum.And;
            // https://developer.genesys.cloud/api/rest/v2/analytics/evaluations
            EvaluationAggregateQueryResponse evals = qualApi.PostAnalyticsEvaluationsAggregatesQuery(body);
            foreach (EvaluationAggregateDataContainer eval in evals.Results)
            {
                Console.WriteLine(eval.Group.Count);
                Console.Write(eval.Data.Count);
            }
            Console.ReadKey();
            System.Environment.Exit(0);
        }

        public static void PopulateIntervals(string dbconnstring)
        {
            Console.WriteLine("Initializing Intervals...");

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            DateTime dtPopStartUTC, dtPopEndUTC, dtPopStart, dtPopEnd, dtPopDayUTC, dtPopDay;
            
            System.Data.DataTable dtIntervals = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.dIntervals WHERE dIntervalUTC = (SELECT MAX(dIntervalUTC) FROM gen.dIntervals)";
            da.Fill(dtIntervals);
            if ((dtIntervals.Rows.Count < 1) || !(DateTime.TryParse(dtIntervals.Rows[0]["dIntervalUTC"].ToString(), out dtPopStartUTC))) dtPopStartUTC = new DateTime(2021, 03, 01);
            if ((dtIntervals.Rows.Count > 0)) dtIntervals.Rows[0].Delete();
            Console.WriteLine("Latest Date Interval found: " + dtPopStartUTC.ToString());


            while (dtPopStartUTC < DateTime.Now.AddDays(50))
            {
                dtPopStartUTC = dtPopStartUTC.AddMinutes(30);
                dtPopEndUTC = dtPopStartUTC.AddMinutes(30);
                dtPopStart = dtPopStartUTC.ToLocalTime();
                dtPopEnd = dtPopEndUTC.ToLocalTime();
                dtPopDayUTC = dtPopStartUTC.Date;
                dtPopDay = dtPopStart.Date;

                DataRow drIntervals = dtIntervals.NewRow();
                drIntervals["dIntervalUTC"] = dtPopStartUTC;
                drIntervals["dIntervalEndUTC"] = dtPopEndUTC;
                drIntervals["dInterval"] = dtPopStart;
                drIntervals["dIntervalEnd"] = dtPopEnd;
                drIntervals["dIntervalDayUTC"] = dtPopDayUTC;
                drIntervals["dIntervalDay"] = dtPopDay;
                dtIntervals.Rows.Add(drIntervals);
            }

            dtIntervals.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.dIntervals";
            bulkCopy.WriteToServer(dtIntervals);

            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateUsers(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing User Population...");
            // 
            UsersApi userApi = new UsersApi();
            
            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Users...");
            sql.CommandText = "TRUNCATE TABLE gen.gcUsers";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtUsers = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcUsers";
            da.Fill(dtUsers);

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of User Skills...");
            sql.CommandText = "TRUNCATE TABLE gen.gcUserSkills";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtUserSkills = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcUserSkills";
            da.Fill(dtUserSkills);

            Console.WriteLine(DateTime.Now.ToString("s") + " - Getting List of Users...");
            int currPage = 1;
            UserEntityListing users = new UserEntityListing(PageCount: 0, PageNumber: 0);

            do
            {
                // https://developer.genesys.cloud/api/rest/v2/users/#get-api-v2-users
                users = userApi.GetUsers(pageSize: 100, pageNumber: currPage, expand: new List<string>() { "geolocation", "station", "team", "profileSkills", "locations", "groups", "skills", "languages", "languagePreference", "employerInfo", "biography" });
                currPage += 1;

                Console.WriteLine($"Writing page {users.PageNumber }/{users.PageCount} with total of {users.Total} Users...");
                foreach (User u in users.Entities)
                {
                    DataRow drUsers = dtUsers.NewRow();
                    drUsers["UserId"] = u.Id;
                    drUsers["UserName"] = u.Username;
                    drUsers["Name"] = u.Name;
                    drUsers["FirstName"] = u.Name.Split(' ')[0];
                    drUsers["LastName"] = u.Name.Split(' ')[u.Name.Split(' ').Length - 1];
                    drUsers["DivisionId"] = u.Division.Id;
                    drUsers["DivisionName"] = u.Division.Name;
                    if (!(u.Geolocation is null) && !(u.Geolocation.Region is null)) drUsers["GeolocationRegion"] = u.Geolocation.Region;
                    if (!(u.Geolocation is null) && !(u.Geolocation.Name is null)) drUsers["GeolocationName"] = u.Geolocation.Name;
                    drUsers["Title"] = u.Title;
                    drUsers["Email"] = u.Email;
                    if (!(u.Department is null) && (u.Department.Trim().Length > 0)) drUsers["Department"] = u.Department;
                    drUsers["LanguagePreference"] = u.LanguagePreference;
                    if (!(u.Team is null)) drUsers["TeamName"] = u.Team.Name;
                    if (!(u.EmployerInfo is null) && !(u.EmployerInfo.DateHire is null)) drUsers["DateHire"] = u.EmployerInfo.DateHire;
                    if (!(u.EmployerInfo is null) && !(u.EmployerInfo.EmployeeId is null)) drUsers["EmployeeId"] = u.EmployerInfo.EmployeeId;
                    if (!(u.EmployerInfo is null) && !(u.EmployerInfo.EmployeeType is null)) drUsers["EmployeeType"] = u.EmployerInfo.EmployeeType;
                    if (!(u.EmployerInfo is null) && !(u.EmployerInfo.OfficialName is null)) drUsers["OfficialName"] = u.EmployerInfo.OfficialName;
                    drUsers["State"] = u.State.ToString();
                    if (!(u.Manager is null)) drUsers["ManagerId"] = u.Manager.Id;
                    dtUsers.Rows.Add(drUsers);
                    /*
                    sql.CommandText = "INSERT INTO gen.gcUsers (UserId,UserName,DivisionId,DivisionName,GeolocationRegion,GeolocationName,Title,Email, DateiHire, EmployeeId, EmployeeType, OfficialName) VALUES ('"
                        + u.Id + "','"
                        + u.Name.Replace("'", "''") + "','"
                        + u.Division.Id + "','"
                        + u.Division.Name.Replace("'", "''") + "','"
                        + (u.Geolocation is null ? "" : u.Geolocation.Region.Replace("'", "''")) + "','"
                        + ((!(u.Geolocation is null) && !(u.Geolocation.Name is null)) ? (u.Geolocation.Name.Replace("'", "''")) : "")  + "','"
                        + (u.Title is null ? "" : u.Title.Replace("'", "''")) + "','"
                        + u.Email.Replace("'", "''") + "'"
                        + ");";
                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                    */
                    //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                    if (!(u.Skills is null))
                    {
                        foreach (UserRoutingSkill s in u.Skills)
                        {
                            DataRow drUserSkills = dtUserSkills.NewRow();
                            drUserSkills["UserId"] = u.Id;
                            drUserSkills["UserName"] = u.Username;
                            drUserSkills["SkillId"] = s.Id;
                            drUserSkills["SkillName"] = s.Name;
                            drUserSkills["Proficiency"] = s.Proficiency;
                            drUserSkills["State"] = s.State.ToString();
                            dtUserSkills.Rows.Add(drUserSkills);

                            if (logMode.Contains("Debug")) Console.WriteLine(s.ToJson());
                        }
                    }
                }
                /*
                Console.WriteLine("Retrieving list of Deleted Users...");
                SearchApi searchApi = new SearchApi();
                UserSearchRequest sbody = new UserSearchRequest();
                sbody.Query = new List<UserSearchCriteria>();
                UserSearchCriteria crit = new UserSearchCriteria( Type: UserSearchCriteria.TypeEnum.Exact, Fields: new List<string>() { "state" }, Values: new List<string>() { "deleted" });;
                sbody.Query.Add(crit);
                sbody.PageSize = 100;
                int currPage = 0, pageCount = 1;
                while (currPage < pageCount)
                {
                    sbody.PageNumber = currPage;
                    if (pageCount == 1) Console.WriteLine($"Getting page 0 of Deleted Users");
                    else Console.WriteLine($"Getting page {currPage}/{pageCount} of Deleted Users");
                    UsersSearchResponse sr = searchApi.PostUsersSearch(sbody);
                    currPage = currPage + 1;
                    pageCount = sr.PageCount ?? 0;
                    if (!(sr.Results is null))
                    {
                        foreach (User u in sr.Results)
                        {
                            DataRow[] foundUsers = dtUsers.Select("UserId = '" + u.Id + "'");
                            if ((foundUsers is null) || (foundUsers.Length < 1))
                            {
                                DataRow drUsers = dtUsers.NewRow();
                                drUsers["UserId"] = u.Id;
                                drUsers["UserName"] = u.Username;
                                drUsers["Name"] = u.Name;
                                drUsers["State"] = u.State.ToString();
                                dtUsers.Rows.Add(drUsers);
                            }
                        }
                    }
                }
                */
            } while (users.PageNumber < users.PageCount);

            
            dtUsers.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcUsers";
            bulkCopy.WriteToServer(dtUsers);

            dtUserSkills.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcUserSkills";
            bulkCopy.WriteToServer(dtUserSkills);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateQueues(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Queues Population...");
            RoutingApi routApi = new RoutingApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine("Deleting old list of Queues...");
            sql.CommandText = "TRUNCATE TABLE gen.gcRoutingQueues";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtQueues = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcRoutingQueues";
            da.Fill(dtQueues);

            Console.WriteLine("Deleting old list of Queue Members...");
            sql.CommandText = "TRUNCATE TABLE gen.gcQueueMembers";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            
            System.Data.DataTable dtQueueMembers = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcQueueMembers";
            da.Fill(dtQueueMembers);
            QueueEntityListing queues = new QueueEntityListing(PageCount:0, PageNumber: 0);

            int currQPage = 1;

            do
            {
                Console.WriteLine("Getting List of Queues...");
                // https://developer.genesys.cloud/api/rest/v2/routing/#get-api-v2-routing-queues
                queues = routApi.GetRoutingQueues(pageSize: 100, pageNumber: currQPage);
                currQPage += 1;

                Console.WriteLine($"Writing page {queues.PageNumber}/{queues.PageCount} with total of {queues.Total} Queues...");
                foreach (Queue q in queues.Entities)
                {
                    long iDurationMs = 0;
                    double fPercentage = 0.0;
                    if (q.MediaSettings.ContainsKey("call"))
                    {
                        iDurationMs = q.MediaSettings["call"].ServiceLevel.DurationMs.Value;
                        fPercentage = q.MediaSettings["call"].ServiceLevel.Percentage.Value;
                    }

                    DataRow drQueues = dtQueues.NewRow();
                    drQueues["QueueId"] = q.Id;
                    drQueues["QueueName"] = q.Name;
                    drQueues["DivisionId"] = q.Division.Id;
                    drQueues["DivisionName"] = q.Division.Name;
                    drQueues["SkillEvaluationMethod"] = q.SkillEvaluationMethod.ToString();
                    drQueues["DurationMs"] = iDurationMs;
                    drQueues["Percentage"] = fPercentage;
                    dtQueues.Rows.Add(drQueues);

                    sql.CommandText = "INSERT INTO gen.gcRoutingQueues (QueueId,QueueName,DivisionId,DivisionName,SkillEvaluationMethod,DurationMs,Percentage) VALUES ('"
                        + q.Id + "','"
                        + q.Name + "','"
                        + q.Division.Id + "','"
                        + q.Division.Name + "','"
                        + q.SkillEvaluationMethod + "',"
                        + iDurationMs + ","
                        + fPercentage + ""
                        + ");";
                    //Console.WriteLine($"Executing: {sql.CommandText}");
                    //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                    if (logMode.Contains("Debug")) Console.WriteLine(q.ToJson());
                    int currQMPage = 1;
                    QueueMemberEntityListing qmem = new QueueMemberEntityListing(PageCount: 0, PageNumber: 0);
                    do
                    {
                        // https://developer.genesys.cloud/api/rest/v2/routing/#get-api-v2-routing-queues--queueId--members
                        qmem = routApi.GetRoutingQueueMembers(queueId: q.Id, pageSize: 100, pageNumber: currQMPage);
                        currQMPage += 1;
                        if (!(qmem.Entities is null))
                        {
                            foreach (QueueMember u in qmem.Entities)
                            {
                                DataRow drQueueMembers = dtQueueMembers.NewRow();
                                drQueueMembers["QueueId"] = q.Id;
                                drQueueMembers["QueueName"] = q.Name;
                                drQueueMembers["UserId"] = u.User.Id;
                                drQueueMembers["UserName"] = u.User.Name;
                                drQueueMembers["MemberId"] = u.Id;
                                drQueueMembers["MemberName"] = u.Name;
                                drQueueMembers["Joined"] = u.Joined;
                                drQueueMembers["MemberBy"] = u.MemberBy;
                                drQueueMembers["RingNumber"] = u.RingNumber;
                                if (!(u.RoutingStatus is null)) drQueueMembers["RoutingStatus"] = u.RoutingStatus.ToString();
                                dtQueueMembers.Rows.Add(drQueueMembers);

                                if (logMode.Contains("Debug")) Console.WriteLine(u.ToJson());
                            }
                        }
                    } while (qmem.PageNumber < qmem.PageCount);
                }
            } while (queues.PageNumber<queues.PageCount);

            dtQueues.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcRoutingQueues";
            bulkCopy.WriteToServer(dtQueues);

            dtQueueMembers.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcQueueMembers";
            bulkCopy.WriteToServer(dtQueueMembers);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateSkills(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine(DateTime.Now.ToString("s") + " - Initializing Skills Population...");
            RoutingApi routApi = new RoutingApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            SkillEntityListing skills = new SkillEntityListing(PageCount: 0, PageNumber: 0);
            int currPage = 1;
            do
            {
                Console.WriteLine(DateTime.Now.ToString("s") + " - Getting List of Skills...");
                // https://developer.genesys.cloud/api/rest/v2/routing/#get-api-v2-routing-skills
                skills = routApi.GetRoutingSkills(pageSize: 100 ,pageNumber: currPage);
                currPage += 1;
                Console.WriteLine("Deleting old list of Skills...");
                sql.CommandText = "TRUNCATE TABLE gen.gcRoutingSkills";
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine($"Writing page {skills.PageNumber}/{skills.PageCount} with total of {skills.Total} Skills...");
                foreach (RoutingSkill s in skills.Entities)
                {
                    sql.CommandText = "INSERT INTO gen.gcRoutingSkills (SkillId,SkillName,State) VALUES ('"
                        + s.Id + "','"
                        + s.Name.Replace("'", "''") + "','"
                        + s.State + "'"
                        + ");";
                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                    Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                }
            } while (skills.PageNumber < skills.PageCount);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateWrapupCodes(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine(DateTime.Now.ToString("s") + " - Initializing WrapupCodes Population...");
            RoutingApi routApi = new RoutingApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            WrapupCodeEntityListing wucodes = new WrapupCodeEntityListing(PageCount:0, PageNumber: 0);
            int currPage = 1;
            do
            {
                Console.WriteLine(DateTime.Now.ToString("s") + " - Getting List of WrapupCodes...");
                // https://developer.genesys.cloud/api/rest/v2/routing/#get-api-v2-routing-wrapupcodes
                wucodes = routApi.GetRoutingWrapupcodes(pageSize: 100, pageNumber: currPage);
                currPage += 1;
                Console.WriteLine("Deleting old list of WrapupCodes...");
                sql.CommandText = "TRUNCATE TABLE gen.gcWrapupCodes";
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine($"Writing {wucodes.PageNumber}/{wucodes.PageCount} with total of {wucodes.Total} WrapupCodes...");
                foreach (WrapupCode wuc in wucodes.Entities)
                {
                    sql.CommandText = "INSERT INTO gen.gcWrapupCodes (WrapupCodeId,WrapupCodeName,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES ("
                        + "'" + wuc.Id + "',"
                        + "'" + wuc.Name.Replace("'", "''") + "',"
                        + "'" + wuc.CreatedBy + "',"
                        + ((wuc.DateCreated.HasValue) ? ("'" + wuc.DateCreated.Value.ToString("s") + "',") : "null,")
                        + "'" + wuc.ModifiedBy + "',"
                        + ((wuc.DateModified.HasValue) ? ("'" + wuc.DateModified.Value.ToString("s") + "'") : "null")
                        + ");";
                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                    Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                }
            } while (wucodes.PageNumber < wucodes.PageCount);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateEvaluations(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Evaluations Population...");
            QualityApi qualApi = new QualityApi();
            
            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Evaluation Forms...");
            sql.CommandText = "TRUNCATE TABLE gen.gcEvaluationForms";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtForms = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcEvaluationForms";
            da.Fill(dtForms);

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Evaluation Question Groups...");
            sql.CommandText = "TRUNCATE TABLE gen.gcEvaluationQuestionGroups";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtQuestionGroups = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcEvaluationQuestionGroups";
            da.Fill(dtQuestionGroups);

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Evaluation Questions...");
            sql.CommandText = "TRUNCATE TABLE gen.gcEvaluationQuestions";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtQuestions = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcEvaluationQuestions";
            da.Fill(dtQuestions);

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Evaluation Answer Options...");
            sql.CommandText = "TRUNCATE TABLE gen.gcEvaluationAnswerOptions";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtAnswerOptions = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcEvaluationAnswerOptions";
            da.Fill(dtAnswerOptions);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            EvaluationFormEntityListing forms = new EvaluationFormEntityListing(PageCount: 0, PageNumber: 0);
            int currPage = 1;
            do
            {
                Console.WriteLine(DateTime.Now.ToString("s") + " - Getting List of Evaluation Forms...");
                // https://developer.genesys.cloud/api/rest/v2/quality/#get-api-v2-quality-forms-evaluations
                forms = qualApi.GetQualityFormsEvaluations(pageSize: 100, pageNumber: currPage);
                currPage += 1;
                Console.WriteLine($"Writing {forms.PageNumber}/{forms.PageCount} with total of {forms.Total} Evaluation Forms...");
                foreach (EvaluationForm mostrecent in forms.Entities)
                {
                    EvaluationFormEntityListing allversions = new EvaluationFormEntityListing(PageCount: 0, PageNumber: 0);
                    int currVPage = 1;
                    do
                    {
                        // https://developer.genesys.cloud/api/rest/v2/quality/#get-api-v2-quality-forms-evaluations--formId--versions
                        allversions = qualApi.GetQualityFormsEvaluationVersions(formId: mostrecent.Id, pageSize: 100, pageNumber: currVPage);
                        currVPage += 1;
                        EvaluationForm latest = allversions.Entities[0];
                        foreach (EvaluationForm e in allversions.Entities)
                        {
                            if (e.ModifiedDate > latest.ModifiedDate) latest = e;
                        }
                        foreach (EvaluationForm e in allversions.Entities)
                        {

                            int iMaxFormScore = 0;
                            int iMaxFormCriticalScore = 0;
                            int QuestionGroupOrder = 1;

                            // https://developer.genesys.cloud/api/rest/v2/quality/#get-api-v2-quality-forms-evaluations--formId-
                            EvaluationForm eval = qualApi.GetQualityFormsEvaluation(e.Id);

                            foreach (EvaluationQuestionGroup qg in eval.QuestionGroups)
                            {
                                int iMaxQGScore = 0;
                                int iMaxQGCriticalScore = 0;
                                int QuestionOrder = 1;

                                foreach (EvaluationQuestion q in qg.Questions)
                                {
                                    int iMaxQScore = 0;
                                    int iMaxQCriticalScore = 0;
                                    int AnswerOrder = 1;

                                    foreach (AnswerOption ao in q.AnswerOptions)
                                    {
                                        //Console.WriteLine($"Collating gcEvaluationAnswerOptions {ao.Id}...");
                                        DataRow drAnswerOptions = dtAnswerOptions.NewRow();
                                        drAnswerOptions["EvaluationFormId"] = e.Id;
                                        drAnswerOptions["ContextId"] = e.ContextId;
                                        drAnswerOptions["EvaluationQuestionGroupId"] = qg.Id;
                                        drAnswerOptions["EvaluationQuestionId"] = q.Id;
                                        drAnswerOptions["EvaluationAnswerOptionId"] = ao.Id;
                                        drAnswerOptions["Text"] = ao.Text;
                                        drAnswerOptions["Value"] = ao.Value;
                                        drAnswerOptions["AnswerOrder"] = AnswerOrder;
                                        dtAnswerOptions.Rows.Add(drAnswerOptions);

                                        sql.CommandText = "INSERT INTO gen.gcEvaluationAnswerOptions (EvaluationFormId,ContextId,EvaluationQuestionGroupId,EvaluationQuestionId,EvaluationAnswerOptionId,Text,Value,AnswerOrder) VALUES ("
                                            + "'" + e.Id + "',"
                                            + "'" + e.ContextId + "',"
                                            + "'" + qg.Id + "',"
                                            + "'" + q.Id + "',"
                                            + "'" + ao.Id + "',"
                                            + "'" + ao.Text.Replace("'", "''") + "',"
                                            + ao.Value.Value + ","
                                            + AnswerOrder
                                            + ")";
                                        if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                        //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

                                        if ((q.IsCritical.Value) && (ao.Value > iMaxQCriticalScore)) iMaxQCriticalScore = ao.Value.Value;
                                        if ((ao.Value > iMaxQScore)) iMaxQScore = ao.Value.Value;
                                        AnswerOrder += 1;
                                    }


                                    //Console.WriteLine($"Collating gcEvaluationQuestions {q.Id}...");
                                    DataRow drQuestions = dtQuestions.NewRow();
                                    drQuestions["EvaluationFormId"] = e.Id;
                                    drQuestions["ContextId"] = e.ContextId;
                                    drQuestions["EvaluationQuestionGroupId"] = qg.Id;
                                    drQuestions["EvaluationQuestionId"] = q.Id;
                                    drQuestions["Text"] = q.Text;
                                    drQuestions["CommentsRequired"] = q.CommentsRequired;
                                    drQuestions["HelpText"] = q.HelpText;
                                    drQuestions["IsCritical"] = q.IsCritical;
                                    drQuestions["IsKill"] = q.IsKill;
                                    drQuestions["NaEnabled"] = q.NaEnabled;
                                    drQuestions["Type"] = q.Type;
                                    drQuestions["VisibilityCondition"] = ((q.VisibilityCondition is null) ? "" : q.VisibilityCondition.ToJson());
                                    drQuestions["MaxQScore"] = iMaxQScore;
                                    drQuestions["MaxQCriticalScore"] = iMaxQCriticalScore;
                                    drQuestions["QuestionOrder"] = QuestionOrder;
                                    dtQuestions.Rows.Add(drQuestions);

                                    sql.CommandText = "INSERT INTO gen.gcEvaluationQuestions (EvaluationFormId,ContextId,EvaluationQuestionGroupId,EvaluationQuestionId,Text,CommentsRequired,HelpText,IsCritical,IsKill,NaEnabled,Type,VisibilityCondition,MaxQScore,MaxQCriticalScore,QuestionOrder) VALUES ("
                                        + "'" + e.Id + "',"
                                            + "'" + e.ContextId + "',"
                                        + "'" + qg.Id + "',"
                                        + "'" + q.Id + "',"
                                        + "'" + q.Text.Replace("'", "''") + "',"
                                        + "'" + q.CommentsRequired + "',"
                                        + "'" + q.HelpText.Replace("'", "''") + "',"
                                        + "'" + q.IsCritical + "',"
                                        + "'" + q.IsKill + "',"
                                        + "'" + q.NaEnabled + "',"
                                        + "'" + q.Type + "',"
                                        + "'" + ((q.VisibilityCondition is null) ? "" : q.VisibilityCondition.ToJson()) + "',"
                                        + iMaxQScore + ","
                                        + iMaxQCriticalScore + ","
                                        + QuestionOrder
                                        + ");";
                                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                    //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

                                    iMaxQGScore += iMaxQScore;
                                    iMaxQGCriticalScore += iMaxQCriticalScore;
                                    QuestionOrder += 1;
                                }

                                //Console.WriteLine($"Collating gcEvaluationQuestionGroups {qg.Id}...");
                                DataRow drQuestionGroups = dtQuestionGroups.NewRow();
                                drQuestionGroups["EvaluationFormId"] = e.Id;
                                drQuestionGroups["ContextId"] = e.ContextId;
                                drQuestionGroups["EvaluationQuestionGroupId"] = qg.Id;
                                drQuestionGroups["EvaluationQuestionGroupName"] = qg.Name;
                                drQuestionGroups["DefaultAnswersToHighest"] = qg.DefaultAnswersToHighest;
                                drQuestionGroups["DefaultAnswersToNA"] = qg.DefaultAnswersToNA;
                                drQuestionGroups["ManualWeight"] = qg.ManualWeight;
                                drQuestionGroups["NaEnabled"] = qg.NaEnabled;
                                drQuestionGroups["Type"] = qg.Type;
                                drQuestionGroups["VisibilityCondition"] = ((qg.VisibilityCondition is null) ? "" : qg.VisibilityCondition.ToJson());
                                drQuestionGroups["Weight"] = qg.Weight;
                                drQuestionGroups["MaxQGScore"] = iMaxQGScore;
                                drQuestionGroups["MaxQGCriticalScore"] = iMaxQGCriticalScore;
                                drQuestionGroups["QuestionGroupOrder"] = QuestionGroupOrder;
                                dtQuestionGroups.Rows.Add(drQuestionGroups);

                                sql.CommandText = "INSERT INTO gen.gcEvaluationQuestionGroups (EvaluationFormId,ContextId,EvaluationQuestionGroupId,EvaluationQuestionGroupName,DefaultAnswersToHighest,DefaultAnswersToNA,ManualWeight,NaEnabled,Type,VisibilityCondition,Weight,MaxQGScore,MaxQGCriticalScore,QuestionGroupOrder) "
                                    + "VALUES ("
                                    + "'" + e.Id + "',"
                                    + "'" + e.ContextId + "',"
                                    + "'" + qg.Id + "',"
                                    + "'" + qg.Name.Replace("'", "''") + "',"
                                    + "'" + qg.DefaultAnswersToHighest + "',"
                                    + "'" + qg.DefaultAnswersToNA + "',"
                                    + "'" + qg.ManualWeight + "',"
                                    + "'" + qg.NaEnabled + "',"
                                    + "'" + qg.Type + "',"
                                    + ((qg.VisibilityCondition is null) ? "null," : ("'" + qg.VisibilityCondition.ToJson() + "',"))
                                    + "" + qg.Weight + ","
                                    + "" + iMaxQGScore + ","
                                    + "" + iMaxQGCriticalScore + ","
                                    + QuestionGroupOrder
                                    + ");";
                                if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

                                iMaxFormScore += iMaxQGScore;
                                iMaxFormCriticalScore += iMaxQGCriticalScore;
                                QuestionGroupOrder += 1;
                            }

                            //Console.WriteLine($"Collating gcEvaluationForms {e.Id}...");
                            DataRow drForms = dtForms.NewRow();
                            drForms["EvaluationFormId"] = e.Id;
                            drForms["EvaluationFormName"] = e.Name;
                            drForms["ContextId"] = e.ContextId;
                            drForms["LatestVersion"] = (latest.Id == e.Id);
                            drForms["ModifiedDateUTC"] = e.ModifiedDate.Value.ToUniversalTime();
                            drForms["Published"] = e.Published;
                            drForms["MaxFormScore"] = iMaxFormScore;
                            drForms["MaxFormCriticalScore"] = iMaxFormCriticalScore;
                            dtForms.Rows.Add(drForms);

                            sql.CommandText = "INSERT INTO gen.gcEvaluationForms (EvaluationFormId,EvaluationFormName,ContextId,LatestVersion,ModifiedDateUTC,Published,MaxFormScore,MaxFormCriticalScore) "
                                + "VALUES ("
                                + "'" + e.Id + "',"
                                + "'" + e.Name.Replace("'", "''") + "',"
                                + "'" + e.ContextId + "',"
                                + "'" + (latest.Id == e.Id) + "',"
                                + "'" + e.ModifiedDate.Value.ToUniversalTime().ToString("s") + "',"
                                + "'" + e.Published + "',"
                                + iMaxFormScore + ","
                                + iMaxFormCriticalScore + ""
                                + ");";
                            if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                            //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                        }
                    } while (allversions.PageNumber < allversions.PageCount);

                    
                }

            } while (forms.PageNumber < forms.PageCount);

            Console.WriteLine($"Writing records to gen.gcEvaluationAnswerOptions...");
            dtAnswerOptions.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcEvaluationAnswerOptions";
            bulkCopy.WriteToServer(dtAnswerOptions);

            Console.WriteLine($"Writing records to gen.gcEvaluationQuestions...");
            dtQuestions.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcEvaluationQuestions";
            bulkCopy.WriteToServer(dtQuestions);

            Console.WriteLine($"Writing records to gen.gcEvaluationQuestionGroups...");
            dtQuestionGroups.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcEvaluationQuestionGroups";
            bulkCopy.WriteToServer(dtQuestionGroups);

            Console.WriteLine($"Writing records to gen.gcEvaluationForms...");
            dtForms.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcEvaluationForms";
            bulkCopy.WriteToServer(dtForms);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateSystemPresences(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine(DateTime.Now.ToString("s") + " - Initializing System Presence Population...");
            // https://developer.genesys.cloud/api/rest/v2/presence/
            PresenceApi presApi = new PresenceApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of System Presences...");
            sql.CommandText = "TRUNCATE TABLE gen.gcPresences";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Getting List of System Presences...");

            // https://developer.genesys.cloud/api/rest/v2/presence/#get-api-v2-systempresences
            List<SystemPresence> lsPresences = presApi.GetSystempresences();
            Console.WriteLine($"Writing total of {lsPresences.Count} System Presences (no paging)...");
            foreach (SystemPresence sp in lsPresences)
            {
                sql.CommandText = "INSERT INTO gen.gcPresences (PresenceId,PresenceName) VALUES ('"
                    + sp.Id + "','"
                    + sp.Name + "'"
                    + ");";
                if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            }
            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateOutboundCampaigns(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine(DateTime.Now.ToString("s") + " - Initializing Outbound Campaign Population...");
            OutboundApi outApi = new OutboundApi(); 
            
            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Outbound Campaigns...");
            sql.CommandText = "TRUNCATE TABLE gen.gcOutboundCampaigns";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Getting List of Outbound Campaigns...");
            Dictionary<String, String> lsOutbound = new Dictionary<string, string>();
            int currPage = 1;
            CampaignEntityListing camps = new CampaignEntityListing(PageCount: 0, PageNumber: 0);
            do
            {
                // https://developer.genesys.cloud/api/rest/v2/outbound/#get-api-v2-outbound-campaigns
                camps = outApi.GetOutboundCampaigns(pageSize: 100, pageNumber: currPage);
                Console.WriteLine($"Writing page {camps.PageNumber}/{camps.PageCount} with total of {camps.Total} Outbound Campaigns...");
                currPage += 1;

                foreach (Campaign obc in camps.Entities)
                {
                    sql.CommandText = "INSERT INTO gen.gcOutboundCampaigns (CampaignId,CampaignName,AbandonRate,AlwaysRunning,CallableTimeSetId,CallableTimeSetName,CallAnalysisLanguage,CallAnalysisResponseSet,CallerAddress,CallerName,CampaignStatus,ContactListId,ContactListName,ContactSortFieldName,ContactSortDirection,ContactSortNumeric,DateCreatedUTC,DateModifiedUTC,DialingMode,DivisionId,DivisionName,DncLists,EdgeGroupId,EdgeGroupName,NoAnswerTimeout,OutboundLineCount,PreviewTimeOutSeconds,Priority,QueueId,QueueName,SiteId,SiteName) VALUES ("
                        + "'" + obc.Id + "',"
                        + "'" + obc.Name.Replace("'","''") + "',"
                        + "" + (obc.AbandonRate ?? 0) + ","
                        + (!(obc.AlwaysRunning is null) ? ("'" + obc.AlwaysRunning + "',") : ("null,"))
                        + (!(obc.CallableTimeSet is null) ? ("'" + obc.CallableTimeSet.Id + "',") : ("null,"))
                        + (!(obc.CallableTimeSet is null) ? ("'" + obc.CallableTimeSet.Name + "',") : ("null,"))
                        + (!(obc.CallAnalysisLanguage is null) ? ("'" + obc.CallAnalysisLanguage + "',") : ("null,"))
                        + (!(obc.CallAnalysisResponseSet is null) ? ("'" + obc.CallAnalysisResponseSet.Name + "',") : ("null,"))
                        + "'" + obc.CallerAddress + "',"
                        + "'" + obc.CallerName + "',"
                        + (!(obc.CampaignStatus is null) ? ("'" + obc.CampaignStatus.ToString() + "',") : ("null,"))
                        + (!(obc.ContactList is null) ? ("'" + obc.ContactList.Id + "',") : ("null,"))
                        + (!(obc.ContactList is null) ? ("'" + obc.ContactList.Name + "',") : ("null,"))
                        //+ "'" + obc.ContactListFilters.ToArray().ToString() + "',"
                        + (!(obc.ContactSort is null) ? ("'" + obc.ContactSort.FieldName + "',") : ("null,"))
                        + (!(obc.ContactSort is null) ? ("'" + obc.ContactSort.Direction + "',") : ("null,"))
                        + (!(obc.ContactSort is null) ? ("'" + obc.ContactSort.Numeric + "',") : ("null,"))
                        + (!(obc.DateCreated is null) ? ("'" + obc.DateCreated.Value.ToUniversalTime().ToString("s") + "',") : ("null,"))
                        + (!(obc.DateModified is null) ? ("'" + obc.DateModified.Value.ToUniversalTime().ToString("s") + "',") : ("null,"))
                        + (!(obc.DialingMode is null) ? ("'" + obc.DialingMode.ToString() + "',") : ("null,"))
                        + (!(obc.Division is null) ? ("'" + obc.Division.Id + "',") : ("null,"))
                        + (!(obc.Division is null) ? ("'" + obc.Division.Name + "',") : ("null,"))
                        + (!(obc.DncLists is null) ? ("'" + obc.DncLists.ToArray().ToString().Replace("System.String", "") + "',") : ("null,"))
                        + (!(obc.EdgeGroup is null) ? ("'" + obc.EdgeGroup.Id + "',") : ("null,"))
                        + (!(obc.EdgeGroup is null) ? ("'" + obc.EdgeGroup.Name + "',") : ("null,"))
                        + "" + (obc.NoAnswerTimeout ?? 0) + ","
                        + "'" + (obc.OutboundLineCount ?? 0) + "',"
                        + "'" + (obc.PreviewTimeOutSeconds ?? 0) + "',"
                        + "'" + (obc.Priority ?? 0) + "',"
                        + (!(obc.Queue is null) ? ("'" + obc.Queue.Id + "',") : ("null,"))
                        + (!(obc.Queue is null) ? ("'" + obc.Queue.Name + "',") : ("null,"))
                        + (!(obc.Site is null) ? ("'" + obc.Site.Id + "',") : ("null,"))
                        + (!(obc.Site is null) ? ("'" + obc.Site.Name + "'") : ("null"))
                        + ");";
                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                    Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                    lsOutbound.Add(obc.Id, obc.Name);
                }
            } while (camps.PageNumber < camps.PageCount);
            
            
            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateOutboundContactLists(string dbconnstring, List<string> extraContactListFields, string logMode="Standard")
        {
            Console.WriteLine(DateTime.Now.ToString("s") + " - Initializing Outbound Contact Lists Population...");
            OutboundApi outApi = new OutboundApi();
            
            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Outbound Contact Lists...");
            sql.CommandText = "TRUNCATE TABLE gen.gcOutboundContactLists";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtContactLists = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcOutboundContactLists";
            da.Fill(dtContactLists);

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Outbound Contacts...");
            sql.CommandText = "TRUNCATE TABLE gen.gcOutboundContacts";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtContacts = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcOutboundContacts";
            da.Fill(dtContacts);

            /*
            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Outbound Contacts Extra Fields...");
            sql.CommandText = "TRUNCATE TABLE gen.gcOutboundContactsExtraFields";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            */

            System.Data.DataTable dtContactsExtraFields = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcOutboundContactsExtraFields";
            da.Fill(dtContactsExtraFields);
            if (dtContactsExtraFields.Rows.Count > 0) dtContactsExtraFields.Rows[0].Delete();

            ContactListEntityListing cls = new ContactListEntityListing(PageCount: 0, PageNumber: 0);
            Console.WriteLine(DateTime.Now.ToString("s") + " - Getting List of Outbound Contact Lists...");
            Dictionary<String, String> lsOutbound = new Dictionary<string, string>();
            
            //https://developer.genesys.cloud/api/rest/v2/outbound/#get-api-v2-outbound-contactlists
            cls= outApi.GetOutboundContactlists(pageSize: 100, sortBy: "DateModified", sortOrder: "descending");
            Console.WriteLine($"Writing total of {cls.Total} Outbound Campaigns (no paging)...");
            foreach (ContactList cl in cls.Entities)
            {
                string strColumnNames = ((cl.ColumnNames is null) ? "" : (String.Join(",",cl.ColumnNames))); 
                string strPhoneColumns = ((cl.PhoneColumns is null) ? "" : (String.Join(",", cl.PhoneColumns))); 
                string strPreviewModeAcceptedValues = ((cl.PreviewModeAcceptedValues is null)? "" : (String.Join(",", cl.PreviewModeAcceptedValues)));

                DataRow drContactLists = dtContactLists.NewRow();
                drContactLists["ContactListId"] = cl.Id;
                drContactLists["ContactListName"] = cl.Name;
                drContactLists["AttemptLimitsId"] = ((cl.AttemptLimits is null) ? null : (cl.AttemptLimits.Id));
                drContactLists["AttemptLimitsName"] = ((cl.AttemptLimits is null) ? null : (cl.AttemptLimits.Name));
                drContactLists["AutomaticTimeZoneMapping"] = ((cl.AutomaticTimeZoneMapping is null) ? null : (cl.AutomaticTimeZoneMapping));
                drContactLists["ColumnNames"] = strColumnNames;
                if (!(cl.DateCreated is null)) drContactLists["DateCreated"] = cl.DateCreated.Value.ToUniversalTime();
                if (!(cl.DateModified is null)) drContactLists["DateModified"] = cl.DateModified.Value.ToUniversalTime();
                drContactLists["DivisionId"] = (!(cl.Division is null) ? cl.Division.Id : null);
                drContactLists["DivisionName"] = (!(cl.Division is null) ? cl.Division.Name : null);
                drContactLists["ImportCompletedRecords"] = (!(cl.ImportStatus is null) ? (cl.ImportStatus.CompletedRecords ?? 0) : 0);
                drContactLists["ImportFailureReason"] = (!(cl.ImportStatus is null) ? cl.ImportStatus.FailureReason : null);
                drContactLists["ImportPercentComplete"] = (!(cl.ImportStatus is null) ? (cl.ImportStatus.PercentComplete ?? 0) : 0);
                drContactLists["ImportState"] = (!(cl.ImportStatus is null) ? cl.ImportStatus.State : null);
                if (!(cl.ImportStatus is null)) drContactLists["ImportStatus"] = cl.ImportStatus.TotalRecords;
                drContactLists["PhoneColumns"] = strPhoneColumns;
                drContactLists["PreviewModeAcceptedValues"] = strPreviewModeAcceptedValues;
                drContactLists["PreviewModeColumnName"] = cl.PreviewModeColumnName;
                drContactLists["Size"] = (cl.Size ?? 0);
                drContactLists["Version"] = (cl.Version ?? 0);
                drContactLists["ZipCodeColumnName"] = cl.ZipCodeColumnName;
                dtContactLists.Rows.Add(drContactLists);

                if (logMode.Contains("Debug")) Console.Write(cl.ToJson());

                /// ////////////////////////////////////////////////////////////////////////////////////////
                /// Dowload Contact List Contacts in CSV
                /// ////////////////////////////////////////////////////////////////////////////////////////

                var contactListId = cl.Id;
                ExportUri downloadUri = new ExportUri();
                // https://developer.genesys.cloud/api/rest/v2/outbound/#post-api-v2-outbound-contactlists--contactListId--export
                DomainEntityRef result = outApi.PostOutboundContactlistExport(contactListId);
                bool KeepTryingExport = true;
                do {
                    try
                    {
                        System.Threading.Thread.Sleep(2000);
                        Console.WriteLine("Trying to retrieve download URI for export file...");
                        // https://developer.genesys.cloud/api/rest/v2/outbound/#get-api-v2-outbound-contactlists--contactListId--export
                        downloadUri = outApi.GetOutboundContactlistExport(contactListId, "false");
                        Console.WriteLine("Retrieved Download URI: " + downloadUri.Uri);
                        KeepTryingExport = false;
                    }
                    catch (ApiException e)
                    {
                        Console.WriteLine(e.ToString());
                        if (e.ErrorCode == 404) KeepTryingExport = true;
                        else KeepTryingExport = false;
                    }
                } while (KeepTryingExport);
                
                if(!(downloadUri.Uri is null))
                {
                    WebClient wc = new WebClient();
                    try
                    {
                        string filepath = Directory.GetCurrentDirectory() + "\\" + cl.Name + ".csv";

                        Console.WriteLine($"Retrieving Contact List {contactListId} - {cl.Name} to {filepath}");

                        Console.WriteLine("Downloading from URI: " + downloadUri.Uri);
                        Console.WriteLine("Access Token: " + PureCloudPlatform.Client.V2.Client.Configuration.Default.AccessToken);
                        //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                        wc.Headers.Add("Authorization", "bearer " + PureCloudPlatform.Client.V2.Client.Configuration.Default.AccessToken);
                        wc.DownloadFile(downloadUri.Uri, filepath);

                        Console.WriteLine($"Wrote Contact List {contactListId} - {cl.Name} to {filepath}");

                        Console.WriteLine(DateTime.Now.ToString("s") + " - Marking older Extra Fields as not latest...");
                        sql.CommandText = $"UPDATE gen.gcOutboundContactsExtraFields SET Latest = 0 WHERE Latest = 1 AND ContactListId = '{contactListId}';" ;
                        Console.WriteLine($"Executing: {sql.CommandText}");
                        Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");


                        string pathOnly = Path.GetDirectoryName(filepath);
                        string fileName = Path.GetFileName(filepath);
                        string csvsql = @"SELECT * FROM [" + fileName + "]";

                        // Read the first line of the file and parse it
                        List<String> colIndex = new List<string>();
                        List<String> allowedFields = new List<string>()
                        {
                            "inin-outbound-id","CustID","FirstName","LastName","Timezone","ContactCallable","Contact Attempts","Contact_Attempts",
                            "Phone_1","CallRecordLastAttempt-Phone_1","CallRecordLastResult-Phone_1","CallRecordLastAgentWrapup-Phone_1","SmsLastAttempt-Phone_1","SmsLastResult-Phone_1","Callable-Phone_1",
                            "Phone_2","CallRecordLastAttempt-Phone_2","CallRecordLastResult-Phone_2","CallRecordLastAgentWrapup-Phone_2","SmsLastAttempt-Phone_2","SmsLastResult-Phone_2","Callable-Phone_2",
                            "Phone_3","CallRecordLastAttempt-Phone_3","CallRecordLastResult-Phone_3","CallRecordLastAgentWrapup-Phone_3","SmsLastAttempt-Phone_3","SmsLastResult-Phone_3","Callable-Phone_3"
                        };
                        using (TextFieldParser parser = new TextFieldParser(filepath))
                        {
                            parser.TextFieldType = FieldType.Delimited;
                            parser.SetDelimiters(",");
                            parser.HasFieldsEnclosedInQuotes = true;
                            bool bFirstRow = true;
                            int iContIndex = 0;
                            while (!parser.EndOfData)
                            {
                                //Process row
                                string[] fields = parser.ReadFields();

                                if (bFirstRow)
                                {
                                    for (int i = 0; i < fields.Length; i++)
                                    {
                                        colIndex.Add(fields[i]);
                                        if (fields[i].ToLower().Equals("inin-outbound-id")) iContIndex = i;
                                    }
                                    bFirstRow = false;
                                    continue;
                                }

                                DataRow drContacts = dtContacts.NewRow();
                                drContacts["contactListId"] = contactListId;
                                string contactId = fields[iContIndex];
                                for (int i=0; i<fields.Length; i++)
                                {
                                    if(allowedFields.Contains(colIndex[i].Replace(" ", "_")))
                                    {
                                        //if (dtContacts.Columns[colIndex[i]].DataType.Name.Equals("DateTime") && fields[i].Length < 1)
                                        if (fields[i].Trim().Length > 0)
                                        {
                                            drContacts[colIndex[i].Replace(" ", "_")] = fields[i];
                                            if (logMode.Contains("Debug")) Console.Write(fields[i] + ",");
                                        }
                                        if (logMode.Contains("Debug")) Console.WriteLine("");
                                    }
                                    
                                    if (extraContactListFields.Contains(colIndex[i].Replace(" ", "_")))
                                    {
                                        DataRow drContactsExtraFields = dtContactsExtraFields.NewRow();
                                        drContactsExtraFields["ContactListId"] = contactListId;
                                        drContactsExtraFields["inin-outbound-id"] = contactId;
                                        drContactsExtraFields["Field"] = colIndex[i].Replace(" ", "_");
                                        drContactsExtraFields["Value"] = fields[i];
                                        drContactsExtraFields["DateAdded"] = DateTime.Now;
                                        drContactsExtraFields["Latest"] = 1;
                                        dtContactsExtraFields.Rows.Add(drContactsExtraFields);
                                    }
                                }
                                dtContacts.Rows.Add(drContacts);
                            }
                            parser.Close();
                        }
                        try
                        {
                            Console.WriteLine($"Deleting Contact List {filepath}...");
                            File.Delete(filepath);
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine(e.InnerException);
                            Console.WriteLine(e.Message);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.InnerException);
                        Console.WriteLine(e.Message);
                        throw (e);
                    }
                }
            }

            dtContactLists.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcOutboundContactLists";
            bulkCopy.WriteToServer(dtContactLists);

            dtContacts.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcOutboundContacts";
            bulkCopy.WriteToServer(dtContacts);

            dtContactsExtraFields.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcOutboundContactsExtraFields";
            bulkCopy.WriteToServer(dtContactsExtraFields);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateOrgPresences(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Organization Presence Population...");
            PresenceApi presApi = new PresenceApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);


            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine("Deleting old list of Organization Presences...");
            sql.CommandText = "TRUNCATE TABLE gen.gcOrgPresences";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            Console.WriteLine("Getting List of Organization Presences...");
            // https://developer.genesys.cloud/api/rest/v2/presence/#get-api-v2-presencedefinitions
            OrganizationPresenceEntityListing orgps = presApi.GetPresencedefinitions(pageSize: 100);
            Console.WriteLine($"Writing total of {orgps.Total} Organization Presences (no paging)...");
            foreach (OrganizationPresence op in orgps.Entities)
            {
                sql.CommandText = "INSERT INTO gen.gcOrgPresences (OrgPresenceId,OrgPresenceName,SystemPresence,[Primary],Deactivated,CreatedById,CreatedByName,CreatedByDateUTC,ModifiedById,ModifiedByName,ModifiedByDateUTC) VALUES ("
                    + "'" + op.Id + "',"
                    + "'" + (op.Name ?? op.SystemPresence) + "',"
                    + "'" + op.SystemPresence + "',"
                    + "'" + op.Primary + "',"
                    + "'" + op.Deactivated + "',"
                    + "'" + op.CreatedBy.Id + "',"
                    + "'" + op.CreatedBy.Name + "',"
                    + ((op.CreatedDate.HasValue) ? ("'" + op.CreatedDate.Value.ToString("s") + "',") : "null")
                    + "'" + op.ModifiedBy.Id + "',"
                    + "'" + op.ModifiedBy.Name + "',"
                    + ((op.ModifiedDate.HasValue) ? ("'" + op.ModifiedDate.Value.ToString("s") + "'") : "null")
                    + ");";
                if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            }
            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateFlows(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Flows Population...");
            ArchitectApi archApi = new ArchitectApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Flows...");
            sql.CommandText = "TRUNCATE TABLE gen.gcFlows";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtFlows = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcFlows";
            da.Fill(dtFlows);

            Console.WriteLine("Creating Flows Query...");
            FlowEntityListing fes = archApi.GetFlows(pageSize: 100);
            Console.WriteLine($"Writing total of {fes.Total} Flows (no paging)...");
            if (fes.Total > 0)
            {
                foreach (Flow fe in fes.Entities)
                {
                    DataRow drFlows = dtFlows.NewRow();
                    drFlows["FlowId"] = fe.Id;
                    drFlows["FlowName"] = fe.Name;
                    drFlows["Active"] = fe.Active.ToString();
                    if (!(fe.CheckedInVersion is null)) drFlows["CheckedInVersion"] = fe.CheckedInVersion.Name;
                    if (!(fe.DebugVersion is null)) drFlows["DebugVersion"] = fe.CheckedInVersion.Name;
                    drFlows["Deleted"] = fe.Deleted.ToString();
                    drFlows["Description"] = fe.Description;
                    if (!(fe.PublishedBy is null)) drFlows["PublishedBy"] = fe.PublishedBy.Name;
                    if (!(fe.PublishedVersion is null)) drFlows["PublishedVersion"] = fe.PublishedVersion.Name;
                    if (!(fe.SavedVersion is null)) drFlows["SavedVersion"] = fe.SavedVersion.Name;
                    if (!(fe.SupportedLanguages is null)) drFlows["SupportedLanguages"] = fe.SupportedLanguages.ToArray().ToString();
                    drFlows["Type"] = fe.Type.ToString();
                    dtFlows.Rows.Add(drFlows);
                    if (logMode.Contains("Debug")) Console.WriteLine(fe.ToJson());
                    /*
                     sql.CommandText = "INSERT INTO gen.gcFlows (FlowId,FlowName,Active,CheckedInVersion,DebugVersion,Deleted,[Description],Division,PublishedBy,PublishedVersion,SavedVersion,SupportedLanguages,[Type]) VALUES ("
                        + "'" + fe.Id +"',"
                        + "'" + fe.Name.Replace("'","''") + "',"
                        + "'" + fe.Active.ToString() + "',"
                        + ((fe.CheckedInVersion is null) ? "null," : ("'" + fe.CheckedInVersion.Name) + "',")
                        + ((fe.DebugVersion is null) ? "null," : ("'" + fe.DebugVersion.Name) + "',")
                        + "'" + fe.Deleted.ToString() + "',"
                        + ((fe.Description is null) ? "null," : ("'" + fe.Description.Replace("'", "''") + "',"))
                        + ((fe.Division is null) ? "null," : ("'" + fe.Division.Name.Replace("'", "''") + "',"))
                        + ((fe.PublishedBy is null) ? "null," : ("'" + fe.PublishedBy.Name.Replace("'", "''") + "',"))
                        + ((fe.PublishedVersion is null) ? "null," : ("'" + fe.PublishedVersion.Name.Replace("'", "''") + "',"))
                        + ((fe.SavedVersion is null) ? "null," : ("'" + fe.SavedVersion.Name) + "',")
                        + ((fe.SupportedLanguages is null) ? "null," : ("'" + fe.SupportedLanguages.ToArray().ToString()) + "',")
                        + "'" + fe.Type.ToString() + "'"
                        + ")";
                    Console.WriteLine($"Executing: {sql.CommandText}");
                    Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                    */
                }
            }
            /*
             * Flow Milestones are not enabled for Serco
            Console.WriteLine("Creating Flows Milestones Query...");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Flow Milestones...");
            sql.CommandText = "TRUNCATE TABLE gen.gcFlowMilestones";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            Console.WriteLine("Creating Flow Milestones Query...");
            FlowMilestoneListing fms = archApi.GetFlowsMilestones(pageSize: 1000);
            if (fms.Total > 0)
            {
                foreach (FlowMilestone fm in fms.Entities)
                {
                    sql.CommandText = "INSERT INTO gen.gcFlowMilestones (FlowMilestoneId,FlowMilestoneName,Description) VALUES ("
                        + "'" + fm.Id + "',"
                        + "'" + fm.Name.Replace("'", "''") + "',"
                        + "'" + fm.Description.Replace("'", "''") + "'"
                        + ")";
                    Console.WriteLine($"Executing: {sql.CommandText}");
                    Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                }

            }
            */

            Console.WriteLine("Creating Flows Outcomes Query...");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Flow Outcomes...");
            sql.CommandText = "TRUNCATE TABLE gen.gcFlowOutcomes";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtFlowOutcomes = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcFlowOutcomes";
            da.Fill(dtFlowOutcomes);

            Console.WriteLine("Creating Flow Outcomes Query...");
            FlowOutcomeListing fos = archApi.GetFlowsOutcomes(pageSize: 100);
            Console.WriteLine($"Writing total of {fos.Total} Flow Outcomes (no paging)...");
            if (fos.Total > 0)
            {
                foreach (FlowOutcome fo in fos.Entities)
                {
                    DataRow drFlowOutcomes = dtFlowOutcomes.NewRow();
                    drFlowOutcomes["FlowOutcomeId"] = fo.Id;
                    drFlowOutcomes["FlowOutcomeName"] = fo.Name;
                    drFlowOutcomes["Description"] = fo.Description;
                    dtFlowOutcomes.Rows.Add(drFlowOutcomes);
                    if (logMode.Contains("Debug")) Console.WriteLine(fo.ToJson());
                    /*
                    sql.CommandText = "INSERT INTO gen.gcFlowOutcomes (FlowOutcomeId,FlowOutcomeName,Description) VALUES ("
                        + "'" + fo.Id + "',"
                        + "'" + fo.Name.Replace("'", "''") + "',"
                        + "'" + fo.Description.Replace("'", "''") + "'"
                        + ")";
                    Console.WriteLine($"Executing: {sql.CommandText}");
                    Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                    */
                }
            }

            dtFlows.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcFlows";
            bulkCopy.WriteToServer(dtFlows);

            dtFlowOutcomes.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcFlowOutcomes";
            bulkCopy.WriteToServer(dtFlowOutcomes);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateEdges(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Edges Population...");
            TelephonyProvidersEdgeApi telApi = new TelephonyProvidersEdgeApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Edges...");
            sql.CommandText = "TRUNCATE TABLE gen.gcEdges";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtEdges = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcEdges";
            da.Fill(dtEdges);

            Console.WriteLine("Creating Edges Query...");
            EdgeEntityListing edges = telApi.GetTelephonyProvidersEdges(pageSize: 100);
            Console.WriteLine($"Writing total of {edges.Total} Edges (no paging)...");
            if (edges.Total > 0)
            {
                foreach (Edge e in edges.Entities)
                {
                    DataRow drEdges = dtEdges.NewRow();
                    drEdges["EdgeId"] = e.Id;
                    drEdges["EdgeName"] = e.Name;
                    drEdges["ApiVersion"] = e.ApiVersion;
                    if (!(e.CallDrainingState is null)) drEdges["CallDrainingState"] = e.CallDrainingState.ToString();
                    drEdges["ConversationCount"] = (object) e.ConversationCount ?? DBNull.Value;
                    drEdges["CreatedBy"] = e.CreatedBy;
                    drEdges["CreatedByApp"] = e.CreatedByApp;
                    drEdges["CurrentVersion"] = e.CurrentVersion;
                    if (!(e.DateCreated is null)) drEdges["DateCreated"] = e.DateCreated;
                    if (!(e.DateModified is null)) drEdges["DateModified"] = e.DateModified;
                    drEdges["Description"] = e.Description;
                    if (!(e.EdgeDeploymentType is null)) drEdges["EdgeDeploymentType"] = e.EdgeDeploymentType.ToString();
                    if (!(e.EdgeGroup is null)) drEdges["EdgeGroupId"] = e.EdgeGroup.Id;
                    if (!(e.EdgeGroup is null)) drEdges["EdgeGroupName"] = e.EdgeGroup.Name;
                    drEdges["Fingerprint"] = e.Fingerprint;
                    drEdges["FingerprintHint"] = e.FingerprintHint;
                    drEdges["FullSoftwareVersion"] = e.FullSoftwareVersion;
                    if (!(e.Interfaces is null)) drEdges["InterfacesCount"] = (object) e.Interfaces.Count ?? DBNull.Value;
                    drEdges["Make"] = e.Make;
                    if (!(e.Managed is null)) drEdges["Managed"] = e.Managed.ToString();
                    drEdges["Model"] = e.Model;
                    drEdges["ModifiedBy"] = e.ModifiedBy;
                    drEdges["ModifiedByApp"] = e.ModifiedByApp;
                    if (!(e.OfflineConfigCalled is null)) drEdges["OfflineConfigCalled"] = e.OfflineConfigCalled.ToString();
                    if (!(e.OnlineStatus is null)) drEdges["OnlineStatus"] = e.OnlineStatus.ToString();
                    drEdges["OsName"] = e.OsName;
                    drEdges["PairingId"] = e.PairingId;
                    drEdges["Patch"] = e.Patch;
                    if (!(e.PhysicalEdge is null)) drEdges["PhysicalEdge"] = e.PhysicalEdge.ToString();
                    drEdges["Proxy"] = e.Proxy;
                    drEdges["SerialNumber"] = e.SerialNumber;
                    if (!(e.Site is null)) drEdges["SiteId"] = e.Site.Id;
                    if (!(e.Site is null)) drEdges["SiteName"] = e.Site.Name;
                    if (!(e.Site is null)) drEdges["SiteDescription"] = e.Site.Description;
                    if (!(e.Site is null) && !(e.Site.Location is null)) drEdges["SiteLocationId"] = e.Site.Location.Id;
                    if (!(e.Site is null) && !(e.Site.Location is null)) drEdges["SiteLocationName"] = e.Site.Location.Name;
                    drEdges["SoftwareVersion"] = e.SoftwareVersion;
                    drEdges["SoftwareVersionConfiguration"] = e.SoftwareVersionConfiguration;
                    drEdges["SoftwareVersionPlatform"] = e.SoftwareVersionPlatform;
                    drEdges["SoftwareVersionTimestamp"] = e.SoftwareVersionTimestamp;
                    drEdges["StagedVersion"] = e.StagedVersion;
                    if (!(e.State is null)) drEdges["State"] = e.State.ToString();
                    if (!(e.StatusCode is null)) drEdges["StatusCode"] = e.StatusCode.ToString();
                    drEdges["Version"] = e.Version;
                    dtEdges.Rows.Add(drEdges);
                    if (logMode.Contains("Debug")) Console.WriteLine(e.ToJson());
                }
            }

            dtEdges.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcEdges";
            bulkCopy.WriteToServer(dtEdges);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateLines(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Lines Population...");
            TelephonyProvidersEdgeApi telApi = new TelephonyProvidersEdgeApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Lines...");
            sql.CommandText = "TRUNCATE TABLE gen.gcLines";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtLines = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcLines";
            da.Fill(dtLines);

            Console.WriteLine("Creating Lines Query...");

            LineEntityListing lines = new LineEntityListing(PageCount: 0, PageNumber: 0);
            int currPage = 1;
            do
            {
                // https://developer.genesys.cloud/api/rest/v2/telephonyprovidersedge/#get-api-v2-telephony-providers-edges-lines
                lines = telApi.GetTelephonyProvidersEdgesLines(pageSize: 100, pageNumber: currPage) ;
                Console.WriteLine($"Writing page {lines.PageNumber}/{lines.PageCount} with total of {lines.Total} Lines...");
                currPage += 1;
                if (lines.Total > 0)
                {
                    foreach (Line l in lines.Entities)
                    {
                        DataRow drLines = dtLines.NewRow();
                        drLines["LineId"] = l.Id;
                        drLines["LineName"] = l.Name;
                        drLines["CreatedBy"] = l.CreatedBy;
                        drLines["CreatedByApp"] = l.CreatedByApp;
                        drLines["DateCreated"] = l.DateCreated;
                        drLines["DateModified"] = l.DateModified;
                        drLines["Description"] = l.Description;
                        if (!(l.LineBaseSettings is null)) drLines["LineBaseSettingsId"] = l.LineBaseSettings.Id;
                        if (!(l.LineBaseSettings is null)) drLines["LineBaseSettingsName"] = l.LineBaseSettings.Name;
                        if (!(l.Site is null)) drLines["SiteId"] = l.Site.Id;
                        if (!(l.Site is null)) drLines["SiteName"] = l.Site.Name;
                        dtLines.Rows.Add(drLines);
                        if (logMode.Contains("Debug")) Console.WriteLine(l.ToJson());

                    }
                }
            } while (lines.PageNumber < lines.PageCount);

            dtLines.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcLines";
            bulkCopy.WriteToServer(dtLines);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateSites(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Sites Population...");
            TelephonyProvidersEdgeApi telApi = new TelephonyProvidersEdgeApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Sites...");
            sql.CommandText = "TRUNCATE TABLE gen.gcSites";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtSites = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcSites";
            da.Fill(dtSites);

            Console.WriteLine("Creating Sites Query...");
            // https://developer.genesys.cloud/api/rest/v2/telephonyprovidersedge/#get-api-v2-telephony-providers-edges-sites
            SiteEntityListing sites = telApi.GetTelephonyProvidersEdgesSites(pageSize: 100);
            Console.WriteLine($"Writing total of {sites.Total} Sites (no paging)...");
            if (sites.Total > 0)
            {
                foreach (Site s in sites.Entities)
                {
                    DataRow drSites = dtSites.NewRow();
                    drSites["SiteId"] = s.Id;
                    drSites["SiteName"] = s.Name;
                    drSites["CoreSite"] = s.CoreSite.Value;
                    drSites["Description"] = s.Description;
                    drSites["AddressCountry"] = s.Location.Address.Country;
                    drSites["AddressCity"] = s.Location.Address.City;
                    drSites["AddressState"] = s.Location.Address.State;
                    drSites["AddressStreet1"] = s.Location.Address.Street1;
                    drSites["AddressStreet2"] = s.Location.Address.Street2;
                    drSites["AddressZipcode"] = s.Location.Address.Zipcode;
                    drSites["LocationName"] = s.Location.Name;
                    drSites["Managed"] = s.Managed.Value;
                    drSites["MediaModel"] = s.MediaModel.Value.ToString();
                    drSites["Version"] = s.Version;
                    dtSites.Rows.Add(drSites);
                    if (logMode.Contains("Debug")) Console.WriteLine(s.ToJson());
                }
            }

            dtSites.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcSites";
            bulkCopy.WriteToServer(dtSites);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulatePhones(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Edges Population...");
            TelephonyProvidersEdgeApi telApi = new TelephonyProvidersEdgeApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Edge Phones...");
            sql.CommandText = "TRUNCATE TABLE gen.gcPhones";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtPhones = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcPhones";
            da.Fill(dtPhones);

            Console.WriteLine("Creating Phones Query...");
            int iPage = 1;
            int iPageSize = 100;
            do
            {
                // https://developer.genesys.cloud/api/rest/v2/telephonyprovidersedge/#get-api-v2-telephony-providers-edges-phones
                PhoneEntityListing phones = telApi.GetTelephonyProvidersEdgesPhones(pageNumber: iPage, pageSize: iPageSize, expand: new List<string>() { "properties", "site", "status", "status.primaryEdgesStatus", "status.secondaryEdgesStatus", "phoneBaseSettings", "lines" });
                Console.WriteLine($"Writing page {phones.PageNumber}/{phones.PageCount} with total of {phones.Total} Phones...");
                if (phones.Total > 0)
                {
                    foreach (Phone p in phones.Entities)
                    {
                        DataRow drPhones = dtPhones.NewRow();
                        drPhones["PhoneId"] = p.Id;
                        drPhones["PhoneName"] = p.Name;
                        if (!(p.Capabilities is null)) drPhones["CapabilityAllowReboot"] = p.Capabilities.AllowReboot.ToString();
                        if (!(p.Capabilities is null)) drPhones["CapabilityCdm"] = p.Capabilities.Cdm.ToString();
                        if (!(p.Capabilities is null)) drPhones["CapabilityDualRegisters"] = p.Capabilities.DualRegisters.ToString();
                        if (!(p.Capabilities is null)) drPhones["CapabilityHardwareIdType"] = p.Capabilities.HardwareIdType;
                        if (!(p.Capabilities is null) && !(p.Capabilities.MediaCodecs is null)) drPhones["CapabilityMediaCodecs"] = p.Capabilities.MediaCodecs.ToArray().ToString();
                        if (!(p.Capabilities is null)) drPhones["CapabilityNoCloudProvisioning"] = p.Capabilities.NoCloudProvisioning.ToString();
                        if (!(p.Capabilities is null)) drPhones["CapabilityNoRebalance"] = p.Capabilities.NoRebalance.ToString();
                        if (!(p.Capabilities is null)) drPhones["CapabilityProvisions"] = p.Capabilities.Provisions.ToString();
                        if (!(p.Capabilities is null)) drPhones["CapabilityRegisters"] = p.Capabilities.Registers.ToString();
                        drPhones["CreatedBy"] = p.CreatedBy;
                        drPhones["CreatedByApp"] = p.CreatedByApp;
                        drPhones["DateCreated"] = p.DateCreated;
                        drPhones["DateModified"] = p.DateModified;
                        drPhones["Description"] = p.Description;
                        if (!(p.LineBaseSettings is null)) drPhones["LineBaseSettingsId"] = p.LineBaseSettings.Id;
                        if (!(p.LineBaseSettings is null)) drPhones["LineBaseSettingsName"] = p.LineBaseSettings.Name;
                        if (!(p.Lines is null)) drPhones["FirstLineId"] = p.Lines[0].Id;
                        if (!(p.Lines is null)) drPhones["FirstLineName"] = p.Lines[0].Name;
                        if (!(p.Lines is null)) drPhones["FirstLineDecription"] = p.Lines[0].Description;
                        if (!(p.Lines is null) && !(p.Lines[0].LoggedInUser is null)) drPhones["FirstLineLoggedInUserId"] = p.Lines[0].LoggedInUser.Id;
                        if (!(p.Lines is null) && !(p.Lines[0].LoggedInUser is null)) drPhones["FirstLineLoggedInUserName"] = p.Lines[0].LoggedInUser.Name;
                        if (!(p.Lines is null) && !(p.Lines[0].PrimaryEdge is null)) drPhones["FirstLinePriEdgeId"] = p.Lines[0].PrimaryEdge.Id;
                        if (!(p.Lines is null) && !(p.Lines[0].PrimaryEdge is null)) drPhones["FirstLinePriEdgeName"] = p.Lines[0].PrimaryEdge.Name;
                        if (!(p.Lines is null) && !(p.Lines[0].Site is null)) drPhones["FirstLineSiteId"] = p.Lines[0].Site.Id;
                        if (!(p.Lines is null) && !(p.Lines[0].Site is null)) drPhones["FirstLineSiteName"] = p.Lines[0].Site.Name;
                        if (!(p.Lines is null)) drPhones["FirstLineState"] = p.Lines[0].State.ToString();
                        drPhones["ModifiedBy"] = p.ModifiedBy;
                        drPhones["ModifiedByApp"] = p.ModifiedByApp;
                        if (!(p.PhoneBaseSettings is null)) drPhones["PhoneBaseSettingsId"] = p.PhoneBaseSettings.Id;
                        if (!(p.PhoneBaseSettings is null)) drPhones["PhoneBaseSettingsName"] = p.PhoneBaseSettings.Name;
                        if (!(p.PrimaryEdge is null)) drPhones["PrimaryEdgeId"] = p.PrimaryEdge.Id;
                        if (!(p.PrimaryEdge is null)) drPhones["PrimaryEdgeName"] = p.PrimaryEdge.Name;
                        //if (!(p.Properties is null)) drPhones["Properties"] = string.Join(",", p.Properties.Values);
                        if (!(p.SecondaryEdge is null)) drPhones["SecondaryEdgeId"] = p.SecondaryEdge.Id;
                        if (!(p.SecondaryEdge is null)) drPhones["SecondaryEdgeName"] = p.SecondaryEdge.Name;
                        if (!(p.SecondaryStatus is null) && !(p.SecondaryStatus.OperationalStatus is null)) drPhones["SecondaryStatus"] = p.SecondaryStatus.OperationalStatus.ToString();
                        drPhones["SiteId"] = p.Site.Id;
                        drPhones["SiteName"] = p.Site.Name;
                        if (p.Lines.Count > 1)
                        {
                            Console.WriteLine("!!");
                        }
                        if (!(p.State is null)) drPhones["State"] = p.State.ToString();
                        if (!(p.Status is null)) drPhones["OperationalStatus"] = p.Status.OperationalStatus;
                        if (!(p.Status is null)) drPhones["EdgesStatus"] = p.Status.EdgesStatus;

                        if (!(p.UserAgentInfo is null)) drPhones["UserAgentInfoFirmwareVersion"] = p.UserAgentInfo.FirmwareVersion;
                        if (!(p.UserAgentInfo is null)) drPhones["UserAgentInfoManufacturer"] = p.UserAgentInfo.Manufacturer;
                        if (!(p.UserAgentInfo is null)) drPhones["UserAgentInfoModel"] = p.UserAgentInfo.Model;

                        if (!(p.Version is null)) drPhones["Version"] = p.Version;
                        if (!(p.WebRtcUser is null)) drPhones["WebRtcUserId"] = p.WebRtcUser.Id;
                        if (!(p.WebRtcUser is null)) drPhones["WebRtcUserName"] = p.WebRtcUser.Name;

                        dtPhones.Rows.Add(drPhones);
                        if (logMode.Contains("Debug")) Console.WriteLine(p.ToJson());
                    }
                    if (iPage >= phones.PageCount) break;
                }
                else break;
                iPage += 1;
            } while (true);

            dtPhones.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcPhones";
            bulkCopy.WriteToServer(dtPhones);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateOutboundMappings(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Outbound Mappings Population...");
            OutboundApi outApi = new OutboundApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Outbound Mappings...");
            sql.CommandText = "TRUNCATE TABLE gen.gcOutboundMappings";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtOBMaps = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcOutboundMappings";
            da.Fill(dtOBMaps);

            Console.WriteLine("Creating Outbound Mappings Query...");
            // https://developer.genesys.cloud/api/rest/v2/outbound/#get-api-v2-outbound-wrapupcodemappings
            WrapUpCodeMapping wucm = outApi.GetOutboundWrapupcodemappings();
            if (!(wucm.Mapping is null) && (wucm.Mapping.Count > 0))
            {
                foreach (KeyValuePair<string, List<string>> m in wucm.Mapping)
                {
                    foreach (String v in m.Value)
                    {
                        DataRow drOBMaps = dtOBMaps.NewRow();
                        drOBMaps["WrapupCodeId"] = m.Key;
                        drOBMaps["OutboundMappingName"] = v;
                        dtOBMaps.Rows.Add(drOBMaps);
                        if (logMode.Contains("Debug")) Console.WriteLine("Outbound Mapping: " + m.Key + v);
                    }
                }
            }

            dtOBMaps.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcOutboundMappings";
            bulkCopy.WriteToServer(dtOBMaps);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateOutboundCampaignRules(string dbconnstring, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Outbound RuleSet Population...");
            OutboundApi outApi = new OutboundApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Outbound RuleSet...");
            sql.CommandText = "TRUNCATE TABLE gen.gcOutboundRulesets";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtOBRulesets = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcOutboundRulesets";
            da.Fill(dtOBRulesets);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting old list of Outbound Campaign Rules...");
            sql.CommandText = "TRUNCATE TABLE gen.gcOutboundCampaignRules";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            System.Data.DataTable dtOBCampRules = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcOutboundCampaignRules";
            da.Fill(dtOBCampRules);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine("Creating Outbound RuleSet Query...");
            // https://developer.genesys.cloud/api/rest/v2/outbound/#get-api-v2-outbound-rulesets
            RuleSetEntityListing rules = outApi.GetOutboundRulesets(100);
            if (!(rules.Entities is null) && (rules.Entities.Count > 0))
            {
                foreach (RuleSet r in rules.Entities)
                {
                    string strRuleActions = "";
                    if (!(r.Rules is null))
                    {
                        foreach (DialerRule dr in r.Rules)
                        {
                            strRuleActions += dr.Order.ToString() + ". " + dr.Name + " (" + dr.Category.ToString() + ");";
                        }
                    }
                    DataRow drOBRulesets = dtOBRulesets.NewRow();
                    drOBRulesets["OutboundCampaignRulesId"] = r.Id;
                    drOBRulesets["OutboundCampaignRulesName"] = r.Name;
                    if (!(r.ContactList is null)) drOBRulesets["ContactListId"] = r.ContactList.Id;
                    if (!(r.ContactList is null)) drOBRulesets["ContactListName"] = r.ContactList.Name;
                    drOBRulesets["DateCreated"] = r.DateCreated;
                    drOBRulesets["DateModified"] = r.DateModified;
                    if (!(r.Queue is null)) drOBRulesets["QueueId"] = r.Queue.Id;
                    if (!(r.Queue is null)) drOBRulesets["QueueName"] = r.Queue.Name;
                    if (!(r.Rules is null)) drOBRulesets["Actions"] = strRuleActions;
                    drOBRulesets["Version"] = r.Version;
                    dtOBRulesets.Rows.Add(drOBRulesets);
                    if (logMode.Contains("Debug")) Console.WriteLine(r.ToJson());
                }
            }

            Console.WriteLine("Creating Outbound Campaign Rules Query...");
            // https://developer.genesys.cloud/api/rest/v2/outbound/#get-api-v2-outbound-campaignrules
            CampaignRuleEntityListing camprules = outApi.GetOutboundCampaignrules(pageSize: 100);
            foreach (CampaignRule cr in camprules.Entities)
            {
                if (!(cr.CampaignRuleEntities is null))
                {
                    foreach (DomainEntityRef camp in cr.CampaignRuleEntities.Campaigns)
                    {
                        DataRow drOBCampRules = dtOBCampRules.NewRow();
                        drOBCampRules["OutboundCampaignRuleId"] = cr.Id;
                        drOBCampRules["OutboundCampaignRuleName"] = cr.Name;
                        drOBCampRules["DateCreated"] = cr.DateCreated;
                        drOBCampRules["DateModified"] = cr.DateModified;
                        drOBCampRules["Enabled"] = cr.Enabled;
                        drOBCampRules["MatchAnyConditions"] = cr.MatchAnyConditions;
                        drOBCampRules["Version"] = cr.Version;
                        drOBCampRules["OutboundCampaignId"] = camp.Id;
                        drOBCampRules["OutboundCampaignName"] = camp.Name;
                        dtOBCampRules.Rows.Add(drOBCampRules);
                    }

                    foreach (DomainEntityRef camp in cr.CampaignRuleEntities.Sequences)
                    {
                        DataRow drOBCampRules = dtOBCampRules.NewRow();
                        drOBCampRules["OutboundCampaignRuleId"] = cr.Id;
                        drOBCampRules["OutboundCampaignRuleName"] = cr.Name;
                        drOBCampRules["DateCreated"] = cr.DateCreated;
                        drOBCampRules["DateModified"] = cr.DateModified;
                        drOBCampRules["Enabled"] = cr.Enabled;
                        drOBCampRules["MatchAnyConditions"] = cr.MatchAnyConditions;
                        drOBCampRules["Version"] = cr.Version;
                        drOBCampRules["SequenceId"] = camp.Id;
                        drOBCampRules["SequenceName"] = camp.Name;
                        dtOBCampRules.Rows.Add(drOBCampRules);
                    }
                }
            }

            dtOBRulesets.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcOutboundRulesets";
            bulkCopy.WriteToServer(dtOBRulesets);

            dtOBCampRules.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcOutboundCampaignRules";
            bulkCopy.WriteToServer(dtOBCampRules);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateConversations(string dbconnstring, string intervalFilter, string useConvJobId="", string logMode = "Standard")
        {
            Console.WriteLine(DateTime.Now.ToString("s") + " - Initializing Conversations Population...");
            AnalyticsApi analApi = new AnalyticsApi();
            RecordingApi recApi = new RecordingApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            string strIntervalStart = intervalFilter.Split('/')[0];
            DateTime dIntervalStartUTC = DateTime.Parse(strIntervalStart);
            string strIntervalEnd = intervalFilter.Split('/')[1];
            DateTime dIntervalEndUTC = DateTime.Parse(strIntervalEnd);
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine("Deleting previous gcConversationDivisions:");
            sql.CommandText = $"DELETE FROM gen.gcConversationDivisions where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtDivisions = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcConversationDivisions where ConversationId = '1'";
            da.Fill(dtDivisions);

            Console.WriteLine("Deleting previous gcConversationEvaluations:");
            sql.CommandText = $"DELETE FROM gen.gcConversationEvaluations where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            Console.WriteLine("Deleting previous gcConversationEvalQGScores:");
            sql.CommandText = $"DELETE FROM gen.gcConversationEvalQGScores where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            Console.WriteLine("Deleting previous gcConversationEvalQScores:");
            sql.CommandText = $"DELETE FROM gen.gcConversationEvalQScores where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");


            Console.WriteLine("Deleting previous gcConversationParticipants:");
            sql.CommandText = $"DELETE FROM gen.gcConversationParticipants where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtParticipants = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcConversationParticipants where ConversationId = '1'";
            da.Fill(dtParticipants);

            Console.WriteLine("Deleting previous gcConversationSessions:");
            sql.CommandText = $"DELETE FROM gen.gcConversationSessions where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtSessions = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcConversationSessions where ConversationId = '1'";
            da.Fill(dtSessions);

            Console.WriteLine("Deleting previous gcConversationSegments:");
            sql.CommandText = $"DELETE FROM gen.gcConversationSegments where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtSegments = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcConversationSegments where ConversationId = '1'";
            da.Fill(dtSegments);

            Console.WriteLine("Deleting previous gcConversationRecordings:");
            sql.CommandText = $"DELETE FROM gen.gcConversationRecordings where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtRecordings = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcConversationRecordings where ConversationId = '1'";
            da.Fill(dtRecordings);

            Console.WriteLine("Deleting previous gcConversationFlowOutcomes:");
            sql.CommandText = $"DELETE FROM gen.gcConversationFlowOutcomes where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtFlowOutcomes = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcConversationFlowOutcomes where ConversationId = '1'";
            da.Fill(dtFlowOutcomes);

            Console.WriteLine("Deleting previous gcConversationResolutions:");
            sql.CommandText = $"DELETE FROM gen.gcConversationResolutions where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

            Console.WriteLine("Deleting previous gcConversations:");
            sql.CommandText = $"DELETE FROM gen.gcConversations where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            System.Data.DataTable dtConversations = new System.Data.DataTable();
            sql.CommandText = "SELECT TOP 1 * FROM gen.gcConversations where ConversationId = '1'";
            da.Fill(dtConversations);

            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine("Creating Conversations Query...");
            bool lastPage = false;
            int pageCounter = 1;
            int pageSize = 100;
            ConversationQuery abody = new ConversationQuery();
            abody.Interval = intervalFilter;
            abody.Paging = new PagingSpec(PageSize: pageSize);

            Console.WriteLine(DateTime.Now.ToString("s") + " - Retrieving results of ConversationDetails:");
            //AnalyticsConversationAsyncQueryResponse queryResults;

            do {
                abody.Paging = new PagingSpec(PageSize: pageSize, PageNumber: pageCounter);
                
                // The API call corresponds to the following web-rest API and populates based on the conversation detail model
                // https://developer.genesys.cloud/api/rest/v2/analytics/conversation
                // https://developer.genesys.cloud/api/rest/v2/analytics/conversation_detail_model
                AnalyticsConversationQueryResponse queryResults = analApi.PostAnalyticsConversationsDetailsQuery(abody);

                if (queryResults.Conversations is null)
                {
                    Console.WriteLine("No Results");
                    lastPage = true;
                    return;
                }

                Console.WriteLine(DateTime.Now.ToString("s") + " - Writing " + queryResults.Conversations.Count + " Conversations to gen.gcConversations...");
                foreach (AnalyticsConversationWithoutAttributes conv in queryResults.Conversations)
                {
                    string headers = "", values = "";
                    DateTime ndIntervalStart = new DateTime(conv.ConversationStart.Value.Year, conv.ConversationStart.Value.Month, conv.ConversationStart.Value.Day, conv.ConversationStart.Value.Hour, (conv.ConversationStart.Value.Minute / 30) * 30, 0).ToLocalTime();
                    DateTime ndIntervalEnd = (conv.ConversationEnd.HasValue) ? (new DateTime(conv.ConversationEnd.Value.Year, conv.ConversationEnd.Value.Month, conv.ConversationEnd.Value.Day, conv.ConversationEnd.Value.Hour, (conv.ConversationEnd.Value.Minute / 30) * 30, 0).ToLocalTime()) : (DateTime.Now.AddDays(100));

                    DataRow drConversations = dtConversations.NewRow();
                    drConversations["ConversationId"] = conv.ConversationId;
                    if ((conv.ConversationStart.HasValue)) drConversations["ConversationStart"] = conv.ConversationStart.Value.ToLocalTime();
                    if ((conv.ConversationEnd.HasValue))   drConversations["ConversationEnd"] = conv.ConversationEnd.Value.ToLocalTime();
                    if ((conv.ConversationStart.HasValue)) drConversations["dIntervalStart"] = ndIntervalStart;
                    if ((conv.ConversationEnd.HasValue))   drConversations["dIntervalEnd"] = ndIntervalEnd;
                    if ((conv.ConversationStart.HasValue)) drConversations["dIntervalStartUTC"] = ndIntervalStart.ToUniversalTime();
                    if ((conv.ConversationEnd.HasValue)) drConversations["dIntervalEndUTC"] = ndIntervalEnd.ToUniversalTime();
                    if ((conv.ConversationStart.HasValue)) drConversations["ConversationStartUTC"] = conv.ConversationStart.Value.ToUniversalTime();
                    if ((conv.ConversationEnd.HasValue)) drConversations["ConversationEndUTC"] = conv.ConversationEnd.Value.ToUniversalTime();
                    drConversations["MediaStatsMinConversationMos"] = (conv.MediaStatsMinConversationMos ?? 0);
                    drConversations["MediaStatsMinConversationRFactor"] = (conv.MediaStatsMinConversationRFactor ?? 0);
                    drConversations["OriginatingDirection"] = conv.OriginatingDirection.ToString();
                    dtConversations.Rows.Add(drConversations);

                    headers += "ConversationId,ConversationStart,ConversationEnd,dIntervalStart,dIntervalEnd,dIntervalStartUTC,dIntervalEndUTC,ConversationStartUTC,ConversationEndUTC,"
                        + "MediaStatsMinConversationMos,MediaStatsMinConversationRFactor,OriginatingDirection";
                    values += "'" + conv.ConversationId.ToString() + "',";
                    values += "'" + conv.ConversationStart.Value.ToLocalTime().ToString("s") + "',";
                    values += (conv.ConversationEnd.HasValue) ? ("'" + conv.ConversationEnd.Value.ToLocalTime().ToString("s") + "',") : "null,";
                    values += "'" + ndIntervalStart.ToString("s") + "',";
                    values += (conv.ConversationEnd.HasValue) ? ("'" + ndIntervalEnd.ToString("s") + "',") : "null,";
                    values += "'" + ndIntervalStart.ToUniversalTime().ToString("s") + "',";
                    values += (conv.ConversationEnd.HasValue) ? ("'" + ndIntervalEnd.ToUniversalTime().ToString("s") + "',") : "null,";
                    values += "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',";
                    values += (conv.ConversationEnd.HasValue) ? ("'" + conv.ConversationEnd.Value.ToUniversalTime().ToString("s") + "',") : "null,";
                    values += " " + (conv.MediaStatsMinConversationMos ?? 0) + ",";
                    values += " " + (conv.MediaStatsMinConversationRFactor ?? 0) + ",";
                    values += "'" + conv.OriginatingDirection.ToString() + "'";
                    //values += "'" + ((conv.Surveys == null) ? "" : conv.Surveys.ToString()) + "'";

                    sql.CommandText = "INSERT INTO gen.gcConversations (" + headers + ") VALUES (" + values + ");";
                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                    //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

                    Console.WriteLine(DateTime.Now.ToString("s") + " - Writing to gen.gcConversationDivisions...");
                    foreach (string divId in conv.DivisionIds)
                    {
                        DataRow drDivisions = dtDivisions.NewRow();
                        drDivisions["ConversationId"] = conv.ConversationId;
                        if ((conv.ConversationStart.HasValue)) drDivisions["ConversationStartUTC"] = conv.ConversationStart.Value.ToUniversalTime();
                        drDivisions["DivisionId"] = divId;
                        dtDivisions.Rows.Add(drDivisions);

                        sql.CommandText = "INSERT INTO gen.gcConversationDivisions (ConversationId,ConversationStartUTC,DivisionId) VALUES (" 
                            + "'" + conv.ConversationId + "',"
                            + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                            + "'" + divId + "');";
                        if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                        //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                    }
                
                    if (conv.Evaluations != null)
                    {
                        Console.WriteLine(DateTime.Now.ToString("s") + " - Writing to gen.gcConversationEvaluations...");
                        
                        QualityApi qualApi = new QualityApi();
                        EvaluationEntityListing evals = qualApi.GetQualityEvaluationsQuery(expand: new List<string>() { "agent","queue","evaluator" }, conversationId: conv.ConversationId, expandAnswerTotalScores: true);
                        foreach (Evaluation eval in evals.Entities)
                        {
                            float TotalScore = 0;
                            float TotalCriticalScore = 0;
                            float TotalNonCriticalScore = 0;

                            float TotalScoreUnweighted = 0;
                            float TotalCriticalScoreUnweighted = 0;
                            float TotalNonCriticalScoreUnweighted = 0;

                            float MaxTotalScore = 0;
                            float MaxTotalCriticalScore = 0;
                            float MaxTotalNonCriticalScore = 0;

                            float MaxTotalScoreUnweighted = 0;
                            float MaxTotalCriticalScoreUnweighted = 0;
                            float MaxTotalNonCriticalScoreUnweighted = 0;

                            int TotalNumCriticalQuestions = 0;
                            int NumAnsweredCriticalQstns = 0;
                            int NumPosAnsweredCriticalQstns = 0;

                            if (!(eval.Answers is null))
                            {
                                foreach (EvaluationQuestionGroupScore eqg in eval.Answers.QuestionGroupScores)
                                {
                                    TotalScore += eqg.TotalScore.HasValue ? eqg.TotalScore.Value:0;
                                    TotalCriticalScore += eqg.TotalCriticalScore.HasValue ? eqg.TotalCriticalScore.Value : 0;
                                    TotalNonCriticalScore += eqg.TotalNonCriticalScore.HasValue ? eqg.TotalNonCriticalScore.Value : 0;
                                    TotalScoreUnweighted += eqg.TotalScoreUnweighted.HasValue ? eqg.TotalScoreUnweighted.Value : 0;
                                    TotalCriticalScoreUnweighted += eqg.TotalCriticalScoreUnweighted.HasValue ? eqg.TotalCriticalScoreUnweighted.Value : 0;
                                    TotalNonCriticalScoreUnweighted += eqg.TotalNonCriticalScoreUnweighted.HasValue ? eqg.TotalNonCriticalScoreUnweighted.Value : 0;
                                    MaxTotalScore += eqg.MaxTotalScore.HasValue ? eqg.MaxTotalScore.Value : 0;
                                    MaxTotalCriticalScore += eqg.MaxTotalCriticalScore.HasValue ? eqg.MaxTotalCriticalScore.Value : 0;
                                    MaxTotalNonCriticalScore += eqg.MaxTotalNonCriticalScore.HasValue ? eqg.MaxTotalNonCriticalScore.Value : 0;
                                    MaxTotalScoreUnweighted += eqg.MaxTotalScoreUnweighted.HasValue ? eqg.MaxTotalScoreUnweighted.Value : 0;
                                    MaxTotalCriticalScoreUnweighted += eqg.MaxTotalCriticalScoreUnweighted.HasValue ? eqg.MaxTotalCriticalScoreUnweighted.Value : 0;
                                    MaxTotalNonCriticalScoreUnweighted += eqg.MaxTotalNonCriticalScoreUnweighted.HasValue ? eqg.MaxTotalNonCriticalScoreUnweighted.Value : 0;

                                    sql.CommandText = "INSERT INTO gen.gcConversationEvalQGScores ("
                                            + "ConversationId,ConversationEvaluationId,EvaluationFormId,EvaluationQuestionGroupId,ConversationStartUTC,MarkedNA,"
                                            + "TotalScore,TotalCriticalScore,TotalNonCriticalScore,TotalScoreUnweighted,TotalCriticalScoreUnweighted,TotalNonCriticalScoreUnweighted,MaxTotalScore,MaxTotalCriticalScore,MaxTotalNonCriticalScore,MaxTotalScoreUnweighted,MaxTotalCriticalScoreUnweighted,MaxTotalNonCriticalScoreUnweighted"
                                            + ") VALUES ("
                                            + "'" + conv.ConversationId + "',"
                                            + "'" + eval.Id + "',"
                                            + "'" + eval.EvaluationForm.Id + "',"
                                            + "'" + eqg.QuestionGroupId + "',"
                                            + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                                            + "'" + eqg.MarkedNA + "',"

                                            + "" + eqg.TotalScore + ","
                                            + "" + eqg.TotalCriticalScore + ","
                                            + "" + eqg.TotalNonCriticalScore + ","

                                            + "" + eqg.TotalScoreUnweighted + ","
                                            + "" + eqg.TotalCriticalScoreUnweighted + ","
                                            + "" + eqg.TotalNonCriticalScoreUnweighted + ","

                                            + "" + eqg.MaxTotalScore + ","
                                            + "" + eqg.MaxTotalCriticalScore + ","
                                            + "" + eqg.MaxTotalNonCriticalScore + ","
                                           
                                            + "" + eqg.MaxTotalScoreUnweighted + ","
                                            + "" + eqg.MaxTotalCriticalScoreUnweighted + ","
                                            + "" + eqg.MaxTotalNonCriticalScoreUnweighted + ""
                                            + ");";
                                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                    Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

                                    foreach (EvaluationQuestionScore eq in eqg.QuestionScores)
                                    {

                                        sql.CommandText = "INSERT INTO gen.gcConversationEvalQScores ("
                                            + "ConversationId,ConversationEvaluationId,EvaluationFormId,EvaluationQuestionGroupId,EvaluationQuestionId,AnswerId,ConversationStartUTC,Comments,FailedKillQuestion,MarkedNA,Score"
                                            + ") VALUES ("
                                            + "'" + conv.ConversationId + "',"
                                            + "'" + eval.Id + "',"
                                            + "'" + eval.EvaluationForm.Id + "',"
                                            + "'" + eqg.QuestionGroupId + "',"
                                            + "'" + eq.QuestionId + "',"
                                            + "'" + eq.AnswerId + "',"
                                            + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                                            + "'" + eq.Comments.Replace("'", "''") + "',"
                                            + "'" + eq.FailedKillQuestion + "',"
                                            + "'" + eq.MarkedNA + "',"
                                            + (eq.Score??0) + ""
                                            + ");";
                                        if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                        Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                                    }
                                }
                            }

                            sql.CommandText = "INSERT INTO gen.gcConversationEvaluations ("
                                + "ConversationId,ConversationEvaluationId,CalibrationId,ConversationStartUTC,AgentHasRead,AssignedDateUTC,ChangedDateUTC,ConversationDateUTC,ConversationEndDateUTC,Comments,AgentComments,Status,EvaluatorId,EvaluatorName,EvaluationFormId,EvaluationFormName,TotalCriticalScorePct,TotalNonCriticalScorePct,TotalScorePct,AnyFailedKillQuestions,QueueId,QueueName,Rescored,UserId,UserName"
                                + ",TotalScore,TotalCriticalScore,TotalNonCriticalScore,TotalScoreUnweighted,TotalCriticalScoreUnweighted,TotalNonCriticalScoreUnweighted,MaxTotalScore,MaxTotalCriticalScore,MaxTotalNonCriticalScore,MaxTotalScoreUnweighted,MaxTotalCriticalScoreUnweighted,MaxTotalNonCriticalScoreUnweighted"
                                //+ ",TotalNumCriticalQuestions,NumAnsweredCriticalQstns,NumPosAnsweredCriticalQstns" 
                                + ") VALUES ("
                                + "'" + conv.ConversationId + "',"
                                + "'" + eval.Id + "',"
                                + ((eval.Calibration is null) ? "null," : ("'" + eval.Calibration.Id + "',")) 
                                + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                                + "'" + eval.AgentHasRead + "',"
                                + ((eval.AssignedDate.HasValue) ? ("'" + eval.AssignedDate.Value.ToUniversalTime().ToString("s") + "',"):"null,")
                                + ((eval.ChangedDate.HasValue) ?  ("'" + eval.ChangedDate.Value.ToUniversalTime().ToString("s") + "',"):"null,")
                                + ((eval.ConversationDate.HasValue) ?  ("'" + eval.ConversationDate.Value.ToUniversalTime().ToString("s") + "',"):"null,")
                                + ((eval.ConversationEndDate.HasValue) ? ("'" + eval.ConversationEndDate.Value.ToUniversalTime().ToString("s") + "',"):"null,")
                                + ((eval.Answers is null) ? "null," : ("'" + eval.Answers.Comments.Replace("'","''") + "',"))
                                + (((eval.Answers is null) || (eval.Answers.AgentComments is null) )? "null," : ("'" + eval.Answers.AgentComments.Replace("'", "''") + "',"))
                                + "'" + eval.Status.Value.ToString() + "',"
                                + ((eval.Evaluator is null) ? "null," : ("'" + eval.Evaluator.Id + "',"))
                                + ((eval.Evaluator is null) ? "null," : ("'" + eval.Evaluator.Name + "',"))
                                + "'" + eval.EvaluationForm.Id + "',"
                                + "'" + eval.EvaluationForm.Name + "',"
                                + ((eval.Answers is null) ? "null," : (eval.Answers.TotalCriticalScore + ","))
                                + ((eval.Answers is null) ? "null," : (eval.Answers.TotalNonCriticalScore + ","))
                                + ((eval.Answers is null) ? "null," : (eval.Answers.TotalScore + ","))
                                + ((eval.Answers is null) ? "null," : ("'" + eval.Answers.AnyFailedKillQuestions + "',"))
                                + ((eval.Queue is null) ? "null," : ("'" + eval.Queue.Id + "',"))
                                + ((eval.Queue is null) ? "null," : ("'" + eval.Queue.Name + "',"))
                                + "'" + (eval.Rescore??false) + "',"
                                + "'" + eval.Agent.Id + "',"
                                + "'" + eval.Agent.Name + "',"
                                
                                + "" + TotalScore + ","
                                + "" + TotalCriticalScore + ","
                                + "" + TotalNonCriticalScore + ","

                                + "" + TotalScoreUnweighted + ","
                                + "" + TotalCriticalScoreUnweighted + ","
                                + "" + TotalNonCriticalScoreUnweighted + ","

                                + "" + MaxTotalScore + ","
                                + "" + MaxTotalCriticalScore + ","
                                + "" + MaxTotalNonCriticalScore + ","

                                + "" + MaxTotalScoreUnweighted + ","
                                + "" + MaxTotalCriticalScoreUnweighted + ","
                                + "" + MaxTotalNonCriticalScoreUnweighted + ""
                            
                                //+ "" + TotalNumCriticalQuestions + ""
                                //+ "" + NumAnsweredCriticalQstns + ""
                                //+ "" + NumPosAnsweredCriticalQstns + ""
                                + ");";
                            if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

                        }
                    }

                    if (conv.Participants != null)
                    {
                        Console.WriteLine(DateTime.Now.ToString("s") + " - Writing to gen.gcConversationParticipants...");
                        foreach (AnalyticsParticipantWithoutAttributes party in conv.Participants)
                        {
                            //string attrs = "";
                            //if (party.Attributes != null)
                            //{
                            //    foreach (string key in party.Attributes.Keys)
                            //    {
                            //        attrs += key + ":" + party.Attributes[key] + ",";
                            //    }
                            //    if (attrs.EndsWith(",")) attrs = attrs.TrimEnd(',');
                            //}

                            DataRow drParticipants = dtParticipants.NewRow();
                            drParticipants["ConversationId"] = conv.ConversationId;
                            drParticipants["ParticipantId"] = party.ParticipantId;
                            if ((conv.ConversationStart.HasValue)) drParticipants["ConversationStartUTC"] = conv.ConversationStart.Value.ToUniversalTime();
                            drParticipants["ExternalContactId"] = party.ExternalContactId;
                            drParticipants["ExternalOrganizationId"] = party.ExternalOrganizationId;
                            if ((party.FlaggedReason.HasValue)) drParticipants["FlaggedReason"] = party.FlaggedReason.Value.ToString();
                            drParticipants["ParticipantName"] = party.ParticipantName;
                            drParticipants["Purpose"] = party.Purpose;
                            drParticipants["TeamId"] = party.TeamId;
                            drParticipants["UserId"] = party.UserId;
                            dtParticipants.Rows.Add(drParticipants);

                            sql.CommandText = "INSERT INTO gen.gcConversationParticipants ("
                                + "ConversationId,ParticipantId,ConversationStartUTC,ExternalContactId,ExternalOrganizationId,FlaggedReason,ParticipantName,Purpose,TeamId,UserId) VALUES ("
                                + "'" + conv.ConversationId + "',"
                                + "'" + party.ParticipantId + "',"
                                + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                                //+ (attrs.Length > 500 ? attrs.Substring(0, 500) : attrs) + "','"
                                + "'" + party.ExternalContactId + "',"
                                + "'" + party.ExternalOrganizationId + "',"
                                + "'" + (party.FlaggedReason.HasValue?party.FlaggedReason.Value.ToString():"") + "',"
                                + "'" + party.ParticipantName + "',"
                                + "'" + party.Purpose + "',"
                                + "'" + party.TeamId + "',"
                                + "'" + party.UserId + "'"
                                + ");";
                            if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                            //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

                            if (party.Sessions != null)
                            {
                                string sessheaders = "", sessvalues = "";
                                //Console.WriteLine(DateTime.Now.ToString("s") + " - Writing to gen.gcConversationSessions...");

                                foreach (AnalyticsSession sess in party.Sessions)
                                {
                                    AnalyticsConversationSegment qseg = sess.Segments.Find(x => !(x.QueueId is null));
                                    AnalyticsConversationSegment wseg = sess.Segments.FindLast(y => !(y.WrapUpCode is null));
                                    AnalyticsConversationSegment dseg = sess.Segments.FindLast(z => !(z.DisconnectType is null));
                                    string strFirstQueue = (qseg is null) ? null : qseg.QueueId;
                                    string strLastWrapUpCode = (wseg is null) ? null : wseg.WrapUpCode;
                                    string strLastDisconnectType = (dseg is null) ? null : dseg.DisconnectType.ToString();

                                    if (!(sess.Flow is null) && !(sess.Flow.Outcomes is null))
                                    {
                                        //Console.WriteLine(DateTime.Now.ToString("s") + " - Writing to gen.gcConversationFlowOutcomes...");
                                        foreach (AnalyticsFlowOutcome fo in sess.Flow.Outcomes)
                                        {
                                            DataRow drFlowOutcomes = dtFlowOutcomes.NewRow();
                                            drFlowOutcomes["ConversationId"] = conv.ConversationId;
                                            drFlowOutcomes["ParticipantId"] = party.ParticipantId;
                                            drFlowOutcomes["UserId"] = party.UserId;
                                            drFlowOutcomes["SessionId"] = sess.SessionId;
                                            drFlowOutcomes["FlowId"] = sess.Flow.FlowId;
                                            if ((conv.ConversationStart.HasValue)) drFlowOutcomes["ConversationStartUTC"] = conv.ConversationStart.Value.ToUniversalTime();
                                            if ((fo.FlowOutcomeStartTimestamp.HasValue)) drFlowOutcomes["FlowOutcomeStartTimestamp"] = fo.FlowOutcomeStartTimestamp.Value.ToUniversalTime();
                                            if ((fo.FlowOutcomeEndTimestamp.HasValue)) drFlowOutcomes["FlowOutcomeEndTimestamp"] = fo.FlowOutcomeEndTimestamp.Value.ToUniversalTime();
                                            drFlowOutcomes["FlowOutcomeId"] = fo.FlowOutcomeId;
                                            drFlowOutcomes["FlowOutcome"] = fo.FlowOutcome;
                                            drFlowOutcomes["FlowOutcomeValue"] = fo.FlowOutcomeValue;
                                            dtFlowOutcomes.Rows.Add(drFlowOutcomes);

                                            sql.CommandText = "INSERT INTO gen.gcConversationFlowOutcomes ("
                                                + "ConversationId,ParticipantId,UserId,SessionId,FlowId,ConversationStartUTC,FlowOutcomeStartTimestamp,FlowOutcomeEndTimestamp,FlowOutcomeId,FlowOutcome,FlowOutcomeValue"
                                                + ") VALUES ("
                                                + "'" + conv.ConversationId + "',"
                                                + "'" + party.ParticipantId + "',"
                                                + "'" + party.UserId + "',"
                                                + "'" + sess.SessionId + "',"
                                                + "'" + sess.Flow.FlowId + "',"
                                                + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                                                + ((fo.FlowOutcomeStartTimestamp.HasValue) ? ("'" + fo.FlowOutcomeStartTimestamp.Value.ToUniversalTime().ToString("s") + "',") : "null,")
                                                + ((fo.FlowOutcomeEndTimestamp.HasValue) ? ("'" + fo.FlowOutcomeEndTimestamp.Value.ToUniversalTime().ToString("s") + "',") : "null,")
                                                + "'" + fo.FlowOutcomeId + "',"
                                                + "'" + fo.FlowOutcome + "',"
                                                + "'" + fo.FlowOutcomeValue + "'"
                                                + ");";
                                            if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                            //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                                        }
                                    }

                                    // added 06-Oct-2021 to truncate the entryreason
                                    string EntryReason;
                                    if (!(sess.Flow is null) && !(sess.Flow.EntryReason is null) && (sess.Flow.EntryReason.Length > 49))
                                    {
                                        EntryReason = sess.Flow.EntryReason.Substring(0, 49);
                                    }
                                    else if (!(sess.Flow is null) && !(sess.Flow.EntryReason is null))
                                    {
                                        EntryReason = sess.Flow.EntryReason;
                                    }
                                    else EntryReason = "";

                                    DataRow drSessions = dtSessions.NewRow();
                                    drSessions["ConversationId"] = conv.ConversationId;
                                    drSessions["ParticipantId"] = party.ParticipantId;
                                    drSessions["UserId"] = party.UserId;
                                    drSessions["SessionId"] = sess.SessionId;
                                    drSessions["ConversationStartUTC"] = conv.ConversationStart.Value.ToUniversalTime();
                                    drSessions["AcwSkipped"] = (sess.AcwSkipped ?? false);
                                    drSessions["AddressFrom"] = sess.AddressFrom;
                                    drSessions["AddressOther"] = sess.AddressOther;
                                    drSessions["AddressSelf"] = sess.AddressSelf;
                                    drSessions["AddressTo"] = sess.AddressTo;
                                    drSessions["AgentAssistantId"] = sess.AgentAssistantId;
                                    drSessions["Ani"] = sess.Ani;
                                    drSessions["AssignerId"] = sess.AssignerId;
                                    if (!(sess.CallbackScheduledTime is null)) drSessions["CallbackScheduledTimeUTC"] = sess.CallbackScheduledTime.Value.ToUniversalTime();
                                    drSessions["CallbackUserName"] = sess.CallbackUserName;
                                    drSessions["CobrowseRole"] = sess.CobrowseRole;
                                    drSessions["CobrowseRoomId"] = sess.CobrowseRoomId;
                                    drSessions["Direction"] = sess.Direction.ToString();
                                    drSessions["DispositionAnalyzer"] = sess.DispositionAnalyzer;
                                    drSessions["DispositionName"] = sess.DispositionName;
                                    drSessions["Dnis"] = sess.Dnis;
                                    drSessions["EdgeId"] = sess.EdgeId;
                                    if (!(sess.Flow is null)) drSessions["FlowId"] = sess.Flow.FlowId;
                                    if (!(sess.Flow is null)) drSessions["FlowName"] = sess.Flow.FlowName;
                                    if (!(sess.Flow is null)) drSessions["EndingLanguage"] = sess.Flow.EndingLanguage;
                                    if (!(sess.Flow is null)) drSessions["EntryReason"] = EntryReason;
                                    if (!(sess.Flow is null)) drSessions["EntryType"] = sess.Flow.EntryType;
                                    if (!(sess.Flow is null)) drSessions["ExitReason"] = sess.Flow.ExitReason;
                                    if (!(sess.Flow is null)) drSessions["FlowType"] = sess.Flow.FlowType;
                                    if (!(sess.Flow is null)) drSessions["FlowVersion"] = sess.Flow.FlowVersion;
                                    if (!(sess.Flow is null)) drSessions["IssuedCallback"] = sess.Flow.IssuedCallback;
                                    if (!(sess.Flow is null)) drSessions["StartingLanguage"] = sess.Flow.StartingLanguage;
                                    if (!(sess.Flow is null)) drSessions["TransferTargetAddress"] = sess.Flow.TransferTargetAddress;
                                    if (!(sess.Flow is null)) drSessions["TransferTargetName"] = sess.Flow.TransferTargetName;
                                    if (!(sess.Flow is null)) drSessions["TransferType"] = sess.Flow.TransferType;
                                    drSessions["FlowInType"] = sess.FlowInType;
                                    drSessions["FlowOutType"] = sess.FlowOutType;
                                    drSessions["JourneyActionId"] = sess.JourneyActionId;
                                    drSessions["JourneyActionMapId"] = sess.JourneyActionMapId;
                                    drSessions["JourneyActionMapVersion"] = sess.JourneyActionMapVersion;
                                    drSessions["JourneyCustomerId"] = sess.JourneyCustomerId;
                                    drSessions["JourneyCustomerIdType"] = sess.JourneyCustomerIdType;
                                    drSessions["JourneyCustomerSessionId"] = sess.JourneyCustomerSessionId;
                                    drSessions["JourneyCustomerSessionIdType"] = sess.JourneyCustomerSessionIdType;
                                    drSessions["MediaBridgeId"] = sess.MediaBridgeId;
                                    if (!(sess.MediaCount is null)) drSessions["MediaCount"] = sess.MediaCount;
                                    if (!(sess.MediaType is null)) drSessions["MediaType"] = sess.MediaType.ToString();
                                    if (!(sess.MessageType is null)) drSessions["MessageType"] = sess.MessageType.ToString();
                                    drSessions["MonitoredParticipantId"] = sess.MonitoredParticipantId;
                                    // drSessions["MonitoredSessionId"] = sess.MonitoredSessionId;
                                    drSessions["OutboundCampaignId"] = sess.OutboundCampaignId;
                                    drSessions["OutboundContactId"] = sess.OutboundContactId;
                                    drSessions["OutboundContactListId"] = sess.OutboundContactListId;
                                    drSessions["PeerId"] = sess.PeerId;
                                    drSessions["ProtocolCallId"] = sess.ProtocolCallId;
                                    drSessions["Provider"] = sess.Provider;
                                    drSessions["Recording"] = sess.Recording;
                                    drSessions["Remote"] = sess.Remote;
                                    drSessions["RemoteNameDisplayable"] = sess.RemoteNameDisplayable;
                                    drSessions["RoomId"] = sess.RoomId;
                                    drSessions["ScreenShareAddressSelf"] = sess.ScreenShareAddressSelf;
                                    drSessions["ScreenShareRoomId"] = sess.ScreenShareRoomId;
                                    drSessions["ScriptId"] = sess.ScriptId;
                                    drSessions["SelectedAgentId"] = sess.SelectedAgentId;
                                    drSessions["SelectedAgentRank"] = (object)sess.SelectedAgentRank ?? DBNull.Value;
                                    drSessions["SessionDnis"] = sess.SessionDnis;
                                    drSessions["SharingScreen"] = sess.SharingScreen;
                                    drSessions["SkipEnabled"] = sess.SkipEnabled;
                                    drSessions["TimeoutSeconds"] = (object)sess.TimeoutSeconds ?? DBNull.Value;
                                    drSessions["UsedRouting"] = sess.UsedRouting.ToString();
                                    drSessions["VideoAddressSelf"] = sess.VideoAddressSelf;
                                    drSessions["VideoRoomId"] = sess.VideoRoomId;
                                    drSessions["FirstQueueId"] = strFirstQueue;
                                    drSessions["LastWrapUpCode"] = strLastWrapUpCode;
                                    if(dtSessions.Columns.Contains("LastDisconnectType")) drSessions["LastDisconnectType"] = strLastDisconnectType;
                                    if (!(sess.Segments is null)) drSessions["StartTimeUTC"] = sess.Segments[0].SegmentStart.Value.ToUniversalTime();
                                    if (!(sess.Segments is null) && sess.Segments[sess.Segments.Count - 1].SegmentEnd.HasValue) drSessions["EndTimeUTC"] = sess.Segments[sess.Segments.Count - 1].SegmentEnd.Value.ToUniversalTime();
                                    dtSessions.Rows.Add(drSessions);

                                    sessheaders = "ConversationId,ParticipantId,UserId,SessionId,ConversationStartUTC,AcwSkipped,AddressFrom,AddressOther,AddressSelf,AddressTo,AgentAssistantId,Ani,AssignerId,CallbackScheduledTimeUTC,"
                                        + "CallbackUserName,CobrowseRole,CobrowseRoomId,Direction,DispositionAnalyzer,DispositionName,Dnis,EdgeId,FlowId,FlowName,EndingLanguage,EntryReason,EntryType,ExitReason,FlowType," 
                                        + "FlowVersion,IssuedCallback,StartingLanguage,TransferTargetAddress,TransferTargetName,TransferType,FlowInType,FlowOutType,JourneyActionId,JourneyActionMapId,JourneyActionMapVersion,"
                                        + "JourneyCustomerId,JourneyCustomerIdType,JourneyCustomerSessionId,JourneyCustomerSessionIdType,MediaBridgeId,MediaCount,MediaType,MessageType,MonitoredParticipantId,MonitoredSessionId," 
                                        + "OutboundCampaignId,OutboundContactId,OutboundContactListId,PeerId,ProtocolCallId,Provider,Recording,Remote,RemoteNameDisplayable,RoomId,ScreenShareAddressSelf,ScreenShareRoomId," 
                                        + "ScriptId,SelectedAgentId,SelectedAgentRank,SessionDnis,SharingScreen,SkipEnabled,TimeoutSeconds,UsedRouting,VideoAddressSelf,VideoRoomId,FirstQueueId,StartTimeUTC,EndTimeUTC,";
                                    sessvalues = ""
                                        + "'" + conv.ConversationId + "',"
                                        + "'" + party.ParticipantId + "',"
                                        + "'" + party.UserId + "',"
                                        + "'" + sess.SessionId + "',"
                                        + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                                        + "'" + (sess.AcwSkipped ?? false) + "',"
                                        + "'" + sess.AddressFrom + "',"
                                        + "'" + sess.AddressOther + "',"
                                        + "'" + sess.AddressSelf + "',"
                                        + "'" + sess.AddressTo + "',"
                                        + "'" + sess.AgentAssistantId + "',"
                                        + "'" + sess.Ani + "',"
                                        + "'" + sess.AssignerId + "',"
                                        //+ sess.CallbackNumbers + "','"
                                        + ((sess.CallbackScheduledTime is null) ? "null," : ("'" + sess.CallbackScheduledTime.Value.ToUniversalTime().ToString("s")) + "',")
                                        + "'" + sess.CallbackUserName + "',"
                                        + "'" + sess.CobrowseRole + "',"
                                        + "'" + sess.CobrowseRoomId + "',"
                                        + "'" + sess.Direction.ToString() + "',"
                                        + "'" + sess.DispositionAnalyzer + "',"
                                        + "'" + sess.DispositionName + "',"
                                        + "'" + sess.Dnis + "',"
                                        + "'" + sess.EdgeId + "',"

                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.FlowId) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.FlowName) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.EndingLanguage) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.EntryReason) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.EntryType.ToString()) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.ExitReason) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.FlowType.ToString()) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.FlowVersion) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.IssuedCallback.ToString()) + "',")
                                        //+ ((sess.Flow is null) ? "null," : (sess.Flow.Outcomes is null) ? "null," : (sess.Flow.Outcomes.ToArray().ToString().Replace("System.String", "") + "','")) 
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.StartingLanguage) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.TransferTargetAddress) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.TransferTargetName) + "',")
                                        + ((sess.Flow is null) ? "null," : ("'" + sess.Flow.TransferType) + "',")

                                        + "'" + sess.FlowInType + "',"
                                        + "'" + sess.FlowOutType + "',"
                                        + "'" + sess.JourneyActionId + "',"
                                        + "'" + sess.JourneyActionMapId + "',"
                                        + "'" + sess.JourneyActionMapVersion + "',"
                                        + "'" + sess.JourneyCustomerId + "',"
                                        + "'" + sess.JourneyCustomerIdType + "',"
                                        + "'" + sess.JourneyCustomerSessionId + "',"
                                        + "'" + sess.JourneyCustomerSessionIdType + "',"
                                        + "'" + sess.MediaBridgeId + "',"
                                        + (sess.MediaCount ?? 0) + ","
                                        //+ sess.MediaEndpointStats
                                        + "'" + sess.MediaType.ToString() + "',"
                                        + ((sess.MessageType is null) ? "null," : ("'" + sess.MessageType.ToString() + "',"))
                                        //+ (sess.Metrics is null ? "":sess.Metrics.ToString()) + "','"
                                        + "'" + sess.MonitoredParticipantId + "',"
                                        // + "'" + sess.MonitoredSessionId + "',"
                                        + ((sess.OutboundCampaignId is null) ? "null," : ("'" + sess.OutboundCampaignId + "',"))
                                        + ((sess.OutboundContactId is null) ? "null," : ("'" + sess.OutboundContactId + "',"))
                                        + ((sess.OutboundContactListId is null) ? "null," : ("'" + sess.OutboundContactListId + "',"))
                                        + ((sess.PeerId is null) ? "null," : ("'" + sess.PeerId + "',"))
                                        //+ sess.ProposedAgents
                                        + "'" + sess.ProtocolCallId + "',"
                                        + "'" + sess.Provider + "',"
                                        + "'" + sess.Recording + "',"
                                        + "'" + sess.Remote + "',"
                                        + "'" + sess.RemoteNameDisplayable + "',"
                                        //+ sess.RequestedRoutings + "','"
                                        + "'" + sess.RoomId + "',"
                                        + "'" + sess.ScreenShareAddressSelf + "',"

                                        + "'" + sess.ScreenShareRoomId + "',"
                                        + "'" + sess.ScriptId + "',"
                                        //+ sess.Segments+ "','"
                                        + "'" + sess.SelectedAgentId + "',"
                                        + (sess.SelectedAgentRank ?? -1) + ","
                                        + "'" + sess.SessionDnis + "',"
                                        + "'" + (sess.SharingScreen ?? false) + "',"

                                        + "'" + (sess.SkipEnabled ?? false) + "',"
                                        + (sess.TimeoutSeconds ?? 0) + ","
                                        + "'" + sess.UsedRouting.ToString() + "',"
                                        + "'" + (sess.VideoAddressSelf ?? "") + "',"
                                        + "'" + (sess.VideoRoomId) + "',"

                                        + "" + strFirstQueue + ","
                                        + ((sess.Segments[0] is null) ? "null," : ("'" + (sess.Segments[0].SegmentStart.Value.ToUniversalTime().ToString("s")) + "',"))
                                        + ((sess.Segments[0] is null || !(sess.Segments[sess.Segments.Count - 1].SegmentEnd.HasValue)) ? "null," : ("'" + (sess.Segments[sess.Segments.Count -1].SegmentEnd.Value.ToUniversalTime().ToString("s")) + "',"))
                                        ;
                                    if (!(sess.Metrics is null))
                                    {
                                        foreach (AnalyticsSessionMetric m in sess.Metrics)
                                        {
                                            if (!sessheaders.Contains(m.Name) && (!m.Name.StartsWith("o")))
                                            {
                                                if(m.Value > Int32.MaxValue) drSessions[m.Name] = Int32.MaxValue;
                                                else drSessions[m.Name] = m.Value;
                                                sessheaders += m.Name + ",";
                                                sessvalues += m.Value + ",";
                                            }
                                        }

                                    }
                                    if (sessheaders.EndsWith(",")) sessheaders = sessheaders.TrimEnd(',');
                                    if (sessvalues.EndsWith(",")) sessvalues = sessvalues.TrimEnd(',');
                                    sql.CommandText = "INSERT INTO gen.gcConversationSessions (" + sessheaders + ") VALUES (" + sessvalues + ");";
                                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                    //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                                    
                                    foreach (AnalyticsConversationSegment seg in sess.Segments)
                                    {

                                        // added 06-Oct-2021 to truncate the entryreason
                                        string WrapupNote;
                                        if (!(seg.WrapUpNote is null) && (sess.Flow.EntryReason.Length > 255))
                                        {
                                            WrapupNote = seg.WrapUpNote.Substring(0, 255);
                                        }
                                        else if (!(seg.WrapUpNote is null))
                                        {
                                            WrapupNote = seg.WrapUpNote;
                                        }
                                        else WrapupNote = "";

                                        DataRow drSegments = dtSegments.NewRow();
                                        drSegments["ConversationId"] = conv.ConversationId;
                                        drSegments["ConversationSessionId"] = sess.SessionId;
                                        drSegments["ParticipantId"] = party.ParticipantId;
                                        drSegments["ConversationStartUTC"] = conv.ConversationStart.Value.ToUniversalTime();
                                        if (!(seg.SegmentStart is null)) drSegments["SegmentStartUTC"] = seg.SegmentStart.Value.ToUniversalTime();
                                        if (!(seg.SegmentEnd is null)) drSegments["SegmentEndUTC"] = seg.SegmentEnd.Value.ToUniversalTime();
                                        drSegments["AudioMuted"] = seg.AudioMuted;
                                        drSegments["Conference"] = seg.Conference;
                                        drSegments["DestinationConversationId"] = seg.DestinationConversationId;
                                        drSegments["DestinationSessionId"] = seg.DestinationSessionId;
                                        drSegments["DisconnectType"] = seg.DisconnectType;
                                        drSegments["ErrorCode"] = seg.ErrorCode;
                                        drSegments["GroupId"] = seg.GroupId;
                                        if (!(seg.Properties is null)) drSegments["Properties"] = seg.Properties.ToArray().ToString();
                                        drSegments["QueueId"] = seg.QueueId;
                                        drSegments["RequestedLanguageId"] = seg.RequestedLanguageId;
                                        if (!(seg.RequestedRoutingSkillIds is null)) drSegments["RequestedRoutingSkillIds"] = seg.RequestedRoutingSkillIds.ToArray().ToString().Replace("System.String", "");
                                        if (!(seg.RequestedRoutingUserIds is null)) drSegments["RequestedRoutingUserIds"] = seg.RequestedRoutingUserIds.ToArray().ToString().Replace("System.String", "");
                                        if (!(seg.ScoredAgents is null)) drSegments["ScoredAgents"] = seg.ScoredAgents.ToArray().ToString().Replace("System.String", "");
                                        drSegments["SegmentType"] = seg.SegmentType;
                                        drSegments["SourceConversationId"] = seg.SourceConversationId;
                                        drSegments["SourceSessionId"] = seg.SourceSessionId;
                                        drSegments["Subject"] = seg.Subject;
                                        drSegments["VideoMuted"] = seg.VideoMuted;
                                        drSegments["WrapUpCode"] = seg.WrapUpCode;
                                        drSegments["DestinationSessionId"] = seg.DestinationSessionId;
                                        drSegments["WrapUpNote"] = WrapupNote;
                                        if (!(seg.WrapUpTags is null)) drSegments["WrapUpTags"] = seg.WrapUpTags.ToArray().ToString().Replace("System.String", "");
                                        dtSegments.Rows.Add(drSegments);

                                        string segheaders = "ConversationId,ConversationSessionId,ParticipantId,ConversationStartUTC,SegmentStart,SegmentEnd,AudioMuted,Conference,DestinationConversationId,DestinationSessionId,DisconnectType,ErrorCode,GroupId,Properties,QueueId,RequestedLanguageId,RequestedRoutingSkillIds,RequestedRoutingUserIds,ScoredAgents,SegmentType,SourceConversationId,SourceSessionId,Subject,VideoMuted,WrapUpCode,WrapUpNote,WrapUpTags";
                                        string segvalues = ""
                                            + "'" + conv.ConversationId + "',"
                                            + "'" + sess.SessionId + "',"
                                            + "'" + party.ParticipantId + "',"
                                            + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                                            + "'" + seg.SegmentStart.Value.ToUniversalTime().ToString("s") + "',"
                                            + ((seg.SegmentEnd is null) ? "null," : ("'" + seg.SegmentEnd.Value.ToUniversalTime().ToString("s") + "',"))
                                            + ((seg.AudioMuted is null) ? "null," : ("'" + seg.AudioMuted + "',"))
                                            + ((seg.Conference is null) ? "null," : ("'" + seg.Conference + "',"))
                                            + "'" + seg.DestinationConversationId + "',"
                                            + "'" + seg.DestinationSessionId + "',"
                                            + ((seg.DisconnectType is null) ? "null," : ("'" + seg.DisconnectType + "',"))
                                            + "'" + seg.ErrorCode + "',"
                                            + "'" + seg.GroupId + "',"
                                            + ((seg.Properties is null) ? "null," : ("'" + seg.Properties.ToArray().ToString().Replace("System.String", "") + "',"))
                                            + "'" + seg.QueueId + "',"
                                            + "'" + seg.RequestedLanguageId + "',"
                                            + ((seg.RequestedRoutingSkillIds is null) ? "null," : ("'" + seg.RequestedRoutingSkillIds.ToArray().ToString().Replace("System.String", "") + "',"))
                                            + ((seg.RequestedRoutingUserIds is null) ? "null," : ("'" + seg.RequestedRoutingUserIds.ToArray().ToString().Replace("System.String", "") + "',"))
                                            + ((seg.ScoredAgents is null) ? "null," : ("'" + seg.ScoredAgents.ToArray().ToString().Replace("System.String", "") + "',"))
                                            + ((seg.SegmentType is null) ? "null," : ("'" + seg.SegmentType + "',"))
                                            + "'" + seg.SourceConversationId + "',"
                                            + "'" + seg.SourceSessionId + "',"
                                            + "'" + seg.Subject + "',"
                                            + ((seg.VideoMuted is null) ? "null," : ("'" + seg.VideoMuted + "',"))
                                            + "'" + seg.WrapUpCode + "',"
                                            + "'" + seg.WrapUpNote + "',"
                                            + ((seg.WrapUpTags is null) ? "null," : ("'" + seg.WrapUpTags.ToArray().ToString().Replace("System.String", "") + "',"))
                                            ;

                                        if (segheaders.EndsWith(",")) segheaders = segheaders.TrimEnd(',');
                                        if (segvalues.EndsWith(",")) segvalues = segvalues.TrimEnd(',');
                                        sql.CommandText = "INSERT INTO gen.gcConversationSegments (" + segheaders + ") VALUES (" + segvalues + ");";
                                        if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                        //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                                    }
                                    
                                }
                            }
                        }
                    }

                    if (conv.Resolutions != null)
                    {
                        Console.WriteLine(DateTime.Now.ToString("s") + " - Writing to gen.gcConversationResolutions...");
                        foreach (AnalyticsResolution resId in conv.Resolutions)
                        {
                            sql.CommandText = "INSERT INTO gen.gcConversationResolutions (ConversationId,ConversationStartUTC,GetnNextContactAvoided,QueueId,UserId) VALUES ('"
                                + conv.ConversationId + "','"
                                + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                                + resId.NNextContactAvoided + "','" 
                                + resId.QueueId + "','" 
                                + resId.UserId + "');";
                            if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                        }
                    }

                    try
                    {
                        List<RecordingMetadata> recs = recApi.GetConversationRecordingmetadata(conv.ConversationId);

                        //Console.WriteLine(DateTime.Now.ToString("s") + " - Writing to gen.gcConversationRecordings...");
                        foreach (RecordingMetadata rec in recs)
                        {
                            DataRow drRecordings = dtRecordings.NewRow();
                            drRecordings["ConversationId"] = rec.ConversationId;
                            drRecordings["SessionId"] = rec.SessionId;
                            drRecordings["RecordingId"] = rec.Id;
                            drRecordings["ConversationStartUTC"] = conv.ConversationStart.Value.ToUniversalTime();
                            drRecordings["Media"] = rec.Media;
                            drRecordings["FileState"] = rec.FileState;
                            drRecordings["Name"] = rec.Name;
                            drRecordings["Path"] = rec.Path;
                            drRecordings["StartTime"] = rec.StartTime;
                            drRecordings["EndTime"] = rec.EndTime;
                            drRecordings["SelfUri"] = rec.SelfUri;
                            if (!(rec.Annotations is null)) drRecordings["Annotations"] = rec.Annotations.ToArray().ToString().Replace("System.String", "").Replace("PureCloudPlatform.Client.V2.Model", "");
                            dtRecordings.Rows.Add(drRecordings);

                            sql.CommandText = "INSERT INTO gen.gcConversationRecordings (ConversationId,SessionId,RecordingId,ConversationStartUTC,Media,FileState,Name,Path,StartTime,EndTime,SelfUri,Annotations) VALUES ("
                                + "'" + rec.ConversationId + "',"
                                + "'" + rec.SessionId + "',"
                                + "'" + rec.Id + "',"
                                + "'" + conv.ConversationStart.Value.ToUniversalTime().ToString("s") + "',"
                                + "'" + rec.Media + "',"
                                + "'" + rec.FileState + "',"
                                + "'" + rec.Name + "',"
                                + "'" + rec.Path + "',"
                                + "'" + rec.StartTime + "',"
                                + "'" + rec.EndTime + "',"
                                + "'" + rec.SelfUri + "',"
                                + ((rec.Annotations is null) ? "null," : ("'" + rec.Annotations.ToArray().ToString().Replace("System.String", "") + "'"))
                                + ");";
                            if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                            //Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                        }
                    }
                    catch (ApiException e)
                    {
                        Console.WriteLine("Error retrieving Recordings: " + e);
                    }


                }
                
                pageCounter++;
                if (queryResults.Conversations.Count < pageSize) lastPage = true;
            } while (!lastPage);

            dtConversations.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcConversations";
            bulkCopy.WriteToServer(dtConversations);

            dtDivisions.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcConversationDivisions";
            bulkCopy.WriteToServer(dtDivisions);

            dtParticipants.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcConversationParticipants";
            bulkCopy.WriteToServer(dtParticipants);

            dtSessions.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcConversationSessions";
            bulkCopy.WriteToServer(dtSessions);

            dtSegments.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcConversationSegments";
            bulkCopy.WriteToServer(dtSegments);

            dtFlowOutcomes.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcConversationFlowOutcomes";
            bulkCopy.WriteToServer(dtFlowOutcomes);

            dtRecordings.AcceptChanges();
            bulkCopy.DestinationTableName = "gen.gcConversationRecordings";
            bulkCopy.WriteToServer(dtRecordings);

            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateConversationAggregates(string dbconnstring, string intervalFilter, string useConvJobId="", string logMode = "Standard")
        {
            Console.WriteLine(DateTime.Now.ToString("s") + " - Initializing Conversation Aggregates Population...");
            AnalyticsApi analApi = new AnalyticsApi();
            
            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            string strIntervalStart = intervalFilter.Split('/')[0];
            DateTime dIntervalStartUTC = DateTime.Parse(strIntervalStart);
            string strIntervalEnd = intervalFilter.Split('/')[1];
            DateTime dIntervalEndUTC = DateTime.Parse(strIntervalEnd);
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Retrieving Aggregated Metrics for all Calls...");
            ConversationAggregationQuery aqBody = new ConversationAggregationQuery();
            aqBody.Interval = intervalFilter;
            //aqBody.TimeZone = timeZone;
            //aqBody.Granularity = "PT1H";
            aqBody.Filter = new ConversationAggregateQueryFilter(ConversationAggregateQueryFilter.TypeEnum.And);
            aqBody.Filter.Predicates = new List<ConversationAggregateQueryPredicate>();
            //aqBody.Filter.Predicates.Add(new ConversationAggregateQueryPredicate(ConversationAggregateQueryPredicate.TypeEnum.Dimension, ConversationAggregateQueryPredicate.DimensionEnum.Interactiontype, ConversationAggregateQueryPredicate.OperatorEnum.Matches, "contactCenter"));
            aqBody.GroupBy = new List<ConversationAggregationQuery.GroupByEnum>() {
                    //ConversationAggregationQuery.GroupByEnum.Addressfrom,
                    //ConversationAggregationQuery.GroupByEnum.Addressto,
                    //ConversationAggregationQuery.GroupByEnum.Agentassistantid,
                    //ConversationAggregationQuery.GroupByEnum.Agentrank,
                    //ConversationAggregationQuery.GroupByEnum.Agentscore,
                    ConversationAggregationQuery.GroupByEnum.Ani,
                    ConversationAggregationQuery.GroupByEnum.Conversationid,
                    //ConversationAggregationQuery.GroupByEnum.Convertedfrom,
                    //ConversationAggregationQuery.GroupByEnum.Convertedto,
                    ConversationAggregationQuery.GroupByEnum.Direction,
                    //ConversationAggregationQuery.GroupByEnum.Disconnecttype,
                    //ConversationAggregationQuery.GroupByEnum.Divisionid,
                    ConversationAggregationQuery.GroupByEnum.Dnis,
                    //ConversationAggregationQuery.GroupByEnum.Edgeid,
                    //ConversationAggregationQuery.GroupByEnum.Externalcontactid,
                    //ConversationAggregationQuery.GroupByEnum.Externalmediacount,
                    //ConversationAggregationQuery.GroupByEnum.Externalorganizationid,
                    //ConversationAggregationQuery.GroupByEnum.Firstqueue,
                    //ConversationAggregationQuery.GroupByEnum.Flaggedreason,
                    //ConversationAggregationQuery.GroupByEnum.Flowintype,
                    //ConversationAggregationQuery.GroupByEnum.Flowouttype,
                    //ConversationAggregationQuery.GroupByEnum.Groupid,
                    ConversationAggregationQuery.GroupByEnum.Interactiontype,
                    //ConversationAggregationQuery.GroupByEnum.Journeyactionid,
                    //ConversationAggregationQuery.GroupByEnum.Journeyactionmapid,
                    //ConversationAggregationQuery.GroupByEnum.Journeyactionmapversion,
                    //ConversationAggregationQuery.GroupByEnum.Journeycustomerid,
                    //ConversationAggregationQuery.GroupByEnum.Journeycustomeridtype,
                    //ConversationAggregationQuery.GroupByEnum.Journeycustomersessionid,
                    //ConversationAggregationQuery.GroupByEnum.Journeycustomersessionidtype,
                    //ConversationAggregationQuery.GroupByEnum.Mediacount,
                    ConversationAggregationQuery.GroupByEnum.Mediatype,
                    //ConversationAggregationQuery.GroupByEnum.Messagetype,
                    //ConversationAggregationQuery.GroupByEnum.Originatingdirection,
                    ConversationAggregationQuery.GroupByEnum.Outboundcampaignid,
                    ConversationAggregationQuery.GroupByEnum.Outboundcontactid,
                    ConversationAggregationQuery.GroupByEnum.Outboundcontactlistid,
                    //ConversationAggregationQuery.GroupByEnum.Participantname,
                    //ConversationAggregationQuery.GroupByEnum.Peerid,
                    //ConversationAggregationQuery.GroupByEnum.Proposedagentid,
                    //ConversationAggregationQuery.GroupByEnum.Provider,
                    //ConversationAggregationQuery.GroupByEnum.Purpose,
                    ConversationAggregationQuery.GroupByEnum.Queueid,
                    ConversationAggregationQuery.GroupByEnum.Remote,
                    //ConversationAggregationQuery.GroupByEnum.Reoffered,
                    //ConversationAggregationQuery.GroupByEnum.Requestedlanguageid,
                    //ConversationAggregationQuery.GroupByEnum.Requestedrouting,
                    //ConversationAggregationQuery.GroupByEnum.Requestedroutingskillid,
                    //ConversationAggregationQuery.GroupByEnum.Roomid,
                    //ConversationAggregationQuery.GroupByEnum.Routingpriority,
                    //ConversationAggregationQuery.GroupByEnum.Scoredagentid,
                    //ConversationAggregationQuery.GroupByEnum.Selectedagentid,
                    //ConversationAggregationQuery.GroupByEnum.Selectedagentrank,
                    //ConversationAggregationQuery.GroupByEnum.Sessiondnis,
                    //ConversationAggregationQuery.GroupByEnum.Sessionid,
                    //ConversationAggregationQuery.GroupByEnum.Stationid,
                    //ConversationAggregationQuery.GroupByEnum.Teamid,
                    //ConversationAggregationQuery.GroupByEnum.Usedrouting,
                    ConversationAggregationQuery.GroupByEnum.Userid,
                    //ConversationAggregationQuery.GroupByEnum.Wrapupcode
                };
            //aqBody.Metrics = new List<ConversationAggregationQuery.MetricsEnum>() { ConversationAggregationQuery.MetricsEnum.Nblindtransferred,ConversationAggregationQuery.MetricsEnum.Ncobrowsesessions,ConversationAggregationQuery.MetricsEnum.Nconnected,
            //ConversationAggregationQuery.MetricsEnum.Nconsult,ConversationAggregationQuery.MetricsEnum.Nconsulttransferred,ConversationAggregationQuery.MetricsEnum.Nerror,ConversationAggregationQuery.MetricsEnum.Noffered,
            //ConversationAggregationQuery.MetricsEnum.Noutbound,ConversationAggregationQuery.MetricsEnum.Noutboundabandoned,ConversationAggregationQuery.MetricsEnum.Noutboundattempted,ConversationAggregationQuery.MetricsEnum.Noutboundconnected,
            //ConversationAggregationQuery.MetricsEnum.Noversla,ConversationAggregationQuery.MetricsEnum.Nstatetransitionerror,ConversationAggregationQuery.MetricsEnum.Ntransferred,ConversationAggregationQuery.MetricsEnum.Oexternalmediacount,
            //ConversationAggregationQuery.MetricsEnum.Omediacount,ConversationAggregationQuery.MetricsEnum.Oservicelevel,ConversationAggregationQuery.MetricsEnum.Oservicetarget,ConversationAggregationQuery.MetricsEnum.Tabandon,
            //ConversationAggregationQuery.MetricsEnum.Tacd,ConversationAggregationQuery.MetricsEnum.Tacw,ConversationAggregationQuery.MetricsEnum.Tagentresponsetime,ConversationAggregationQuery.MetricsEnum.Talert,
            //ConversationAggregationQuery.MetricsEnum.Tanswered,ConversationAggregationQuery.MetricsEnum.Tcallback,ConversationAggregationQuery.MetricsEnum.Tcallbackcomplete,ConversationAggregationQuery.MetricsEnum.Tcontacting,
            //ConversationAggregationQuery.MetricsEnum.Tdialing,ConversationAggregationQuery.MetricsEnum.Tflowout,ConversationAggregationQuery.MetricsEnum.Thandle,ConversationAggregationQuery.MetricsEnum.Theld,
            //ConversationAggregationQuery.MetricsEnum.Theldcomplete,ConversationAggregationQuery.MetricsEnum.Tivr,ConversationAggregationQuery.MetricsEnum.Tmonitoring,ConversationAggregationQuery.MetricsEnum.Tnotresponding,
            //ConversationAggregationQuery.MetricsEnum.Tshortabandon,ConversationAggregationQuery.MetricsEnum.Ttalk,ConversationAggregationQuery.MetricsEnum.Ttalkcomplete,ConversationAggregationQuery.MetricsEnum.Tuserresponsetime,ConversationAggregationQuery.MetricsEnum.Tvoicemail,ConversationAggregationQuery.MetricsEnum.Twait };
            aqBody.Metrics = new List<ConversationAggregationQuery.MetricsEnum>() { ConversationAggregationQuery.MetricsEnum.Nblindtransferred, ConversationAggregationQuery.MetricsEnum.Nconnected, ConversationAggregationQuery.MetricsEnum.Nconsult, 
                ConversationAggregationQuery.MetricsEnum.Nconsulttransferred, ConversationAggregationQuery.MetricsEnum.Nerror, ConversationAggregationQuery.MetricsEnum.Noffered, ConversationAggregationQuery.MetricsEnum.Noutbound, 
                ConversationAggregationQuery.MetricsEnum.Noutboundabandoned, ConversationAggregationQuery.MetricsEnum.Noutboundattempted, ConversationAggregationQuery.MetricsEnum.Noutboundconnected, ConversationAggregationQuery.MetricsEnum.Noversla, 
                ConversationAggregationQuery.MetricsEnum.Nstatetransitionerror, ConversationAggregationQuery.MetricsEnum.Ntransferred, ConversationAggregationQuery.MetricsEnum.Oexternalmediacount, ConversationAggregationQuery.MetricsEnum.Omediacount, 
                ConversationAggregationQuery.MetricsEnum.Oservicelevel, ConversationAggregationQuery.MetricsEnum.Oservicetarget, ConversationAggregationQuery.MetricsEnum.Tabandon,ConversationAggregationQuery.MetricsEnum.Tacd, 
                ConversationAggregationQuery.MetricsEnum.Tacw, ConversationAggregationQuery.MetricsEnum.Tagentresponsetime, ConversationAggregationQuery.MetricsEnum.Talert,ConversationAggregationQuery.MetricsEnum.Tanswered, 
                ConversationAggregationQuery.MetricsEnum.Tcontacting, ConversationAggregationQuery.MetricsEnum.Tdialing, ConversationAggregationQuery.MetricsEnum.Tflowout, ConversationAggregationQuery.MetricsEnum.Thandle, 
                ConversationAggregationQuery.MetricsEnum.Theld, ConversationAggregationQuery.MetricsEnum.Theldcomplete, ConversationAggregationQuery.MetricsEnum.Tivr, ConversationAggregationQuery.MetricsEnum.Tmonitoring, 
                ConversationAggregationQuery.MetricsEnum.Tnotresponding, ConversationAggregationQuery.MetricsEnum.Tshortabandon, ConversationAggregationQuery.MetricsEnum.Ttalk, ConversationAggregationQuery.MetricsEnum.Ttalkcomplete, 
                ConversationAggregationQuery.MetricsEnum.Tuserresponsetime, ConversationAggregationQuery.MetricsEnum.Tvoicemail, ConversationAggregationQuery.MetricsEnum.Twait };
 
            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting Metrics for individual calls from database...");
            sql.CommandText = $"DELETE FROM gen.gcConversationAggregates where ConversationId in (select ConversationId from gen.gcConversations where ConversationStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}');";
            if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Running All Calls Query against Cloud API" + intervalFilter);
            ConversationAggregateQueryResponse aqResult = analApi.PostAnalyticsConversationsAggregatesQuery(aqBody);
            if(aqResult.Results is null)
            {
                Console.WriteLine("No Results");
                return;
            }
            Console.WriteLine(DateTime.Now.ToString("s") + " - Results retrieved: " + aqResult.Results.Count);
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Writing Aggregated Metrics for All Calls to Database...");

            foreach (ConversationAggregateDataContainer res in aqResult.Results)
            {

                string headers = "", values = "";

                if (res.Group.ContainsKey("conversationId"))
                {
                    //Console.WriteLine("conversationId - " + res.Group["conversationId"]);
                    foreach (string key in res.Group.Keys)
                    {
                        //Console.WriteLine(key + ": " + res.Group[key]);
                        headers += key + ",";
                        values += "'" + res.Group[key] + "',";
                    }
                    headers += "dIntervalStart,dIntervalStartUTC,nDuration" + ",";
                    StatisticalResponse data = res.Data[0];
                    string strRecIntervalStart = data.Interval.Split('/')[0];
                    DateTime dRecIntervalStart = DateTime.Parse(strRecIntervalStart);
                    string strRecIntervalEnd = data.Interval.Split('/')[1];
                    DateTime dRecIntervalEnd = DateTime.Parse(strRecIntervalEnd);
                    int nDuration = 30 * 60;
                    values += "'" + dRecIntervalStart.ToLocalTime().ToString("s") + "','" + dRecIntervalStart.ToUniversalTime().ToString("s") + "'," + nDuration + ",";

                    //headers += "interval" + ",";
                    //values += "'" + data.Interval + "',";
                    foreach (AggregateMetricData metric in data.Metrics)
                    {
                        if (!metric.Metric.Equals("interval"))
                        {
                            headers += metric.Metric + ",";
                            switch (metric.Metric[0])
                            {
                                case 'n':
                                    values += metric.Stats.Count + ",";
                                    break;
                                case 't':
                                    values += metric.Stats.Sum + ",";
                                    headers += "n" + metric.Metric.Substring(1) + ",";
                                    values += metric.Stats.Count + ",";
                                    headers += "max" + metric.Metric.Substring(1) + ",";
                                    values += metric.Stats.Max + ",";
                                    break;
                                case 'o':
                                    if (metric.Metric.Equals("oServiceLevel")) values += metric.Stats.Ratio + ",";
                                    else values += metric.Stats.Current + ",";
                                    break;
                                default:
                                    values += metric.Stats.Count + ",";
                                    break;
                            }
                        }
                    }
                    //Console.WriteLine(data.ToString());
                    if (headers.EndsWith(",")) headers = headers.TrimEnd(',');
                    if (values.EndsWith(",")) values = values.TrimEnd(',');

                    sql.CommandText = "INSERT INTO gen.gcConversationAggregates (" + headers + ") VALUES (" + values + ");";
                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                    Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");

                }
            }
            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateQueueAggregates(string dbconnstring, string intervalFilter, string useConvJobId="", string logMode = "Standard")
        {
            Console.WriteLine(DateTime.Now.ToString("s") + " - Initializing Conversation Aggregates Population...");
            AnalyticsApi analApi = new AnalyticsApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            string strIntervalStart = intervalFilter.Split('/')[0];
            DateTime dIntervalStartUTC = DateTime.Parse(strIntervalStart);
            string strIntervalEnd = intervalFilter.Split('/')[1];
            DateTime dIntervalEndUTC = DateTime.Parse(strIntervalEnd);
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting Aggregated Metrics for Queues from database...");
            sql.CommandText = $"DELETE FROM gen.gcQueueAggregates where dIntervalStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Retrieving Aggregated Metrics for Queues...");
            ConversationAggregationQuery aqBody = new ConversationAggregationQuery();
            aqBody.Interval = intervalFilter;
            //aqBody.TimeZone = timeZone;
            aqBody.Granularity = "PT30M";
            //aqBody.Filter = new ConversationAggregateQueryFilter(ConversationAggregateQueryFilter.TypeEnum.And);
            //aqBody.Filter.Predicates = new List<ConversationAggregateQueryPredicate>();
            //aqBody.Filter.Predicates.Add(new ConversationAggregateQueryPredicate(ConversationAggregateQueryPredicate.TypeEnum.Dimension, ConversationAggregateQueryPredicate.DimensionEnum.Interactiontype, ConversationAggregateQueryPredicate.OperatorEnum.Matches, "contactCenter"));
            aqBody.GroupBy = new List<ConversationAggregationQuery.GroupByEnum>() {
                    ConversationAggregationQuery.GroupByEnum.Mediatype, // map to cHKey3
                    //ConversationAggregationQuery.GroupByEnum.Participantname, // map to cName
                    //ConversationAggregationQuery.GroupByEnum.Purpose, // map to cType
                    ConversationAggregationQuery.GroupByEnum.Queueid,
                    ConversationAggregationQuery.GroupByEnum.Userid,
                    ConversationAggregationQuery.GroupByEnum.Remote
                };
            //aqBody.Metrics = new List<ConversationAggregationQuery.MetricsEnum>() { ConversationAggregationQuery.MetricsEnum.Nblindtransferred,ConversationAggregationQuery.MetricsEnum.Ncobrowsesessions,ConversationAggregationQuery.MetricsEnum.Nconnected,ConversationAggregationQuery.MetricsEnum.Nconsult,
            //ConversationAggregationQuery.MetricsEnum.Nconsulttransferred,ConversationAggregationQuery.MetricsEnum.Nerror,ConversationAggregationQuery.MetricsEnum.Noffered,ConversationAggregationQuery.MetricsEnum.Noutbound,ConversationAggregationQuery.MetricsEnum.Noutboundabandoned,
            //ConversationAggregationQuery.MetricsEnum.Noutboundattempted,ConversationAggregationQuery.MetricsEnum.Noutboundconnected,ConversationAggregationQuery.MetricsEnum.Noversla,ConversationAggregationQuery.MetricsEnum.Nstatetransitionerror,ConversationAggregationQuery.MetricsEnum.Ntransferred,
            //ConversationAggregationQuery.MetricsEnum.Oexternalmediacount,ConversationAggregationQuery.MetricsEnum.Omediacount,ConversationAggregationQuery.MetricsEnum.Oservicelevel,ConversationAggregationQuery.MetricsEnum.Oservicetarget,ConversationAggregationQuery.MetricsEnum.Tabandon,
            //ConversationAggregationQuery.MetricsEnum.Tacd,ConversationAggregationQuery.MetricsEnum.Tacw,ConversationAggregationQuery.MetricsEnum.Tagentresponsetime,ConversationAggregationQuery.MetricsEnum.Talert,ConversationAggregationQuery.MetricsEnum.Tanswered,
            //ConversationAggregationQuery.MetricsEnum.Tcallback,ConversationAggregationQuery.MetricsEnum.Tcallbackcomplete,ConversationAggregationQuery.MetricsEnum.Tcontacting,ConversationAggregationQuery.MetricsEnum.Tdialing,ConversationAggregationQuery.MetricsEnum.Tflowout,
            //ConversationAggregationQuery.MetricsEnum.Thandle,ConversationAggregationQuery.MetricsEnum.Theld,ConversationAggregationQuery.MetricsEnum.Theldcomplete,ConversationAggregationQuery.MetricsEnum.Tivr,ConversationAggregationQuery.MetricsEnum.Tmonitoring,
            //ConversationAggregationQuery.MetricsEnum.Tnotresponding,ConversationAggregationQuery.MetricsEnum.Tshortabandon,ConversationAggregationQuery.MetricsEnum.Ttalk,ConversationAggregationQuery.MetricsEnum.Ttalkcomplete,ConversationAggregationQuery.MetricsEnum.Tuserresponsetime,
            //ConversationAggregationQuery.MetricsEnum.Tvoicemail,ConversationAggregationQuery.MetricsEnum.Twait };
            aqBody.Metrics = new List<ConversationAggregationQuery.MetricsEnum>() { ConversationAggregationQuery.MetricsEnum.Nblindtransferred, ConversationAggregationQuery.MetricsEnum.Nconnected, ConversationAggregationQuery.MetricsEnum.Nconsult, ConversationAggregationQuery.MetricsEnum.Nconsulttransferred, 
                ConversationAggregationQuery.MetricsEnum.Nerror, ConversationAggregationQuery.MetricsEnum.Noffered, ConversationAggregationQuery.MetricsEnum.Noutbound, ConversationAggregationQuery.MetricsEnum.Noutboundabandoned, ConversationAggregationQuery.MetricsEnum.Noutboundattempted, 
                ConversationAggregationQuery.MetricsEnum.Noutboundconnected, ConversationAggregationQuery.MetricsEnum.Noversla, ConversationAggregationQuery.MetricsEnum.Nstatetransitionerror, ConversationAggregationQuery.MetricsEnum.Ntransferred, ConversationAggregationQuery.MetricsEnum.Oexternalmediacount, 
                ConversationAggregationQuery.MetricsEnum.Omediacount, ConversationAggregationQuery.MetricsEnum.Oservicelevel, ConversationAggregationQuery.MetricsEnum.Oservicetarget, ConversationAggregationQuery.MetricsEnum.Tabandon, ConversationAggregationQuery.MetricsEnum.Tacd, 
                ConversationAggregationQuery.MetricsEnum.Tacw, ConversationAggregationQuery.MetricsEnum.Tagentresponsetime, ConversationAggregationQuery.MetricsEnum.Talert, ConversationAggregationQuery.MetricsEnum.Tanswered, ConversationAggregationQuery.MetricsEnum.Tcontacting, 
                ConversationAggregationQuery.MetricsEnum.Tdialing, ConversationAggregationQuery.MetricsEnum.Tflowout, ConversationAggregationQuery.MetricsEnum.Thandle, ConversationAggregationQuery.MetricsEnum.Theld, ConversationAggregationQuery.MetricsEnum.Theldcomplete, 
                ConversationAggregationQuery.MetricsEnum.Tivr, ConversationAggregationQuery.MetricsEnum.Tmonitoring, ConversationAggregationQuery.MetricsEnum.Tnotresponding, ConversationAggregationQuery.MetricsEnum.Tshortabandon, ConversationAggregationQuery.MetricsEnum.Ttalk, 
                ConversationAggregationQuery.MetricsEnum.Ttalkcomplete, ConversationAggregationQuery.MetricsEnum.Tuserresponsetime, ConversationAggregationQuery.MetricsEnum.Tvoicemail, ConversationAggregationQuery.MetricsEnum.Twait };

            Console.WriteLine(DateTime.Now.ToString("s") + " - Running Queues Query against Cloud API" + intervalFilter);
            ConversationAggregateQueryResponse aqResult = analApi.PostAnalyticsConversationsAggregatesQuery(aqBody);
            if (aqResult.Results is null)
            {
                Console.WriteLine("No Results");
                return;
            }
            Console.WriteLine("Results retrieved: " + aqResult.Results.Count);
            Console.WriteLine("Writing Aggregated Metrics for Queues to Database...");

            foreach (ConversationAggregateDataContainer res in aqResult.Results)
            {

                string headers = "", values = "";

                //if (res.Group.ContainsKey("queueId"))
                if(true)
                {
                    //Console.WriteLine("queueId - " + res.Group["queueId"]);
                    foreach (string key in res.Group.Keys)
                    {
                        //Console.WriteLine(key + ": " + res.Group[key]);
                        headers += key + ",";
                        values += "'" + res.Group[key] + "',";
                    }

                    headers += "dIntervalStart,dIntervalStartUTC,nDuration" + ",";
                    StatisticalResponse data = res.Data[0];
                    string strRecIntervalStart = data.Interval.Split('/')[0];
                    DateTime dRecIntervalStart = DateTime.Parse(strRecIntervalStart);
                    string strRecIntervalEnd = data.Interval.Split('/')[1];
                    DateTime dRecIntervalEnd = DateTime.Parse(strRecIntervalEnd);
                    int nDuration = 30 * 60;
                    values += "'" + dRecIntervalStart.ToLocalTime().ToString("s") + "','" + dRecIntervalStart.ToUniversalTime().ToString("s") + "'," + nDuration + ",";

                    foreach (AggregateMetricData metric in data.Metrics)
                    {
                        if (!(metric.Metric.Equals("oServiceLevel") || metric.Metric.Equals("interval")))
                        {
                            headers += metric.Metric + ",";
                            switch (metric.Metric[0])
                            {
                                case 'n':
                                    values += metric.Stats.Count + ",";
                                    break;
                                case 't':
                                    values += metric.Stats.Sum + ",";
                                    headers += "n" + metric.Metric.Substring(1) + ",";
                                    values += metric.Stats.Count + ",";
                                    headers += "max" + metric.Metric.Substring(1) + ",";
                                    values += metric.Stats.Max + ",";
                                    break;
                                case 'o':
                                    values += metric.Stats.Current + ",";
                                    break;
                                default:
                                    values += metric.Stats.Count + ",";
                                    break;
                            }
                        }
                    }
                    //Console.WriteLine(data.ToString());
                    if (headers.EndsWith(",")) headers = headers.TrimEnd(',');
                    if (values.EndsWith(",")) values = values.TrimEnd(',');
                    //AggregateMetricData m = res.Data[0].Metrics.Find(o => o.Metric == "nOffered");
                    sql.CommandText = "INSERT INTO gen.gcQueueAggregates (" + headers + ") VALUES (" + values + ");";
                    if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                    Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");



                }
            }
            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateUserStatusAggregates(string dbconnstring, string intervalFilter, Dictionary<String, String> lsUsers, Dictionary<String, String> lsOrgPresences, string logMode = "Standard")
        {
            Console.WriteLine(DateTime.Now.ToString("s") + " - Initializing User Status Aggregates Population...");
            AnalyticsApi analApi = new AnalyticsApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            string strIntervalStart = intervalFilter.Split('/')[0];
            DateTime dIntervalStartUTC = DateTime.Parse(strIntervalStart);
            string strIntervalEnd = intervalFilter.Split('/')[1];
            DateTime dIntervalEndUTC = DateTime.Parse(strIntervalEnd);
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Deleting Aggregated Metrics for Agent Status from database...");
            sql.CommandText = $"DELETE FROM gen.gcUserAggregates where dIntervalStartUTC between '{dIntervalStartUTC.ToString("s")}' and '{dIntervalEndUTC.AddDays(0).ToString("s")}';";
            Console.WriteLine($"Executing: {sql.CommandText}");
            Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine(DateTime.Now.ToString("s") + " - Retrieving Aggregated Metrics for Agent Status...");
            UserAggregationQuery agBody = new UserAggregationQuery();
            agBody.Interval = intervalFilter;
            //agBody.TimeZone = timeZone;
            agBody.Granularity = "PT30M";
            agBody.GroupBy = new List<UserAggregationQuery.GroupByEnum>() { UserAggregationQuery.GroupByEnum.Userid };
            agBody.Metrics = new List<UserAggregationQuery.MetricsEnum>() {
                    UserAggregationQuery.MetricsEnum.Tagentroutingstatus,
                    UserAggregationQuery.MetricsEnum.Torganizationpresence,
                    UserAggregationQuery.MetricsEnum.Tsystempresence
                };
            agBody.Filter = new UserAggregateQueryFilter(UserAggregateQueryFilter.TypeEnum.Or);
            agBody.Filter.Predicates = new List<UserAggregateQueryPredicate>();
            
            Console.WriteLine(DateTime.Now.ToString("s") + " - Running Agents Query against Cloud API" + intervalFilter);
            int intUserMatch = 0;
            int intTotalProcessed = 0;
            string strUserMatch = "";
            foreach (string u in lsUsers.Keys)
            {
                
                agBody.Filter.Predicates.Add(new UserAggregateQueryPredicate(UserAggregateQueryPredicate.TypeEnum.Dimension, UserAggregateQueryPredicate.DimensionEnum.Userid, UserAggregateQueryPredicate.OperatorEnum.Matches, u));
                strUserMatch += u;
                intUserMatch += 1;
                intTotalProcessed += 1;
                if (intUserMatch < 20 && intTotalProcessed <lsUsers.Count) continue;
                
                UserAggregateQueryResponse agResult = analApi.PostAnalyticsUsersAggregatesQuery(agBody);
                if (!(agResult.Results is null))
                {
                    Console.WriteLine("Users " + strUserMatch + " - " + "Results retrieved: " + agResult.Results.Count);
                    foreach (UserAggregateDataContainer res in agResult.Results)
                    {
                        string headers = "", values = "";

                        if (res.Group.ContainsKey("userId"))
                        {
                            foreach (StatisticalResponse data in res.Data)
                            {
                                string strUserName = lsUsers[res.Group["userId"]];
                                string strRecIntervalStart = data.Interval.Split('/')[0];
                                DateTime dRecIntervalStart = DateTime.Parse(strRecIntervalStart);
                                string strRecIntervalEnd = data.Interval.Split('/')[1];
                                //DateTime dRecIntervalEnd = DateTime.Parse(strRecIntervalEnd);
                                int nDuration = 30 * 60;

                                foreach (AggregateMetricData metric in data.Metrics)
                                {
                                    headers = "userName,userId,";
                                    values = "'" + strUserName.Replace("'", "''") + "','" + res.Group["userId"].Replace("'", "''") + "',";
                                    headers += "interval,dIntervalStart,dIntervalStartUTC,nDuration" + ",";
                                    values += "'" + data.Interval + "','" + dRecIntervalStart.ToLocalTime().ToString("s") + "','" + dRecIntervalStart.ToUniversalTime().ToString("s") + "'," + nDuration + ",";
                                    
                                    if (metric.Metric.ToLower().Equals("torganizationpresence") && (!(lsOrgPresences[metric.Qualifier].ToLower().Equals("offline"))))
                                    {
                                        headers += "OrgPresenceId, OrgPresenceName, tOrganizationPresence";
                                        values += "'" + metric.Qualifier + "','" + lsOrgPresences[metric.Qualifier] + "'," + metric.Stats.Sum.ToString();

                                        //Console.WriteLine(data.ToString());
                                        if (headers.EndsWith(",")) headers = headers.TrimEnd(',');
                                        if (values.EndsWith(",")) values = values.TrimEnd(',');

                                        //AggregateMetricData m = res.Data[0].Metrics.Find(o => o.Metric == "nOffered");
                                        sql.CommandText = "INSERT INTO gen.gcUserAggregates (" + headers + ") VALUES (" + values + ");";
                                        if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                        Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                                    }
                                    else if (metric.Metric.ToLower().Equals("tagentroutingstatus"))
                                    {
                                        headers += "RoutingStatusName, tAgentRoutingStatus";
                                        values += "'" + metric.Qualifier + "'," + metric.Stats.Sum.ToString();

                                        //Console.WriteLine(data.ToString());
                                        if (headers.EndsWith(",")) headers = headers.TrimEnd(',');
                                        if (values.EndsWith(",")) values = values.TrimEnd(',');

                                        //AggregateMetricData m = res.Data[0].Metrics.Find(o => o.Metric == "nOffered");
                                        sql.CommandText = "INSERT INTO gen.gcUserAggregates (" + headers + ") VALUES (" + values + ");";
                                        if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                        Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                                    }
                                    else if (!(metric.Metric.ToLower().Equals("torganizationpresence")) && !(metric.Metric.ToLower().Equals("tsystempresence")))
                                    {
                                        Console.WriteLine("!!!");
                                    }
                                }
                            }
                        }
                    }
                }
                strUserMatch = "";
                intUserMatch = 0;
                agBody.Filter.Predicates = new List<UserAggregateQueryPredicate>();
            }
            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateUserStatusDetails(string dbconnstring, string intervalFilter, Dictionary<String, String> lsUsers, Dictionary<String, String> lsOrgPresences, string useUserJobId="", string logMode = "Standard")
        {
            Console.WriteLine("Initializing User Status Details Population...");
            AnalyticsApi analApi = new AnalyticsApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            string strIntervalStart = intervalFilter.Split('/')[0];
            DateTime dIntervalStartUTC = DateTime.Parse(strIntervalStart);
            string strIntervalEnd = intervalFilter.Split('/')[1];
            DateTime dIntervalEndUTC = DateTime.Parse(strIntervalEnd);
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine("Creating User Status Details Query...");
            bool lastPage = false;
            int pageCounter = 1;
            int pageSize = 100;
            UserDetailsQuery ubody = new UserDetailsQuery();
            ubody.Interval = intervalFilter;
            ubody.UserFilters = new List<UserDetailQueryFilter>();
            ubody.PresenceFilters = new List<PresenceDetailQueryFilter>();
            ubody.RoutingStatusFilters = new List<RoutingStatusDetailQueryFilter>();
            //ubody.Paging = new PagingSpec(1000);
            ubody.Order = UserDetailsQuery.OrderEnum.Asc;
            //ubody.Order = AsyncUserDetailsQuery.OrderEnum.Asc;

            Console.WriteLine(DateTime.Now.ToString("s") + " - Retrieving results of User Status Details:");
            AnalyticsUserDetailsQueryResponse userQueryResults;

            do
            {
                ubody.Paging = new PagingSpec(PageSize: pageSize, PageNumber: pageCounter);
                userQueryResults = analApi.PostAnalyticsUsersDetailsQuery(ubody);

                if (!(userQueryResults.UserDetails is null))
                {
                    foreach (AnalyticsUserDetail userstatus in userQueryResults.UserDetails)
                    {
                        if (!(userstatus.PrimaryPresence is null))
                        {
                            foreach (AnalyticsUserPresenceRecord pres in userstatus.PrimaryPresence)
                            {
                                string headers = "", values = "";
                                headers += "UserId,StartTimeUTC,EndTimeUTC,SystemPresence,OrganizationPresenceId";
                                values += "'" + userstatus.UserId + "',";
                                values += "'" + pres.StartTime.Value.ToUniversalTime().ToString("s") + "',";
                                values += pres.EndTime.HasValue ? ("'" + pres.EndTime.Value.ToUniversalTime().ToString("s") + "',") : ("null,");
                                values += "'" + pres.SystemPresence.Value.ToString() + "',";
                                values += "'" + pres.OrganizationPresenceId + "'";
                                sql.CommandText = "INSERT INTO gen.gcUserStatusDetails (" + headers + ") VALUES (" + values + ")";
                                Console.WriteLine($"Executing: {sql.CommandText}");
                                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                            }
                        }

                        if (!(userstatus.RoutingStatus is null))
                        {
                            foreach (AnalyticsRoutingStatusRecord routs in userstatus.RoutingStatus)
                            {
                                string headers = "", values = "";
                                headers += "UserId,StartTimeUTC,EndTimeUTC,RoutingStatus";
                                values += "'" + userstatus.UserId + "',";
                                values += "'" + routs.StartTime.Value.ToUniversalTime().ToString("s") + "',";
                                values += routs.EndTime.HasValue ? ("'" + routs.EndTime.Value.ToUniversalTime().ToString("s") + "',") : ("null,");
                                values += "'" + routs.RoutingStatus.Value.ToString() + "'";
                                sql.CommandText = "INSERT INTO gen.gcUserRoutingStatus (" + headers + ") VALUES (" + values + ")";
                                if (logMode.Contains("Debug")) Console.WriteLine($"Executing: {sql.CommandText}");
                                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                            }
                        }
                    }
                    pageCounter++;
                    if (userQueryResults.UserDetails.Count < pageSize) lastPage = true;
                }
                else lastPage = true;
            } while (!lastPage);
            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void PopulateFlowAggregates(string dbconnstring, string intervalFilter, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Flows Population...");
            AnalyticsApi analApi = new AnalyticsApi();

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            string strIntervalStart = intervalFilter.Split('/')[0];
            DateTime dIntervalStartUTC = DateTime.Parse(strIntervalStart);
            string strIntervalEnd = intervalFilter.Split('/')[1];
            DateTime dIntervalEndUTC = DateTime.Parse(strIntervalEnd);
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            Console.WriteLine("Creating Flows Details Query...");
            FlowAggregationQuery fbody = new FlowAggregationQuery();
            fbody.Interval = intervalFilter;
            //fbody.TimeZone = timeZone;
            //fbody.Granularity = "PT30M";
            fbody.GroupBy = new List<FlowAggregationQuery.GroupByEnum>() { 
                FlowAggregationQuery.GroupByEnum.Conversationid,
                FlowAggregationQuery.GroupByEnum.Flowid, 
                FlowAggregationQuery.GroupByEnum.Flowname, 
                FlowAggregationQuery.GroupByEnum.Ani, 
                FlowAggregationQuery.GroupByEnum.Direction, 
                FlowAggregationQuery.GroupByEnum.Dnis, 
                FlowAggregationQuery.GroupByEnum.Entryreason, 
                FlowAggregationQuery.GroupByEnum.Entrytype, 
                FlowAggregationQuery.GroupByEnum.Exitreason, 
                //FlowAggregationQuery.GroupByEnum.Flowintype, 
                //FlowAggregationQuery.GroupByEnum.Flowmilestoneid, 
                FlowAggregationQuery.GroupByEnum.Flowoutcome, 
                FlowAggregationQuery.GroupByEnum.Flowoutcomeid, 
                FlowAggregationQuery.GroupByEnum.Flowoutcomevalue, 
                FlowAggregationQuery.GroupByEnum.Flowouttype, 
                FlowAggregationQuery.GroupByEnum.Flowtype, 
                FlowAggregationQuery.GroupByEnum.Flowversion, 
                FlowAggregationQuery.GroupByEnum.Interactiontype, 
                FlowAggregationQuery.GroupByEnum.Mediatype, 
                FlowAggregationQuery.GroupByEnum.Purpose
            };
            fbody.Metrics = new List<FlowAggregationQuery.MetricsEnum>() { 
                FlowAggregationQuery.MetricsEnum.Nflow, 
                //FlowAggregationQuery.MetricsEnum.Nflowmilestone, 
                FlowAggregationQuery.MetricsEnum.Nflowoutcome, 
                FlowAggregationQuery.MetricsEnum.Nflowoutcomefailed, 
                //FlowAggregationQuery.MetricsEnum.Oflowmilestone, 
                FlowAggregationQuery.MetricsEnum.Tflow, 
                FlowAggregationQuery.MetricsEnum.Tflowdisconnect, 
                FlowAggregationQuery.MetricsEnum.Tflowexit, 
                FlowAggregationQuery.MetricsEnum.Tflowoutcome 
            };
            //fbody.Filter = new FlowAggregateQueryFilter();
            
            Console.WriteLine(DateTime.Now.ToString("s") + " - Retrieving results of Flow Aggregates:");
            FlowAggregateQueryResponse flowQueryResults;

            flowQueryResults = analApi.PostAnalyticsFlowsAggregatesQuery(fbody);
            if (flowQueryResults.Results is null)
            {
                Console.WriteLine("No Flow Aggregate Results");
                return;
            }
            foreach (FlowAggregateDataContainer f in flowQueryResults.Results)
            {
                Console.WriteLine(f.Data.ToString());
            }

            
            cnn.Close();
            Console.WriteLine("------------------------------------------------------------------------\n\n");
        }

        public static void CleanData(string dbconnstring, DateTime dIntervalStartUTC, DateTime dIntervalEndUTC, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Data Clean...");

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            try
            {
                Console.WriteLine(DateTime.Now.ToString("s") + " - Running sp_gciCleanData...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_gciCleanData";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while cleaning GC tables...");
                Console.WriteLine(e.ToString());
                //System.Environment.Exit(10);
                throw (e);

            }
        }

        public static void TransformIntermediateData(string dbconnstring, DateTime dIntervalStartUTC, DateTime dIntervalEndUTC, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Data Transformation...");

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            try
            {
                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_gciOutboundAttempts...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_gciOutboundAttempts";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");

                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_gciTaskTracker...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_gciTaskTracker";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");

                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_gciIVRSurveyResults...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_gciIVRSurveyResults";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while transforming to Intermediate database tables...");
                Console.WriteLine(e.ToString());
                //System.Environment.Exit(10);
                throw (e);

            }
        }

        public static void TransformPureConnectData(string dbconnstring, DateTime dIntervalStartUTC, DateTime dIntervalEndUTC, string logMode = "Standard")
        {
            Console.WriteLine("Initializing Data Transformation...");

            SqlConnection cnn;
            cnn = new SqlConnection(dbconnstring);
            cnn.Open();
            SqlCommand sql = cnn.CreateCommand();
            SqlDataAdapter da = new SqlDataAdapter(sql);
            SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn);

            try
            {
                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_AgentActivityLog...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_AgentActivityLog";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");

                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_IAgentQueueStats...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_IAgentQueueStats";
                sql.CommandType = CommandType.StoredProcedure;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");

                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_InteractionSummary...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_InteractionSummary";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");

                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_IWrkGrpQueueStats...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_scoringsummary_viw";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");

                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_ScoringDetail_viw...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_ScoringDetail_viw";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");

                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_scoringsummary_viw...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_IWrkgrpQueueStats";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");

                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_vcssSurveyScoringDetail...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_vcssSurveyScoringDetail";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");

                Console.WriteLine(DateTime.Now.ToString("s") + " - Populating sp_vcssSurveyScoringSummary...");
                sql = cnn.CreateCommand();
                sql.CommandText = "gen.sp_vcssSurveyScoringSummary";
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandTimeout = 600;
                sql.Parameters.Add(new SqlParameter("procStartDate", dIntervalStartUTC));
                sql.Parameters.Add(new SqlParameter("procEndDate", dIntervalEndUTC));
                Console.WriteLine($"Executing: {sql.CommandText}");
                Console.WriteLine($"Affected rows: {sql.ExecuteNonQuery()}");
                Console.WriteLine("------------------------------------------------------------------------\n\n");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while transforming to PureConnect database tables...");
                Console.WriteLine(e.ToString());
                //System.Environment.Exit(10);
                throw (e);

            }
        }

        public static Dictionary<String, String> GetUsers()
        {
            Console.WriteLine("Getting List of Users...");
            UsersApi userApi = new UsersApi();
            int currPage = 1;
            Dictionary<String, String> lsUsers = new Dictionary<string, string>();

            Console.WriteLine(DateTime.Now.ToString("s") + " - Getting List of Users...");
            UserEntityListing users = new UserEntityListing(PageCount: 0, PageSize: 0);
            do
            {
                users = userApi.GetUsers(pageSize: 100, pageNumber: currPage);
                currPage += 1;
                foreach (User u in users.Entities)
                {
                    lsUsers.Add(u.Id, u.Name);
                }

            } while (users.PageNumber < users.PageCount);

            Console.WriteLine("------------------------------------------------------------------------\n\n");
            return lsUsers;
        }

        public static Dictionary<String, String> GetOrgPresences()
        {
            Console.WriteLine("Getting List of Organization Presences...");
            PresenceApi presApi = new PresenceApi();
            Dictionary<String, String> lsOrgPresences = new Dictionary<string, string>();
            OrganizationPresenceEntityListing orgps = presApi.GetPresencedefinitions(pageSize: 100, deleted: "false");

            foreach (OrganizationPresence op in orgps.Entities)
            {
                lsOrgPresences.Add(op.Id, (op.Name ?? op.SystemPresence));
            }
            orgps = presApi.GetPresencedefinitions(pageSize: 100, deleted: "true");
            foreach (OrganizationPresence op in orgps.Entities)
            {
                if (!(lsOrgPresences.ContainsKey(op.Id))) lsOrgPresences.Add(op.Id, (op.Name ?? op.SystemPresence));
            }
            Console.WriteLine("------------------------------------------------------------------------\n\n");

            return lsOrgPresences;
        }
    }
}
