CREATE OR ALTER PROCEDURE gen.sp_IAgentQueueStats
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
AS

DELETE FROM gen.IAgentQueueStats WHERE dIntervalStartUTC BETWEEN @procStartDate and @procEndDate;

--TRUNCATE TABLE [gen].[IWrkgrpQueueStats];

--------------------------------------------------------------------------------------------------------------------------------
-- Insert the split-interval data into a temp table #TEMPTABLE_WRKGRP
--------------------------------------------------------------------------------------------------------------------------------

SELECT 
	conv.dIntervalStart,
	conv.dIntervalStartUTC, 
	--convaggq.mediaType,
	convaggq.QueueId,
	convaggq.UserId,
	CASE WHEN u.UserName IS NULL THEN '(Missing User Name)' ELSE u.UserName END as cName,
	MAX(CASE WHEN LOWER(convaggq.mediaType) = 'email' and q.QueueName = 'TaskTracker' then SUBSTRING(REPLACE(convaggq.Remote, '.com', ''), 0, CHARINDEX('@', convaggq.Remote)) else q.QueueName end) as cReportGroup,
	CASE when LOWER(convaggq.mediaType) = 'email' and q.QueueName = 'TaskTracker' then 'TaskTracker' else LOWER(convaggq.mediaType) end as cHKey3,
	MAX(CASE WHEN LOWER(convaggq.mediaType) = 'email' and q.QueueName = 'TaskTracker' then SUBSTRING(REPLACE(convaggq.Remote, '.com', ''), CHARINDEX('@', convaggq.Remote)+1, LEN(convaggq.Remote)) else '*' end) as cHKey4,

	SUM(nOffered) as nOffered,
	SUM(nAbandon) as nAbandon,
	SUM(tAbandon) as tAbandon,
	SUM(maxAbandon) as maxAbandon,
	SUM(nOverSla) as nOverSla,
	SUM(nFlowOut) as nFlowOut,
	SUM(tFlowOut) as tFlowOut,
	SUM(nVoicemail) as nVoicemail,

	SUM(nBlindTransferred) as nBlindTransferred,
	SUM(nCobrowseSessions) as nCobrowseSessions, 
	SUM(nConsult) as nConsult, 
	SUM(nConsultTransferred) as nConsultTransferred, 
	SUM(nError) as nError, 
	SUM(nOutbound) as nOutbound, 
	SUM(nOutboundAbandoned) as nOutboundAbandoned, 
	SUM(nOutboundAttempted) as nOutboundAttempted, 
	SUM(nOutboundConnected) as nOutboundConnected, 
	SUM(nTransferred) as nTransferred, 
	NULL as tAcw, --SUM(tAcw) as tAcw, 
	SUM(nAcw) as nAcw, 
	SUM(maxAcw) as maxAcw, 
	SUM(tAgentResponseTime) as tAgentResponseTime, 
	SUM(nAgentResponseTime) as nAgentResponseTime, 
	SUM(maxAgentResponseTime) as maxAgentResponseTime, 
	NULL as tAlert, --SUM(tAlert) as tAlert, 
	SUM(nAlert) as nAlert, 
	SUM(maxAlert) as maxAlert, 
	SUM(tAnswered) as tAnswered, 
	SUM(nAnswered) as nAnswered, 
	SUM(maxAnswered) as maxAnswered, 
	SUM(tCallback) as tCallback, 
	SUM(nCallback) as nCallback, 
	SUM(maxallback) as maxallback, 
	SUM(tCallbackComplete) as tCallbackComplete, 
	SUM(nCallbackComplete) as nCallbackComplete, 
	SUM(maxCallbackComplete) as maxCallbackComplete, 
	NULL as tContacting, --SUM(tContacting) as tContacting, 
	SUM(nContacting) as nContacting, 
	SUM(convaggq.maxContacting) as maxContacting, 
	NULL as tDialing, --SUM(convaggq.tDialing) as tDialing, 
	SUM(convaggq.nDialing) as nDialing, 
	SUM(convaggq.maxDialing) as maxDialing, 
	NULL as tHandle, --SUM(convaggq.tHandle) as tHandle, 
	SUM(convaggq.nHandle) as nHandle, 
	SUM(convaggq.maxHandle) as maxHandle, 
	NULL as tHeld, --SUM(convaggq.tHeld) as tHeld, 
	SUM(convaggq.nHeld) as nHeld, 
	SUM(convaggq.maxHeld) as maxHeld, 
	SUM(convaggq.tHeldComplete) as tHeldComplete, 
	SUM(convaggq.nHeldComplete) as nHeldComplete, 
	SUM(convaggq.maxHeldComplete) as maxHeldComplete, 
	SUM(convaggq.tMonitoring) as tMonitoring, 
	SUM(convaggq.nMonitoring) as nMonitoring, 
	SUM(convaggq.maxMonitoring) as maxMonitoring, 
	SUM(convaggq.tNotResponding) as tNotResponding, 
	SUM(convaggq.nNotResponding) as nNotResponding, 
	SUM(convaggq.maxNotResponding) as maxNotResponding, 
	NULL as tTalk, --SUM(convaggq.tTalk) as tTalk, 
	SUM(convaggq.nTalk) as nTalk, 
	SUM(convaggq.maxTalk) as maxTalk, 
	SUM(convaggq.tTalkComplete) as tTalkComplete, 
	SUM(convaggq.nTalkComplete) as nTalkComplete, 
	SUM(convaggq.maxTalkComplete) as maxTalkComplete, 
	SUM(convaggq.tUserResponseTime) as tUserResponseTime, 
	SUM(convaggq.nUserResponseTime) as nUserResponseTime, 
	SUM(convaggq.maxUserResponseTime + convaggq.maxUserResponseTime) as maxUserResponseTime
INTO #TEMPTABLE_WRKGRP
FROM gen.gcConversations conv
	LEFT JOIN gen.gcConversationAggregates convaggq ON convaggq.dIntervalStartUTC BETWEEN @procStartDate and @procEndDate
		AND convaggq.conversationId = conv.ConversationId
		AND convaggq.queueId IS NOT NULL
	LEFT JOIN gen.gcRoutingQueues q ON q.QueueId = convaggq.queueId
	LEFT JOIN gen.gcUsers u ON u.UserId = convaggq.userId
WHERE 
	conv.ConversationStartUTC BETWEEN @procStartDate and @procEndDate
	AND convaggq.mediaType IS NOT NULL 
	AND convaggq.QueueId IS NOT NULL
	AND convaggq.UserId IS NOT NULL
GROUP BY 
	conv.dIntervalStart,
	conv.dIntervalStartUTC, 
	convaggq.mediaType,
	convaggq.QueueId,
	q.QueueName,
	convaggq.UserId,
	u.UserName
;


--------------------------------------------------------------------------------------------------------------------------------
-- Split the durations for Handle Time components into separate time buckets into #TEMPTABLE_WRKGRP
-- Note that only these metrics are split into time interval buckets. 
-- Other metrics not listed below will be attributed to the interval where the call started.
--------------------------------------------------------------------------------------------------------------------------------

INSERT INTO #TEMPTABLE_WRKGRP (dIntervalStart, dIntervalStartUTC, QueueId, UserId, cName, cReportGroup, cHKey3,cHKey4,tAcw,tAlert,tContacting,tDialing,tHandle,tHeld,tTalk) 
SELECT 
	dt.dInterval,
	dt.dIntervalUTC, 
	q.queueId,
	u.UserId,
	CASE WHEN u.UserName IS NULL THEN '(Missing User Name)' ELSE u.UserName END as cName,
	MAX(CASE WHEN LOWER(sess.mediaType) = 'email' and q.QueueName = 'TaskTracker' then SUBSTRING(REPLACE(sess.Remote, '.com', ''), 0, CHARINDEX('@', sess.Remote)) else q.QueueName end) as cReportGroup,
	CASE when LOWER(sess.mediaType) = 'email' and q.QueueName = 'TaskTracker' then 'TaskTracker' else LOWER(sess.mediaType) end as cHKey3,
	MAX(CASE WHEN LOWER(sess.mediaType) = 'email' and q.QueueName = 'TaskTracker' then SUBSTRING(REPLACE(sess.Remote, '.com', ''), CHARINDEX('@', sess.Remote)+1, LEN(sess.Remote)) else '*' end) as cHKey4,
	
	(SUM(CASE 
		WHEN seg.SegmentType = 'Wrapup' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Wrapup' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Wrapup' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Wrapup' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, dt.dIntervalUTC, seg.SegmentEndUTC)
	END)) AS tAcw,
	(SUM(CASE 
		WHEN seg.SegmentType = 'Alert' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Alert' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Alert' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Alert' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, dt.dIntervalUTC, seg.SegmentEndUTC)
	END)) AS tAlert,
	(SUM(CASE 
		WHEN seg.SegmentType = 'Contacting' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Contacting' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Contacting' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Contacting' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, dt.dIntervalUTC, seg.SegmentEndUTC)
	END)) AS tContacting,
	(SUM(CASE 
		WHEN seg.SegmentType = 'Dialing' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Dialing' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Dialing' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Dialing' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, dt.dIntervalUTC, seg.SegmentEndUTC)
	END)) AS tDialing,
	(SUM(CASE 
		WHEN seg.SegmentType IN ('Contacting','Interact','Hold','Wrapup') AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType IN ('Contacting','Interact','Hold','Wrapup') AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType IN ('Contacting','Interact','Hold','Wrapup') AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType IN ('Contacting','Interact','Hold','Wrapup') AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, dt.dIntervalUTC, seg.SegmentEndUTC)
	END)) AS tHandle,
	(SUM(CASE 
		WHEN seg.SegmentType = 'Hold' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Hold' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Hold' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Hold' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, dt.dIntervalUTC, seg.SegmentEndUTC)
	END)) as tHeld,
	(SUM(CASE 
		WHEN seg.SegmentType IN ('Contacting','Interact') AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType IN ('Contacting','Interact') AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType IN ('Contacting','Interact') AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType IN ('Contacting','Interact') AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, dt.dIntervalUTC, seg.SegmentEndUTC)
	END)) as tTalk



	/*
	0 as nOffered,
	0 as nAbandon,
	0 as tAbandon,
	0 as maxAbandon,
	0 as nOverSla,
	0 as nFlowOut,
	0 as tFlowOut,
	0 as nVoicemail,

	0 as nBlindTransferred,
	0 as nCobrowseSessions, 
	0 as nConsult, 
	0 as nConsultTransferred, 
	0 as nError, 
	0 as nOutbound, 
	0 as nOutboundAbandoned, 
	0 as nOutboundAttempted, 
	0  as nOutboundConnected, 
	0 as nTransferred, 
	
	SUM(CASE 
		WHEN seg.SegmentType = 'Wrapup' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Wrapup' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Wrapup' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Wrapup' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentEndUTC, dt.dIntervalEndUTC)
	END) AS tAcw,

	0 as nAcw, 
	0 as maxAcw, 
	0 as tAgentResponseTime, 
	0 as nAgentResponseTime, 
	0 as maxAgentResponseTime, 

	--0 as tAlert, --SUM(tAlert) as tAlert,
	SUM(CASE 
		WHEN seg.SegmentType = 'Alert' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Alert' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Alert' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Alert' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentEndUTC, dt.dIntervalEndUTC)
	END) AS tAlert,

	0 as nAlert, 
	0 as maxAlert, 
	0 as tAnswered, 
	0 as nAnswered, 
	0 as maxAnswered, 
	0 as tCallback, 
	0 as nCallback, 
	0 as maxallback, 
	0 as tCallbackComplete, 
	0 as nCallbackComplete, 
	0 as maxCallbackComplete, 

	-- SUM(tContacting) as tContacting, 
	SUM(CASE 
		WHEN seg.SegmentType = 'Contacting' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Contacting' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Contacting' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Contacting' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentEndUTC, dt.dIntervalEndUTC)
	END) AS tContacting,
	
	0 as nContacting, 
	0 as maxContacting, 
	
	--0 as tDialing, --SUM(convaggq.tDialing) as tDialing, 
	SUM(CASE 
		WHEN seg.SegmentType = 'Dialing' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Dialing' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Dialing' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Dialing' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentEndUTC, dt.dIntervalEndUTC)
	END) AS tDialing,

	0 as nDialing, 
	0  as maxDialing, 
	
	0 as tHandle, --SUM(convaggq.tHandle) as tHandle, 
	SUM(CASE 
		WHEN seg.SegmentType IN ('Contacting','Interact','Hold','Wrapup') AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType IN ('Contacting','Interact','Hold','Wrapup') AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType IN ('Contacting','Interact','Hold','Wrapup') AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType IN ('Contacting','Interact','Hold','Wrapup') AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentEndUTC, dt.dIntervalEndUTC)
	END) AS tHandle,

	0 as nHandle, 
	0 as maxHandle, 
	
	--0 as tHeld, --SUM(convaggq.tHeld) as tHeld, 
	SUM(CASE 
		WHEN seg.SegmentType = 'Hold' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType = 'Hold' AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType = 'Hold' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType = 'Hold' AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentEndUTC, dt.dIntervalEndUTC)
	END) as tHeld,
	
	0 as nHeld, 
	0 as maxHeld, 
	0 as tHeldComplete, 
	0 as nHeldComplete, 
	0 as maxHeldComplete, 
	0 as tMonitoring, 
	0 as nMonitoring, 
	0 as maxMonitoring, 
	0 as tNotResponding, 
	0 as nNotResponding, 
	0 as maxNotResponding, 
	
	--0 as tTalk, --SUM(convaggq.tTalk) as tTalk, 
	SUM(CASE 
		WHEN seg.SegmentType IN ('Contacting','Interact') AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC <= dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, seg.SegmentEndUTC)
		WHEN seg.SegmentType IN ('Contacting','Interact') AND (seg.SegmentStartUTC >= dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentStartUTC, dt.dIntervalEndUTC)
		WHEN seg.SegmentType IN ('Contacting','Interact') AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC > dt.dIntervalEndUTC) THEN 1800
		WHEN seg.SegmentType IN ('Contacting','Interact') AND (seg.SegmentStartUTC < dt.dIntervalUTC) and (seg.SegmentEndUTC < dt.dIntervalEndUTC) THEN DATEDIFF(ss, seg.SegmentEndUTC, dt.dIntervalEndUTC)
	END) as tTalk,

	0 as nTalk, 
	0 as maxTalk, 
	0 as tTalkComplete, 
	0 as nTalkComplete, 
	0 as maxTalkComplete, 
	0 as tUserResponseTime, 
	0 as nUserResponseTime, 
	0 as maxUserResponseTime
	*/
FROM gen.gcConversationSegments seg
	LEFT JOIN gen.gcConversationSessions sess 
		ON sess.ConversationStartUTC BETWEEN @procStartDate and @procEndDate
	 AND sess.SessionId = seg.ConversationSessionId
	LEFT JOIN gen.gcRoutingQueues q ON q.QueueId = sess.FirstQueueId
	LEFT JOIN gen.gcUsers u ON u.UserId = sess.UserId
	LEFT JOIN gen.dIntervals dt 
		ON (seg.SegmentStartUTC >= dt.dIntervalUTC and seg.SegmentStartUTC <= dt.dIntervalEndUTC)
		OR (seg.SegmentEndUTC >= dt.dIntervalUTC and seg.SegmentEndUTC <= dt.dIntervalEndUTC)
		OR (seg.SegmentStartUTC < dt.dIntervalUTC and seg.SegmentEndUTC > dt.dIntervalEndUTC)
	WHERE seg.ConversationStartUTC BETWEEN @procStartDate and @procEndDate
	AND sess.FirstQueueId IS NOT NULL
	AND sess.UserId IS NOT NULL
	AND u.UserId IS NOT NULL
GROUP BY 
	dt.dInterval,
	dt.dIntervalUTC, 
	sess.mediaType,
	q.QueueId,
	q.QueueName,
	sess.Remote,
	u.UserId,
	u.UserName
;


--------------------------------------------------------------------------------------------------------------------------------
-- Combine the combined-interval and split-interval data into a temp table
--------------------------------------------------------------------------------------------------------------------------------

SELECT 
	dIntervalStart,
	dIntervalStartUTC, 
	queueId,
	userId,
	cName,
	cReportGroup,
	cHKey3,
	cHKey4,

	SUM(nOffered) as nOffered,
	SUM(nAbandon) as nAbandon,
	SUM(tAbandon) as tAbandon,
	MAX(maxAbandon) as maxAbandon,
	SUM(nOverSla) as nOverSla,
	SUM(nFlowOut) as nFlowOut,
	SUM(tFlowOut) as tFlowOut,
	SUM(nVoicemail) as nVoicemail,

	SUM(nBlindTransferred) as nBlindTransferred,
	SUM(nCobrowseSessions) as nCobrowseSessions, 
	SUM(nConsult) as nConsult, 
	SUM(nConsultTransferred) as nConsultTransferred, 
	SUM(nError) as nError, 
	SUM(nOutbound) as nOutbound, 
	SUM(nOutboundAbandoned) as nOutboundAbandoned, 
	SUM(nOutboundAttempted) as nOutboundAttempted, 
	SUM(nOutboundConnected) as nOutboundConnected, 
	SUM(nTransferred) as nTransferred, 
	SUM(tAcw) as tAcw, 
	SUM(nAcw) as nAcw, 
	MAX(maxAcw) as maxAcw, 
	SUM(tAgentResponseTime) as tAgentResponseTime, 
	SUM(nAgentResponseTime) as nAgentResponseTime, 
	MAX(maxAgentResponseTime) as maxAgentResponseTime, 
	SUM(tAlert) as tAlert, 
	SUM(nAlert) as nAlert, 
	MAX(maxAlert) as maxAlert, 
	SUM(tAnswered) as tAnswered, 
	SUM(nAnswered) as nAnswered, 
	MAX(maxAnswered) as maxAnswered, 
	SUM(tCallback) as tCallback, 
	SUM(nCallback) as nCallback, 
	MAX(maxallback) as maxallback, 
	SUM(tCallbackComplete) as tCallbackComplete, 
	SUM(nCallbackComplete) as nCallbackComplete, 
	MAX(maxCallbackComplete) as maxCallbackComplete, 
	SUM(tContacting) as tContacting, 
	SUM(nContacting) as nContacting, 
	MAX(maxContacting) as maxContacting, 
	SUM(tDialing) as tDialing, 
	SUM(nDialing) as nDialing, 
	SUM(maxDialing) as maxDialing, 
	SUM(tHandle) as tHandle, 
	SUM(nHandle) as nHandle, 
	MAX(maxHandle) as maxHandle, 
	SUM(tHeld) as tHeld, 
	SUM(nHeld) as nHeld, 
	MAX(maxHeld) as maxHeld, 
	SUM(tHeldComplete) as tHeldComplete, 
	SUM(nHeldComplete) as nHeldComplete, 
	MAX(maxHeldComplete) as maxHeldComplete, 
	SUM(tMonitoring) as tMonitoring, 
	SUM(nMonitoring) as nMonitoring, 
	MAX(maxMonitoring) as maxMonitoring, 
	SUM(tNotResponding) as tNotResponding, 
	SUM(nNotResponding) as nNotResponding, 
	MAX(maxNotResponding) as maxNotResponding, 
	SUM(tTalk) as tTalk, 
	SUM(nTalk) as nTalk, 
	MAX(maxTalk) as maxTalk, 
	SUM(tTalkComplete) as tTalkComplete, 
	SUM(nTalkComplete) as nTalkComplete, 
	MAX(maxTalkComplete) as maxTalkComplete, 
	SUM(tUserResponseTime) as tUserResponseTime, 
	SUM(nUserResponseTime) as nUserResponseTime, 
	MAX(maxUserResponseTime) as maxUserResponseTime
INTO #TEMPTABLE_WRKGRP2
FROM #TEMPTABLE_WRKGRP
GROUP BY 
	dIntervalStart,
	dIntervalStartUTC, 
	queueId,
	userId,
	cName,
	cReportGroup,
	cHKey3,
	cHKey4
;

--------------------------------------------------------------------------------------------------------------------------------
-- Populate the IWrkGrpQueueStats Table
--------------------------------------------------------------------------------------------------------------------------------

INSERT INTO [gen].IAgentQueueStats
(
	--[StatisticsSet],
	[cName],[cReportGroup],[cHKey3],[cHKey4],[cType],[dIntervalStart],[dIntervalStartUTC],[nDuration],[nEnteredAcd],[nAbandonedAcd],[nGrabbedAcd],[nLocalDisconnectAcd],[nLocalDisconnectAgentAlertAcd],[nLocalDisconnectAgentAcd],[nAlertedAcd],[nAnsweredAcd],[nAnswered],[nAcdSvcLvl],[nAnsweredAcdSvcLvl],
	[nAnsweredAcdSvcLvl1],[nAnsweredAcdSvcLvl2],[nAnsweredAcdSvcLvl3],[nAnsweredAcdSvcLvl4],[nAnsweredAcdSvcLvl5],[nAnsweredAcdSvcLvl6],[nAbandonAcdSvcLvl],[nAbandonAcdSvcLvl1],[nAbandonAcdSvcLvl2],[nAbandonAcdSvcLvl3],[nAbandonAcdSvcLvl4],[nAbandonAcdSvcLvl5],[nAbandonAcdSvcLvl6],
	[tGrabbedAcd],[tAnsweredAcd],[mtAnsweredAcd],[tAbandonedAcd],[tTalkAcd],[tTalkCompleteAcd],[nHoldAcd],[tHoldAcd],[nSuspendedAcd],[tSuspendedAcd],[nHeldSpanAcd],[nAcw],[tAcw],[tAcwComplete],
	[nExternToInternCalls],[nExternToInternAcdCalls],[nInternToExternCalls],[nInternToExternAcdCalls],[nInternToInternCalls],[nInternToInternAcdCalls],[tExternToInternCalls],[tExternToInternAcdCalls],[tInternToExternCalls],[tInternToExternAcdCalls],[tInternToInternCalls],[tInternToInternAcdCalls],
	[nAcwCalls],[tAcwCalls],[nTransferedAcd],nConsult,nBlindTransferred,nConsultTransferred,[nNotAnsweredAcd],[tAlertedAcd],[nFlowOutAcd],[tFlowOutAcd],[nStartWaitAlertAcdCalls],[nStartActiveAcdCalls],[nStartHeldAcdCalls],[nEndWaitAlertAcdCalls],
	[nEndActiveAcdCalls],[nEndHeldAcdCalls],[nTransferWithinAcdCalls],[nTransferOutAcdCalls],[nDisconnectAcd],[tAgentLoggedIn],[tAgentAvailable],[tAgentTalk],[tAgentOtherBusy],
	[tAgentOnAcdCall],[tAgentOnOtherAcdCall],[tAgentInAcw],[tAgentOnNonAcdCall],[tAgentDnd],[tAgentNotAvailable],[tAgentAcdLoggedIn],[tAgentAcdLoggedIn2],[tAgentStatusDnd],[tAgentStatusAcw],[tAgentLoggedInDiluted],[tStatusGroupFollowup],[tStatusGroupBreak],[tStatusGroupTraining],
	[CustomValue1],[CustomValue2],[CustomValue3],[CustomValue4],[CustomValue5],[CustomValue6],
	[nAllInternToExternCalls],[nAllInternToExternAcdCalls],[tAllInternToExternCalls],[tAllInternToExternAcdCalls],[tAllTalkAcd],[tAllHoldAcd],[tAllAgentOnAcdCall],[nAllInternToInternCalls],[nAllExternToInternCalls],[nAllExternToInternAcdCalls],[tAllExternToInternCalls],[tAllExternToInternAcdCalls],[tAllAgentAvailable],[tAllAgentInAcw],[tAllAgentStatusDnd],[tAllAgentLoggedIn],[tAllAgentAcdLoggedIn2],[tAllAgentAcdLoggedIn],
	[SiteId],[SubSiteId],
	[I3TimeStampGMT],[DimensionSet],[SummDimensionSet],[ConfigurationSet],[nServiceLevel],[cServiceLevels],[SchemaMajorVersion],[SchemaMinorVersion]
)
SELECT 
	--COALESCE(u.UserName,'') as cName,
	--LEFT(COALESCE(rq.QueueName,''), 20) as cReportGroup,
	--qagg.mediaType as cHKey3,
	--'*' as cHKey4,
	COALESCE(qagg.cName,'') as cName,
	LEFT(COALESCE(qagg.cReportGroup,''), 20) as cReportGroup,
	qagg.cHKey3 as cHKey3,
	qagg.cHKey4 as cHKey4,
	'W' as cType,
	qagg.dIntervalStart as dIntervalStart,
	qagg.dIntervalStartUTC as dIntervalStartUTC,
	--qagg.nDuration as nDuration,
	1800 as nDuration,

	COALESCE(qagg.nOffered,0) as nEnteredAcd, 
	COALESCE(qagg.nAbandon,0) as nAbandonedAcd,
	0 as nGrabbedAcd, -- unused
	0 as nLocalDisconnectAcd, 
	(COALESCE(qagg.nAlert,0) - COALESCE(qagg.nAnswered,0)) as nLocalDisconnectAgentAlertAcd, -- not in Genesys Cloud
	0 as nLocalDisconnectAgentAcd, -- not in Genesys Cloud
	COALESCE(qagg.nAlert,0) as nAlertedAcd,
	COALESCE(qagg.nAnswered,0) as nAnsweredAcd, 
	COALESCE(qagg.nAnswered,0) as nAnswered,
	COALESCE(rq.DurationMs,0)/1000 as nAcdSvcLvl,
	(COALESCE(qagg.nAnswered,0) - COALESCE(qagg.nOverSla,0)) as nAnsweredAcdSvcLvl, 
	0 as nAnsweredAcdSvcLvl1, -- not in Genesys Cloud
	0 as nAnsweredAcdSvcLvl2, -- not in Genesys Cloud
	0 as nAnsweredAcdSvcLvl3, -- not in Genesys Cloud
	0 as nAnsweredAcdSvcLvl4, -- not in Genesys Cloud
	0 as nAnsweredAcdSvcLvl5, -- not in Genesys Cloud
	0 as nAnsweredAcdSvcLvl6, -- not in Genesys Cloud
	0 as nAbandonAcdSvcLvl, -- not in Genesys Cloud
	0 as nAbandonAcdSvcLvl1, -- not in Genesys Cloud
	0 as nAbandonAcdSvcLvl2, -- not in Genesys Cloud
	0 as nAbandonAcdSvcLvl3, -- not in Genesys Cloud
	0 as nAbandonAcdSvcLvl4, -- not in Genesys Cloud
	0 as nAbandonAcdSvcLvl5, -- not in Genesys Cloud
	0 as nAbandonAcdSvcLvl6, -- not in Genesys Cloud
	0 as tGrabbedAcd, -- unused
	COALESCE(qagg.tAnswered,0)/1000 as tAnsweredAcd,
	COALESCE(qagg.maxAnswered,0)/1000 as mtAnsweredAcd,
	COALESCE(qagg.tAbandon,0)/1000 as tAbandonedAcd,
	COALESCE(qagg.tTalk,0)/1000 as tTalkAcd,
	0 as tTalkCompleteAcd, -- not in Genesys Cloud
	COALESCE(qagg.nHeld,0) as nHoldAcd,
	COALESCE(qagg.tHeld,0)/1000 as tHoldAcd,
	0 as nSuspendedAcd, -- unused
	0 as tSuspendedAcd, -- unused
	0 as nHeldSpanAcd, -- unused
	COALESCE(qagg.nAcw,0) as nAcw,
	COALESCE(qagg.tAcw,0)/1000 as tAcw,
	0 as tAcwComplete, -- not in Genesys Cloud
	0 as nExternToInternCalls, -- not in Genesys Cloud
	0 as nExternToInternAcdCalls, -- not in Genesys Cloud
	0 as nInternToExternCalls, -- not in Genesys Cloud
	0 as nInternToExternAcdCalls, -- not in Genesys Cloud
	0 as nInternToInternCalls, -- not in Genesys Cloud
	0 as nInternToInternAcdCalls, -- not in Genesys Cloud
	0 as tExternToInternCalls, -- not in Genesys Cloud
	0 as tExternToInternAcdCalls, -- not in Genesys Cloud
	0 as tInternToExternCalls, -- not in Genesys Cloud
	0 as tInternToExternAcdCalls, -- not in Genesys Cloud
	0 as tInternToInternCalls, -- not in Genesys Cloud
	0 as tInternToInternAcdCalls, -- not in Genesys Cloud
	0 as nAcwCalls,
	0 as tAcwCalls,
	(COALESCE(qagg.nBlindTransferred,0) + COALESCE(qagg.nConsultTransferred,0)) as nTransferedAcd,
	(COALESCE(qagg.nConsult,0)) as nConsult,
	(COALESCE(qagg.nBlindTransferred,0)) as nBlindTransferred,
	(COALESCE(qagg.nConsultTransferred,0)) as nConsultTransferred,
	COALESCE(qagg.nNotResponding,0) as nNotAnsweredAcd,
	COALESCE(qagg.tAlert,0)/1000 as tAlertedAcd,
	COALESCE(qagg.nFlowOut,0) as nFlowOutAcd,
	COALESCE(qagg.tFlowOut,0)/1000 as tFlowOutAcd,
	0 as nStartWaitAlertAcdCalls, -- not in Genesys Cloud
	0 as nStartActiveAcdCalls, -- not in Genesys Cloud
	0 as nStartHeldAcdCalls, -- not in Genesys Cloud
	0 as nEndWaitAlertAcdCalls, -- not in Genesys Cloud
	0 as nEndActiveAcdCalls, -- not in Genesys Cloud
	0 as nEndHeldAcdCalls, -- not in Genesys Cloud
	0 as nTransferWithinAcdCalls, -- unused
	0 as nTransferOutAcdCalls, -- unused
	0 as nDisconnectAcd, -- not in Genesys Cloud
	COALESCE(uqagg2.LoggedIn,0) /1000 as tAgentLoggedIn,
	COALESCE(uqagg2.Available,0) /1000 as tAgentAvailable,
	COALESCE(qagg.tTalk,0)/1000 as tAgentTalk,
	0 as tAgentOtherBusy,
	(COALESCE(qagg.tTalk,0)/1000 + COALESCE(qagg.tHeld,0)/1000 + COALESCE(qagg.tAcw,0)/1000) as tAgentOnAcdCall,
	0 as tAgentOnOtherAcdCall,
	COALESCE(qagg.tAcw,0)/1000 as tAgentInAcw,
	0 as tAgentOnNonAcdCall,
	COALESCE(uqagg2.Busy,0)/1000 as tAgentDnd,
	COALESCE(uqagg2.LoggedIn,0)/1000 - COALESCE(uqagg2.OnQueue,0)/1000 as tAgentNotAvailable,
	COALESCE(uqagg2.OnQueue,0)/1000 as tAgentAcdLoggedIn,
	COALESCE(uqagg2.OnQueue,0)/1000 as tAgentAcdLoggedIn2,
	COALESCE(uqagg2.Busy,0)/1000 as tAgentStatusDnd,
	COALESCE(qagg.tAcw,0)/1000 as tAgentStatusAcw,
	COALESCE(uqagg2.LoggedIn,0) / 1000 / COALESCE(uqagg2.NoOfQueues,1) as tAgentLoggedInDiluted,
	0 as tStatusGroupFollowup,-- unused
	COALESCE(uqagg2.[Break],0)/1000 as tStatusGroupBreak,-- unused
	COALESCE(uqagg2.Training,0)/1000 as tStatusGroupTraining,-- unused
	0 as CustomValue1, -- not in Genesys Cloud
	0 as CustomValue2, -- not in Genesys Cloud
	0 as CustomValue3, -- not in Genesys Cloud
	COALESCE(qagg.maxAbandon,0)/1000 as CustomValue4, -- not in Genesys Cloud
	COALESCE(qagg.nVoicemail,0)/1000 as CustomValue5, -- not in Genesys Cloud
	COALESCE(qagg.nConsult,0)/1000 as CustomValue6 -- not in Genesys Cloud

	, 0 as [nAllInternToExternCalls]	--todo
	, 0 as [nAllInternToExternAcdCalls]		--todo
	, 0 as [tAllInternToExternCalls]		--todo
	, 0 as [tAllInternToExternAcdCalls]		--todo
	, 0 as [tAllTalkAcd]		--todo
	, 0 as [tAllHoldAcd]		--todo
	, 0 as [tAllAgentOnAcdCall]		--todo
	, 0 as [nAllInternToInternCalls]		--todo
	, 0 as [nAllExternToInternCalls]		--todo
	, 0 as [nAllExternToInternAcdCalls]		--todo
	, 0 as [tAllExternToInternCalls]		--todo
	, 0 as [tAllExternToInternAcdCalls]		--todo
	, 0 as [tAllAgentAvailable]		--todo
	, 0 as [tAllAgentInAcw]		--todo
	, 0 as [tAllAgentStatusDnd]		--todo
	, 0 as [tAllAgentLoggedIn]		--todo
	, 0 as [tAllAgentAcdLoggedIn2]		--todo
	, 0 as [tAllAgentAcdLoggedIn]		--todo


	, 0 as SiteId -- not in Genesys Cloud
	, 0 as SubSiteId -- not in Genesys Cloud
	, qagg.dIntervalStartUTC as I3TimeStampGMT -- not in Genesys Cloud
	, 0 as DimensionSet -- not in Genesys Cloud
	, 0 as SummDimensionSet -- not in Genesys Cloud
	, 0 as ConfigurationSet -- not in Genesys Cloud
	, COALESCE(rq.DurationMs,0)/1000 as nServiceLevel
	, 0 as cServiceLevels -- not in Genesys Cloud
	, 0 as SchemaMajorVersion -- not in Genesys Cloud
	, 0 as SchemaMinorVersion -- not in Genesys Cloud
FROM #TEMPTABLE_WRKGRP2 qagg

LEFT JOIN (
		select 
			uqagg.dIntervalStartUTC, 
			uqagg.queueId, 
			uqagg.userId,
			COUNT(queueId) as NoOfQueues,
		
			SUM(LoggedIn) as LoggedIn,
			SUM(Available) as Available,
			SUM(Away) as Away,
			SUM([Break]) as [Break],
			SUM(Busy) as Busy,
			SUM(Idle) as Idle,
			SUM(Meal) as Meal,
			SUM(Meeting) as Meeting,
			SUM(OnQueue) as OnQueue,
			SUM(Training) as Training

		FROM gen.gcQueueAggregates uqagg
		LEFT JOIN (
			select 
				dIntervalStartUTC,
				userId, 
				SUM(tOrganizationPresence) as LoggedIn,
				SUM(case when OrgPresenceNAme = 'Available' then tOrganizationPresence else 0 end) as Available,
				SUM(case when OrgPresenceNAme = 'Away' then tOrganizationPresence else 0 end) as Away,
				SUM(case when OrgPresenceNAme = 'Break' then tOrganizationPresence else 0 end) as [Break],
				SUM(case when OrgPresenceNAme = 'Busy' then tOrganizationPresence else 0 end) as Busy,
				SUM(case when OrgPresenceNAme = 'Idle' then tOrganizationPresence else 0 end) as Idle,
				SUM(case when OrgPresenceNAme = 'Meal' then tOrganizationPresence else 0 end) as Meal,
				SUM(case when OrgPresenceNAme = 'Meeting' then tOrganizationPresence else 0 end) as Meeting,
				SUM(case when OrgPresenceNAme = 'On Queue' then tOrganizationPresence else 0 end) as OnQueue,
				SUM(case when OrgPresenceNAme = 'Training' then tOrganizationPresence else 0 end) as Training
			FROM gen.gcUserAggregates
			GROUP BY userId, dIntervalStartUTC
		) usagg 
			on usagg.dIntervalStartUTC = uqagg.dIntervalStartUTC
		 AND usagg.userId = uqagg.UserId
		WHERE usagg.dIntervalStartUTC BETWEEN @procStartDate and @procEndDate
		 AND uqagg.UserId IS NOT NULL
		GROUP BY uqagg.dIntervalStartUTC, uqagg.queueId, uqagg.userId

	) uqagg2 ON uqagg2.dIntervalStartUTC = qagg.dIntervalStartUTC
	 AND qagg.queueId = uqagg2.queueId
	 AND qagg.userId = uqagg2.userId
	
LEFT JOIN gen.gcRoutingQueues rq ON rq.QueueId = qagg.queueId
--WHERE (qagg.Purpose = 'acd' )
;

--select * FROM [gen].IAgentQueueStats order by dIntervalStartUTC;



