CREATE OR ALTER PROCEDURE gen.sp_vcssSurveyScoringDetail
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
AS

delete from gen.vcssSurveyScoringDetail where [EventDate] between @procStartDate and @procEndDate;

insert into gen.vcssSurveyScoringDetail
SELECT 
	sess.SessionId as [SurveyId]
	,sess.FlowName as  [SurveyName]

	, null as [SurveyNote]
	, 1 as [SurveyType]
	, 1 as [SurveyPublished]
	, sess.ConversationId as [InteractionId]
	, CAST(rec.RecordingId AS UNIQUEIDENTIFIER) as [RecordingId]
	, rec.StartTime as [RecordingDate]
	, 10 as [RecordingDateOffset]
	, conv.ConversationStart as [EventDate]

	, sess.RemoteNameDisplayable AS [SurveyParticipant]
	, COALESCE(us.UserName, 'USER_NOT_FOUND') AS [ICUserId]
	, us.FirstName AS [LastName]
	, us.LastName AS [FirstName]

	, sess.FlowId as [SurveyFormId]
	, CASE WHEN tot.NoOfAnswers > =4 THEN 1 ELSE 0 END as [IsCompleted]
	, 0 as [MinAcceptableScore]
	, 0 as [MinScore]
	, CASE WHEN outc.Description like '%Rating%' then 5 else 0 END as MaxScore
	, CASE WHEN outc.Description like '%Rating%' then SUBSTRING(outc.Description, 4, 1) else 0 END as [Score]
	, COALESCE(SUBSTRING(outc.Description, 1, 2), 'N/A') as [QuestionId]
	, SUBSTRING(outc.Description, 2, 1) as [QuestionSequence]
	, SUBSTRING(outc.Description, 8, CHARINDEX(':', outc.Description) - 8) as [QuestionText]
	, 1 as [QuestionWeight]
	, SUBSTRING(outc.Description, 4, 1) as [NumericScore]
	, SUBSTRING(outc.Description, CHARINDEX(':', outc.Description)+1, LEN(outc.Description)) as [AnswerText]
	, outc.FlowOutcomeName as [FreeFormAnswerRecording]

FROM gen.gcConversationSessions sess
	LEFT JOIN gen.gcConversationFlowOutcomes coutc on coutc.SessionId = sess.SessionId
	LEFT JOIN gen.gcFlowOutcomes outc on outc.FlowOutcomeId = coutc.FlowOutcomeId
	LEFT JOIN gen.gcConversations conv on conv.ConversationId = sess.ConversationId
	LEFT JOIN gen.gcConversationRecordings rec on rec.ConversationId = conv.ConversationId
	LEFT JOIN (
		SELECT 
			DISTINCT ConversationId, UserId
		FROM gen.gcConversationParticipants 
		WHERE ConversationStartUTC between @procStartDate and @procEndDate
			and Purpose = 'Agent'
	) part on part.ConversationId = conv.ConversationId
	LEFT JOIN gen.gcUsers us on us.UserId = part.UserId
	LEFT JOIN (
		select 
			coutc.SessionId, 
			COUNT(coutc.SessionId) as NoOfAnswers,
			SUM(CASE WHEN outc.Description like '%Rating%' then SUBSTRING(outc.Description, 4, 1) else 0 END) as [Score]
		FROM gen.gcConversationFlowOutcomes coutc 
			LEFT JOIN gen.gcFlowOutcomes outc on outc.FlowOutcomeId = coutc.FlowOutcomeId
		WHERE coutc.FlowOutcomeStartTimestamp between @procStartDate and @procEndDate
		group by coutc.SessionId
	) tot on tot.SessionId = sess.SessionId
where 
	sess.StartTimeUTC  between @procStartDate and @procEndDate
	and sess.FlowName like 'IVR_Survey%'
	and not(part.UserId is null or part.UserId = '')
;


