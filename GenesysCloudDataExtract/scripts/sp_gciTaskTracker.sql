CREATE OR ALTER PROCEDURE gen.sp_gciTaskTracker 
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
AS

delete from gen.gciTaskTracker where StartTimeUTC BETWEEN @procStartDate and @procEndDate;

insert into gen.gciTaskTracker
select 
	ConversationId, 
	MAX(UserId) as UserId,
	SUBSTRING(REPLACE(MAX(AddressFrom), '.com', ''), CHARINDEX('@', MAX(AddressFrom))+1, LEN(MAX(AddressFrom))) as Category,
	SUBSTRING(REPLACE(MAX(AddressFrom), '.com', ''), 0, CHARINDEX('@', MAX(AddressFrom))) as Task,
	MIN(StartTimeUTC) as StartTimeUTC, 
	MAX(EndTimeUTC) as EndTimeUTC,
	SUM(tTalk) as tTalk,
	-- Held is not part of Talk, if agent takes a non-TaskTracker interaction during this time it will put the TaskTracker interaction on Hold
	SUM(tHeld) as tHeld,
	SUM(tAcw) as tAcw,
	DATEDIFF(ms, MIN(StartTimeUTC), MAX(EndTimeUTC)) as Duration
from gen.gcConversationSessions
where 1=1
	AND ConversationStartUTC BETWEEN @procStartDate and @procEndDate
	AND ConversationId in
		(
			SELECT ConversationId 
			FROM gen.gcConversationSessions
			where 
				StartTimeUTC between @procStartDate and @procEndDate
				and MediaType = 'Email' 
				and FlowName = 'TaskTracker'
		)
group by ConversationId
;
