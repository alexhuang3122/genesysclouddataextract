
CREATE OR ALTER PROCEDURE gen.sp_gciCleanData
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
AS

----------------------------------------------------------------------------

INSERT INTO gen.gcUsers (UserId, UserName, Name, FirstName, LastName) (
	SELECT 
		part.UserId, 
		MIN(part.ParticipantName) AS UserName, 
		MIN(part.ParticipantName) AS [Name], 
		substring(MIN(part.ParticipantName),1,PATINDEX('% %',MIN(part.ParticipantName))) as FirstName, 
		SUBSTRING(MIN(part.ParticipantName),patindex('% %',MIN(part.ParticipantName))+1,LEN(MIN(part.ParticipantName))) as LastName
	FROM gen.gcConversationParticipants part
		LEFT JOIN gen.gcUsers u ON u.UserId = part.UserId
	WHERE part.ConversationStartUTC BETWEEN @procStartDate AND @procEndDate
		AND part.Purpose = 'Agent' 
		AND ParticipantName IS NOT NULL 
		AND u.UserId IS NULL
	GROUP BY part.UserId
);

----------------------------------------------------------------------------

MERGE
INTO    gen.gcConversationSessions ca1
USING   (
	SELECT gen.gcConversationSessions.ConversationId, Purpose, MediaType, CallbackScheduledTimeUTC, StartTimeUTC, EndTimeUTC
	FROM gen.gcConversationSessions LEFT JOIN gen.gcConversationParticipants 
		ON gen.gcConversationSessions.ConversationStartUTC = gen.gcConversationParticipants.ConversationStartUTC
			AND gen.gcConversationSessions.ParticipantId = gen.gcConversationParticipants.ParticipantId
	WHERE gen.gcConversationSessions.ConversationStartUTC BETWEEN @procStartDate AND @procEndDate
		AND Purpose = 'Customer'
		AND MediaType = 'Callback'
) ca2
ON      (
		ca1.ConversationStartUTC BETWEEN @procStartDate AND @procEndDate
		AND ca1.MediaType = 'Voice'
		AND ca1.ConversationId = ca2.ConversationId
        AND ca1.CallbackScheduledTimeUTC IS NULL
		AND ca2.CallbackScheduledTimeUTC IS NOT NULL
        AND  ca1.StartTimeUTC BETWEEN ca2.StartTimeUTC AND ca2.EndTimeUTC
		AND ca1.StartTimeUTC BETWEEN DATEADD(mi, -30, ca2.CallbackScheduledTimeUTC) AND DATEADD(mi, 30, ca2.CallbackScheduledTimeUTC)
        )
WHEN MATCHED THEN
UPDATE SET 
	ca1.CallbackScheduledTimeUTC = ca2.CallbackScheduledTimeUTC
;
/*

DECLARE 
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
;
SET @procStartDate = '01-Apr-2021';
SET @procEndDate = '01-Jul-2021';

SELECT
	ca1.ConversationId, ca1.SessionId, ConversationStartUTC, ca1.StartTimeUTC, ca1.EndTimeUTC, ca1.MediaType,ca1.Remote,
	ca2.*
--	ca1.CallbackScheduledTimeUTC
FROM gen.gcConversationSessions ca1

LEFT JOIN (
	SELECT gen.gcConversationSessions.ConversationId, Purpose, MediaType, CallbackScheduledTimeUTC, StartTimeUTC, EndTimeUTC, Remote
	FROM gen.gcConversationSessions LEFT JOIN gen.gcConversationParticipants 
		ON gen.gcConversationSessions.ConversationStartUTC = gen.gcConversationParticipants.ConversationStartUTC
			AND gen.gcConversationSessions.ParticipantId = gen.gcConversationParticipants.ParticipantId
	WHERE gen.gcConversationSessions.ConversationStartUTC BETWEEN @procStartDate AND @procEndDate
	AND Purpose = 'Customer'
	AND MediaType = 'Callback'
	AND CallbackScheduledTimeUTC IS NOT NULL
) ca2
ON      (
		ca1.ConversationStartUTC BETWEEN @procStartDate AND @procEndDate
        AND ca1.ConversationId = ca2.ConversationId
        AND ca1.CallbackScheduledTimeUTC IS NULL
        AND ca1.StartTimeUTC BETWEEN ca2.StartTimeUTC AND ca2.EndTimeUTC
		AND ca1.StartTimeUTC BETWEEN DATEADD(mi, -30, ca2.CallbackScheduledTimeUTC) AND DATEADD(mi, 30, ca2.CallbackScheduledTimeUTC)
        )

WHERE ca1.ConversationStartUTC BETWEEN @procStartDate AND @procEndDate
AND ca1.MediaType = 'Voice'
AND ca2.ConversationId IS NOT NULL

--AND ca1.ConversationId = '3082134a-cc1f-44db-b326-ac868a9e363a'
--AND ca1.ConversationId = '3d9e6bbd-c3ca-4493-b73e-348141f38263'
ORDER BY
	ConversationStartUTC, ca1.StartTimeUTC
*/