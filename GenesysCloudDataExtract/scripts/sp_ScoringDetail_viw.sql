CREATE OR ALTER PROCEDURE gen.sp_ScoringDetail_viw 
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
AS

delete from gen.ScoringDetail_viw  where [RecordingDate] between @procStartDate and @procEndDate;

insert into gen.ScoringDetail_viw 
([RecordingId],[RecordingDate],[MediaType],[InteractionIdKey],[RecordingLength],[InitiationPolicyName],[Direction],[RecordedUserIndivId],[RecordedUserLastName],[RecordedUserFirstName],[RecordedUserMiddleName],[RecordedUserDisplayName],[RecordedUserICUserId],[WorkgroupQueue],[QuestionnaireId],[QuestionnaireName],[QuestionnaireNote],[QuestionnaireMinScore],[QuestionnaireMaxScore],[QDirectoryName],[QGroupId],[QGroupSequence],[QGroupName],[QGroupNote],[IsOptional],[QuestionId],[QuestionSequence],[QuestionText],[QuestionType],[QuestionPromptType],[QuestionWeight],[QuestionMin],[QuestionMax],[QuestionMinAcceptableScore],[QuestionCanMarkNA],[FormId],[ScoredUserLastName],[ScoredUserFirstName],[ScoredUserMiddleName],[ScoredUserICUserId],[ScoredUserIndivID],[ScoringUserLastName],[ScoringUserFirstName],[ScoringUserMiddleName],[ScoringUserICUserId],[ScoringUserIndivId],[CombinedMaxScore],[CombinedMinScore],[CombinedScore],[CombinedPercentileScore],[RankName],[NonCriticalMaxScore],[NonCriticalMinScore],[NonCriticalScore],[NonCriticalPercentileScore],[TotalNumCriticalQuestions],[NumAnsweredCriticalQstns],[NumPosAnsweredCriticalQstns],[CriticalScore],[IsFailed],[UsingForCalibration],[AnswerScore],[RawAnswer],[DisplayAnswer],[UserComments],[IsMarkedNA])




select

	CAST([RecordingId] as uniqueidentifier) as RecordingId
	, eval.ConversationStartUTC as [RecordingDate]
	, sess.MediaType as [MediaType]
	, eval.ConversationId as [InteractionIdKey]
	, datediff(ss, recsess.StartTimeUTC, recsess.EndTimeUTC) as [RecordingLength]
	, null as [InitiationPolicyName]
	, (case when LOWER(sess.Direction) ='inbound' then 1 when LOWER(sess.Direction) ='outbound' then 2  else 0 end) as [Direction] --1-inbound, 2-Outbound, 3-Intercom, 4-Intercom Outbound, 5-Intercom Inbound, 0-Unknown

	, scored.UserId as [RecordedUserIndivId]
	, scored.LastName as [RecordedUserLastName]
	, scored.FirstName as [RecordedUserFirstName]
	, null as [RecordedUserMiddleName]
	, scored.Name as [RecordedUserDisplayName]
	, scored.UserName as [RecordedUserICUserId]
	, q.QueueName [WorkgroupQueue]

	, eval.ConversationEvaluationId as [QuestionnaireId]
	, eval.EvaluationFormName as [QuestionnaireName]
	, eval.Comments as [QuestionnaireNote]
	
	, 0 as [QuestionnaireMinScore]
	, form.MaxFormScore as [QuestionnaireMaxScore]
	, null as [QDirectoryName]

	, ceq.EvaluationQuestionGroupId as [QGroupId]
	, COALESCE(eqg.QuestionGroupOrder, 0) [QGroupSequence]
	, eqg.EvaluationQuestionGroupName as [QGroupName]
	, null as [QGroupNote]
	, 0 as [IsOptional]
	, eq.EvaluationQuestionId as [QuestionId]
	, COALESCE(eq.QuestionOrder, 0) as [QuestionSequence]
	, eq.Text as [QuestionText]
	, 0 as [QuestionType] --eq.Type as [QuestionType]
	, 0 as [QuestionPromptType]
	, eqg.Weight as [QuestionWeight]
	, 0 as [QuestionMin]
	, eq.MaxQScore as [QuestionMax]
	, 0 as [QuestionMinAcceptableScore]
	, CASE WHEN LOWER(eq.NaEnabled) = 'true' then 1 else 0 END as [QuestionCanMarkNA]
	, form.EvaluationFormId as [FormId]

	, scored.LastName as ScoredUserLastName
	, scored.FirstName as ScoredUserFirstName
	, null as ScoredUserMiddleName
	, scored.UserName as ScoredUserICUserId
	, scored.UserId as ScoredUserIndivID

	, scorer.LastName as ScoringUserLastName
	, scorer.FirstName as ScoringUserFirstName
	, null as ScoringUserMiddleName
	, scorer.UserName as ScoringUserICUserId
	, scorer.UserId as ScoringUserIndivId
	
	, eval.MaxTotalScore as [CombinedMaxScore]
	, 0 as [CombinedMinScore]
	, eval.TotalScore as [CombinedScore]
	, eval.TotalScorePct as [CombinedPercentileScore]
	, 0 as [RankName]
	, eval.MaxTotalNonCriticalScore as [NonCriticalMaxScore]
	, 0 as [NonCriticalMinScore]
	, eval.TotalNonCriticalScore as [NonCriticalScore]
	, eval.TotalNonCriticalScorePct as [NonCriticalPercentileScore]
	, COALESCE(numcritq.TotalNumCriticalQuestions, 0) as [TotalNumCriticalQuestions]
	, COALESCE(numcritq.NumAnsweredCriticalQstns, 0) as [NumAnsweredCriticalQstns]
	, COALESCE(numcritq.NumPosAnsweredCriticalQstns, 0) as [NumPosAnsweredCriticalQstns]
	, COALESCE(eval.TotalCriticalScore, 0) as [CriticalScore]
	
	, (case when LOWER(eval.AnyFailedKillQuestions) = 'true' then 1 else 0 end) as IsFailed
	, CASE WHEN CalibrationId is not null THEN 1 ELSE 0 END as [UsingForCalibration]

	, ea.Value [AnswerScore]
	, ea.Text as [RawAnswer]
	, ea.Text as [DisplayAnswer]
	, eval.Comments as [UserComments]
	, CASE WHEN LOWER(ceq.MarkedNA) = 'true' then 1 else 0 END as [IsMarkedNA]

from gen.gcConversationEvaluations eval
left join gen.gcConversationSessions sess on eval.ConversationId = sess.ConversationId
	and eval.UserId = sess.UserId 
	and (
		(sess.Direction = 'Outbound' and sess.nOutboundConnected > 0)
		or (sess.Direction <> 'Outbound' and tHandle > 0)
	)
left join gen.gcConversationRecordings rec on rec.ConversationId = eval.ConversationId
left join gen.gcConversationSessions recsess on recsess.SessionId = rec.SessionId
left join gen.gcUsers scored on scored.UserId = eval.UserId
left join gen.gcUsers scorer on scorer.UserId = eval.EvaluatorId
left join gen.gcRoutingQueues q on q.QueueId = sess.FirstQueueId
left join gen.gcEvaluationForms form on form.EvaluationFormId = eval.EvaluationFormId
left join gen.gcConversationEvalQScores ceq on ceq.ConversationEvaluationId = eval.ConversationEvaluationId
left join gen.gcEvaluationQuestionGroups eqg on eqg.EvaluationQuestionGroupId = ceq.EvaluationQuestionGroupId
left join gen.gcEvaluationQuestions eq on eq.EvaluationQuestionId = ceq.EvaluationQuestionId
left join gen.gcEvaluationAnswerOptions ea on ea.EvaluationAnswerOptionId = ceq.AnswerId
left join (
	select 
		ce.ConversationEvaluationId, 
		COUNT(ce.ConversationEvaluationId) as TotalQuestions,
		SUM(case when LOWER(q.IsCritical)='true' then 1 else 0 end) as TotalNumCriticalQuestions,
		SUM(case when LOWER(q.IsCritical)='true' and LOWER(ceq.MarkedNA) <> 'true' then 1 else 0 end) as NumAnsweredCriticalQstns,
		SUM(case when LOWER(q.IsCritical)='true' and LOWER(ceq.MarkedNA) <> 'true' and ceq.Score>0 then 1 else 0 end) as NumPosAnsweredCriticalQstns
		--ce.*, ceq.*, q.*
	from gen.gcConversationEvaluations ce
	left join gen.gcConversationEvalQScores ceq on ceq.ConversationEvaluationId = ce.ConversationEvaluationId
	left join gen.gcEvaluationQuestions q on q.EvaluationQuestionId = ceq.EvaluationQuestionId
	--where ce.ConversationStartUTC between @procStartDate and @procEndDate
	group by ce.ConversationEvaluationId
) numcritq on numcritq.ConversationEvaluationId = eval.ConversationEvaluationId
where sess.StartTimeUTC between @procStartDate and @procEndDate
	and recsess.nConnected > 0
	and eqg.EvaluationQuestionGroupId IS NOT NULL
	and form.EvaluationFormId IS NOT NULL
;



;




