CREATE OR ALTER PROCEDURE gen.sp_gciOutboundAttempts
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
AS

DELETE FROM gen.gciOutboundAttempts where ConversationStartUTC between @procStartDate and @procEndDate;


INSERT INTO gen.gciOutboundAttempts

SELECT 
	camps.CampaignId
	, camps.CampaignName
	, camps.DialingMode
	, sesspart.OutboundContactListId
	, lists.ContactListName
	, cont.[inin-outbound-id]
	, sesspart.ConversationId
	, sesspart.DNIS
	, sesspart.CallbackScheduledTimeUTC
	, sesspart.ConversationStartTime
	, sesspart.ConversationEndTime
	, sesspart.QueueId
	, q.QueueName
	, sesspart.QueueStartTime
	, sesspart.QueueEndTime
	, sesspart.UserId
	, users.UserName
	, sesspart.ANI
	, sesspart.CustomerStartTime
	, sesspart.CustomerEndTime
	, sesspart.AgentPreviewStartTime
	, sesspart.AgentCallStartTime
	, sesspart.AgentCallEndTime
	, sesspart.LastWrapupCode
	, wraps.WrapupCodeName
	, CASE WHEN cumaps.WrapupCodeId IS NOT NULL THEN 1 ELSE 0 END as Contact_UnCallable
	, CASE WHEN numaps.WrapupCodeId IS NOT NULL THEN 1 ELSE 0 END as Number_UnCallable
	, CASE WHEN rpcmaps.WrapupCodeId IS NOT NULL THEN 1 ELSE 0 END as Right_Party_Contact
	, CASE WHEN wraps.WrapupCodeName LIKE 'OB-SUCCESS%' THEN 1 ELSE 0 END as Success
	, sesspart.DispositionName

	, sesspart.nAbandon/1000 AS nAbandon
	, sesspart.tAcd/1000 AS tAcd
	, sesspart.tPreview/1000 AS tPreview
	, sesspart.tTalk/1000 AS tTalk
	, sesspart.tHeld/1000 AS tHeld
	, sesspart.tAcw/1000 AS tAcw
	, sesspart.tDialing/1000 AS tDialing
	, sesspart.tAlert/1000 AS tAlert
	, sesspart.tNotResponding/1000 AS tNotResponding
	--, sesspart.tContacting
	, sesspart.nOutboundAbandoned
	, sesspart.nOutboundAttempted
	, sesspart.nOutboundConnected
	, sesspart.nError
	--, cont.*
	
FROM (
		SELECT 
			sess.ConversationId, OutboundCampaignId, OutboundContactListId, OutboundContactId, sess.CallbackScheduledTimeUTC
			, MAX(sess.DNIS) as DNIS
			, MAX(sess.ANI) as ANI
			, MAX(sess.ConversationStartUTC) as ConversationStartUTC
			--, MAX(sess.CallbackScheduledTimeUTC) as CallbackScheduledTimeUTC
			, MAX(sess.UserId) as UserId
			, MAX(sess.FirstQueueId) as QueueId
			, MAX(sess.LastWrapupCode) as LastWrapupCode
			, MIN(StartTimeUTC) as ConversationStartTime
			, MAX(EndTimeUTC) as ConversationEndTime
			, MIN(CASE WHEN parts.Purpose = 'Acd' THEN StartTimeUTC ELSE NULL END) as QueueStartTime
			, MAX(CASE WHEN parts.Purpose = 'Acd' THEN EndTimeUTC ELSE NULL END) as QueueEndTime
			, MIN(CASE WHEN parts.Purpose = 'Customer' AND sess.MediaType='Voice' THEN StartTimeUTC ELSE NULL END) as CustomerStartTime
			, MAX(CASE WHEN parts.Purpose = 'Customer' AND sess.MediaType='Voice' THEN EndTimeUTC ELSE NULL END) as CustomerEndTime
			, MIN(CASE WHEN parts.Purpose = 'Agent' AND sess.MediaType='Callback' THEN StartTimeUTC ELSE NULL END) as AgentPreviewStartTime
			, MIN(CASE WHEN parts.Purpose = 'Agent' AND sess.MediaType='Voice' THEN StartTimeUTC ELSE NULL END) as AgentCallStartTime
			, MAX(CASE WHEN parts.Purpose = 'Agent' AND sess.MediaType='Voice' THEN EndTimeUTC ELSE NULL END) as AgentCallEndTime
			, SUM(CASE WHEN parts.Purpose = 'Acd' THEN tAcd ELSE 0 END) as tAcd
			, SUM(CASE WHEN parts.Purpose = 'Acd' THEN nAbandon ELSE 0 END) as nAbandon
			, SUM(CASE WHEN parts.Purpose = 'Acd' THEN tAbandon ELSE 0 END) as tAbandon
			, SUM(CASE WHEN parts.Purpose = 'Agent' AND sess.MediaType='Callback' THEN tTalk ELSE 0 END) as tPreview
			, SUM(CASE WHEN parts.Purpose = 'Agent' AND sess.MediaType='Voice' THEN tTalk ELSE 0 END) as tTalk
			, SUM(CASE WHEN parts.Purpose = 'Agent' AND sess.MediaType='Voice' THEN tHeld ELSE 0 END) as tHeld
			, SUM(CASE WHEN parts.Purpose = 'Agent' AND sess.MediaType='Voice' THEN tAcw ELSE 0 END) as tAcw
			, SUM(CASE WHEN parts.Purpose = 'Agent' THEN tDialing ELSE 0 END) as tDialing
			, SUM(CASE WHEN parts.Purpose = 'Agent' THEN tAlert ELSE 0 END) as tAlert
			, SUM(CASE WHEN parts.Purpose = 'Agent' THEN tNotResponding ELSE 0 END) as tNotResponding
			, SUM(sess.nError) AS nError
			--, SUM(CASE WHEN parts.Purpose = 'Agent' AND sess.MediaType='Voice' THEN tContacting ELSE 0 END) as tContacting
			, SUM(sess.nOutboundAttempted) as nOutboundAbandoned
			, SUM(sess.nOutboundAttempted) as nOutboundAttempted
			, SUM(sess.nOutboundAttempted) as nOutboundConnected
			, MAX(sess.DispositionName) AS DispositionName
		FROM gen.gcConversationSessions sess 
			LEFT JOIN gen.gcConversationParticipants parts ON parts.ParticipantId = sess.ParticipantId
		WHERE sess.ConversationStartUTC BETWEEN @procStartDate AND @procEndDate
			AND sess.OutboundCampaignId IS NOT NULL
			AND sess.OutboundContactListId IS NOT NULL
			AND sess.OutboundContactId IS NOT NULL
		GROUP BY sess.ConversationId, sess.OutboundCampaignId, sess.OutboundContactListId, sess.OutboundContactId, sess.CallbackScheduledTimeUTC
	) sesspart 
	LEFT JOIN gen.gcOutboundCampaigns camps ON camps.CampaignId = sesspart.OutboundCampaignId
	LEFT JOIN gen.gcOutboundContactLists lists ON lists.ContactListId = sesspart.OutboundContactListId
	LEFT JOIN gen.gcOutboundContacts cont
		ON sesspart.OutboundContactListId = cont.ContactListId
		AND sesspart.OutboundContactId = cont.[inin-outbound-id]
	LEFT JOIN gen.gcWrapupCodes wraps ON wraps.WrapupCodeId = sesspart.LastWrapUpCode
	LEFT JOIN gen.gcOutboundMappings rpcmaps ON wraps.WrapupCodeId = rpcmaps.WrapupCodeId AND rpcmaps.OutboundMappingName = 'Right_Party_Contact'
	LEFT JOIN gen.gcOutboundMappings cumaps ON wraps.WrapupCodeId = cumaps.WrapupCodeId AND cumaps.OutboundMappingName = 'Contact_UnCallable'
	LEFT JOIN gen.gcOutboundMappings numaps ON wraps.WrapupCodeId = numaps.WrapupCodeId AND numaps.OutboundMappingName = 'Number_UnCallable'
	LEFT JOIN gen.gcUsers users ON users.UserId = sesspart.UserId
	LEFT JOIN gen.gcRoutingQueues q ON q.QueueId = sesspart.QueueId
--WHERE 1=1
	--AND cont.ContactListId = '07ddd0c2-8339-4998-a61d-df5859083363'
ORDER BY sesspart.ConversationStartUTC
;




