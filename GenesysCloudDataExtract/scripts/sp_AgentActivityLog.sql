CREATE OR ALTER PROCEDURE gen.sp_AgentActivityLog 
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
AS

delete from gen.AgentActivityLog where [StatusDateTimeGMT] between @procStartDate and @procEndDate;

--TRUNCATE TABLE gen.AgentActivityLog;

INSERT INTO gen.AgentActivityLog 
([UserId],[StatusDateTime],[StatusDateTimeGMT],[ChangedStatus],[ChangedStatusGroup],[ChangedLoggedIn],[ChangedAcdLoggedIn],[StatusKey],[StatusGroup],[LoggedIn],[AcdLoggedIn],[StatusDnd],[StatusAcw],[EndDateTime],[EndDateTimeGMT],[StateDuration],[SeqNo],[I3TimeStampGMT],[SiteId],[SubSiteId])
SELECT
	[UserId],[StatusDateTime],[StatusDateTimeGMT],[ChangedStatus],[ChangedStatusGroup],[ChangedLoggedIn],[ChangedAcdLoggedIn],[StatusKey],[StatusGroup],[LoggedIn],[AcdLoggedIn],[StatusDnd],[StatusAcw],[EndDateTime],[EndDateTimeGMT],[StateDuration],[SeqNo],[I3TimeStampGMT],[SiteId],[SubSiteId]
FROM (
SELECT

	users.Email as UserId
	, DATEADD(hh, 10, stat.StartTimeUTC) as [StatusDateTime]
	, stat.StartTimeUTC as [StatusDateTimeGMT]
	, 0 as [ChangedStatus]
	, 0 as [ChangedStatusGroup]
	, 0 as [ChangedLoggedIn]
	, 0 as [ChangedAcdLoggedIn]
	, op.OrgPresenceName as [StatusKey]
	, op.SystemPresence as [StatusGroup]
	, 1 as [LoggedIn]
	, case when stat.SystemPresence = 'OnQueue' then 1 else 0 end as [AcdLoggedIn]
	, 0 as [StatusDnd]
	, 0 as [StatusAcw]
	, DATEADD(hh, 10, stat.EndTimeUTC) as [EndDateTime]
	, stat.EndTimeUTC as [EndDateTimeGMT]
	, DATEDIFF(ss, stat.STARTTimeUTC, stat.EndTimeUTC) as [StateDuration]
	, 0 as [SeqNo]
	, stat.StartTimeUTC as [I3TimeStampGMT]
	, 0 as [SiteId]
	, 0 as [SubSiteId]

FROM [gen].[gcUserStatusDetails] stat
	left join gen.gcUsers users on stat.UserId = users.UserId
	left join gen.gcOrgPresences op on op.OrgPresenceId = stat.OrganizationPresenceId

WHERE stat.StartTimeUTC between @procStartDate and @procEndDate
	and stat.SystemPresence not in ('Offline')
	and stat.EndTimeUTC is not null
	and users.Email is not null

UNION

SELECT
	users.Email as UserId
      , DATEADD(hh, 10, rout.StartTimeUTC) as [StatusDateTime]
      , rout.StartTimeUTC as [StatusDateTimeGMT]
      , 0 as [ChangedStatus]
      , 0 as [ChangedStatusGroup]
      , 0 as [ChangedLoggedIn]
      , 0 as [ChangedAcdLoggedIn]
      , rout.RoutingStatus as [StatusKey]
      , rout.RoutingStatus as [StatusGroup]
      , 1 as [LoggedIn]
      , case when rout.RoutingStatus = 'OnQueue' then 1 else 0 end as [AcdLoggedIn]
      , 0 as [StatusDnd]
      , 0 as [StatusAcw]
      , DATEADD(hh, 10, rout.EndTimeUTC) as [EndDateTime]
      , rout.EndTimeUTC as [EndDateTimeGMT]
      , DATEDIFF(ss, rout.STARTTimeUTC, rout.EndTimeUTC) as [StateDuration]
      , 1 as [SeqNo]
      , rout.StartTimeUTC as [I3TimeStampGMT]
      , 0 as [SiteId]
      , 0 as [SubSiteId]
FROM [gen].gcUserRoutingStatus rout
	left join gen.gcUsers users on rout.UserId = users.UserId


WHERE rout.StartTimeUTC between @procStartDate and @procEndDate
	and rout.EndTimeUTC is not null
	and users.Email is not null

) AgentState
--order by UserId, StatusDateTime
;

-- SELECT * FROM [SERVAU].[gen].[gcUserStatusDetails];



