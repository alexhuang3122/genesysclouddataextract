CREATE OR ALTER PROCEDURE gen.[sp_InteractionSummary] 
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
AS

delete from gen.[InteractionSummary] where [StartDateTimeUTC] between @procStartDate and @procEndDate;

--TRUNCATE TABLE gen.[InteractionSummary];

DECLARE @TimeOffset INT = 10;

INSERT INTO gen.[InteractionSummary] 
([InteractionIdKey],[SiteID],[SeqNo],[InteractionID],[StartDateTimeUTC],[StartDTOffset],[Direction],[ConnectionType],[MediaType],[RemoteID],[DNIS_LocalID],[tDialing],[tIVRWait],[tQueueWait],[tAlert],[tConnected],[tHeld],[tSuspend],[tConference],[tExternal],[tACW],[nIVR],[nQueueWait],[nTalk],[nConference],[nHeld],[nTransfer],[nExternal],[Disposition],[DispositionCode],[WrapUpCode],[AccountCode],[IsRecorded],[IsSurveyed],[MediaServerID],[IndivID],[OrgID],[LineId],[LastStationId],[LastLocalUserId],[LastAssignedWorkgroupID],[LastLocalNumber],[LastLocalName],[RemoteICUserID],[RemoteNumberCountry],[RemoteNumberLoComp1],[RemoteNumberLoComp2],[RemoteNumberFmt],[RemoteNumberCallId],[RemoteName],[InitiatedDateTimeUTC],[ConnectedDateTimeUTC],[TerminatedDateTimeUTC],[LineDuration],[CallEventLog],[PurposeCode],[CallNote],[nSecuredIVR],[tSecuredIVR],[FirstAssignedAcdSkillSet],[nPark],[tPark])

SELECT
	sess.SessionId as [InteractionIdKey]
	, 1 as [SiteID]
	, ROW_NUMBER() OVER(PARTITION BY sess.ConversationID ORDER BY sess.StartTimeUTC) as [SeqNo]
	, sess.ConversationID as [InteractionID]
	, sess.StartTimeUTC  as [StartDateTimeUTC]
	, @TimeOffset*60*60 as [StartDTOffset]
	
	, (case when LOWER(sess.Direction) ='inbound' then 1 when LOWER(sess.Direction) ='outbound' then 2  else 0 end) as [Direction] --1-inbound, 2-Outbound, 3-Intercom, 4-Intercom Outbound, 5-Intercom Inbound, 0-Unknown
	, 0 as [ConnectionType] --todo
	, case sess.MediaType 
		when 'Voice' then 0
		when 'Chat' then 1
		when 'Email' then 5
		when 'Callback' then 6
		else 255
	end as [MediaType] --Unknown(255), calls(0), chat(1), SMS(2), GenericObject(4), Email(5), Callback(6)), InstantQuestion(7),WebCollabration(8), MonitorObject(11), Fax(21), WorkItem(22).

	, COALESCE(REPLACE(REPLACE(sess.Ani,'tel:',''), '+serco-au-cc-ndia-prod.orgspan.com@localhost', ''), sess.Provider) as [RemoteID]
	, RTRIM(LTRIM(REPLACE(REPLACE(sess.Dnis,'tel:',''), '+serco-au-cc-ndia-prod.orgspan.com@localhost', ''))) as [DNIS_LocalID]
	, COALESCE(sess.tDialing, 0) as [tDialing]
	, COALESCE(sess.tIvr/1000, 0) as [tIVRWait]
	, COALESCE(sess.tAcd/1000, 0) as [tQueueWait]
	, COALESCE(sess.tAlert/1000, 0) as [tAlert]
	, COALESCE(sess.tAnswered/1000, 0) as [tConnected]
	, COALESCE(sess.tHeld/1000, 0) as [tHeld]
	, COALESCE(sess.TimeoutSeconds/1000, 0) as [tSuspend]
	, 0 as [tConference]	--todo
	, 0 as [tExternal]		--todo
	, COALESCE(sess.tIvr/1000, 0) as [tACW]
	, COALESCE(sess.nIvr, 0) as [nIVR]
	, COALESCE(sess.nAcd, 0) as [nQueueWait]
	, COALESCE(sess.nTalk, 0) as [nTalk]
	, 0 as [nConference] --todo
	, COALESCE(sess.nHeld, 0) as [nHeld]
	, COALESCE(sess.nTransferred, 0) as [nTransfer]
	, 0 as [nExternal] --todo
	, 0 as [Disposition]
	, 0 as [DispositionCode]	--todo
	, sess.LastWrapUpCode as [WrapUpCode]
	, null as [AccountCode] --todo
	, case when sess.Recording is not null then 1 else 0 end as [IsRecorded]
	, 0 as [IsSurveyed]	--todo
	, null as [MediaServerID]	--todo
	
	, sess.UserId as [IndivID]
	, null as [OrgID]
	, null as [LineId]
	, RTRIM(LTRIM(REPLACE(sess.EdgeId,'tel:',''))) as [LastStationId]
	, u.UserName as [LastLocalUserId]
	, q.QueueName as [LastAssignedWorkgroupID]
	, RTRIM(LTRIM(REPLACE(sess.ani,'tel:',''))) as [LastLocalNumber]
	, COALESCE(u.UserName, flow.FlowName) as [LastLocalName]
	
	, CASE WHEN LEN(sess.RemoteNameDisplayable)=36 THEN RemoteNameDisplayable END  as [RemoteICUserID]
	, 0 as [RemoteNumberCountry]
	, LEFT(RTRIM(LTRIM(REPLACE(sess.Dnis,'tel:',''))),4) as [RemoteNumberLoComp1] --, LEFT(TRIM(REPLACE(sess.Dnis,'tel:','')),10) as [RemoteNumberLoComp1]
	, '' as [RemoteNumberLoComp2] --, RIGHT(LEFT(sess.Dnis,20),10) as [RemoteNumberLoComp2]
	, LEFT(RTRIM(LTRIM(REPLACE(REPLACE(sess.SessionDnis,'tel:',''),'+61','0'))),50) as [RemoteNumberFmt]
	, LEFT(RTRIM(LTRIM(REPLACE(sess.SessionDnis,'tel:',''))),50) as [RemoteNumberCallId]
	
	, LEFT(sess.Remote, 50) as [RemoteName]
	, sess.ConversationStartUTC as [InitiatedDateTimeUTC]
	, DATEADD(ss, COALESCE(sess.tDialing,0)/1000, sess.StartTimeUTC) as [ConnectedDateTimeUTC]
	, sess.EndTimeUTC as [TerminatedDateTimeUTC]
	, CASE WHEN DATEDIFF(ss,sess.StartTimeUTC,sess.EndTimeUTC) > (5*86400) THEN DATEDIFF(ss,sess.StartTimeUTC,sess.EndTimeUTC) * 100 ELSE datediff(MILLISECOND,sess.StartTimeUTC,sess.EndTimeUTC) END as [LineDuration] 
	, '' as [CallEventLog]	--todo
	, 0 as [PurposeCode]	--todo
	, null as [CallNote]
	, 0 as [nSecuredIVR]
	, 0 as [tSecuredIVR]
	, q.QueueName as [FirstAssignedAcdSkillSet]	
	, 0 as [nPark]	--todo
	, 0 as [tPark]	--todo
	
FROM gen.gcConversationSessions sess
	LEFT JOIN gen.gcConversations conv on conv.ConversationId = sess.ConversationId
	LEFT JOIN gen.gcConversationRecordings rec on rec.SessionId = sess.SessionId
	LEFT JOIN gen.gcUsers u on u.UserId = sess.UserId
	LEFT JOIN gen.gcFlows flow
		ON flow.FlowId = sess.FlowId
	LEFT JOIN gen.gcRoutingQueues q on q.QueueId = sess.FirstQueueId

WHERE sess.StartTimeUTC between @procStartDate and @procEndDate
	and sess.EndTimeUTC is not null
	and sess.SessionId is not null
	--AND sess.ConversationId = '57c8f87c-fb4f-4b4b-ae4c-d013d6fe8b8a'

--ORDER BY sess.StartTimeUTC
;

-- SELECT * FROM gen.gcConversationSessions



