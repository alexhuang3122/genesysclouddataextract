---------------------------------------------------
-- Genesys Cloud Data Extract Initialization
-- Versaion:		0.03
-- Modified Date:	09/04/2021

-- Note: Run this script to initialize a database for the data extract job. Note that it will drop and recreate all required tables.
---------------------------------------------------

DROP PROCEDURE IF EXISTS	gen.sp_TaskTracker;
DROP PROCEDURE IF EXISTS	gen.sp_IVSurveyResults;
DROP PROCEDURE IF EXISTS	gen.sp_TaskTracker;
DROP PROCEDURE IF EXISTS	 gen.sp_IVRSurveyResults;
DROP TABLE IF EXISTS		gen.IVRSurveyResults;
DROP TABLE IF EXISTS		gen.TaskTracker;

DROP TABLE IF EXISTS gen.gcExtractDBVersion;
CREATE TABLE gen.gcExtractDBVersion(
	SCHEMAVERSION					VARCHAR(10),
	LASTRUN							DATETIME,
	LASTRUNSTATUS					VARCHAR(10)
)

DROP TABLE IF EXISTS gen.dIntervals;
CREATE TABLE gen.dIntervals(
	dIntervalUTC					DATETIME,
	dIntervalEndUTC					DATETIME,
	dInterval						DATETIME,
	dIntervalEnd					DATETIME,
	dIntervalDayUTC					DATETIME,
	dIntervalDay					DATETIME,
	PRIMARY KEY (dIntervalUTC)
);

--------------------------------------------------------------------------------------------------------------------------
-- PureCloud Extract Tables - Dimensions and Configuration
--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcUsers;
CREATE TABLE gen.gcUsers(
	UserId							VARCHAR(50),
	UserName						VARCHAR(255),
	Name							VARCHAR(255),
	FirstName						VARCHAR(255),
	LastName						VARCHAR(255),
	DivisionId						VARCHAR(50),
	DivisionName					VARCHAR(50),
	GeolocationRegion				VARCHAR(50),
	GeolocationName					VARCHAR(50),
	Title							VARCHAR(50),
	Email							VARCHAR(50),
	Department						VARCHAR(50),
	LanguagePreference				VARCHAR(50),
	TeamName						VARCHAR(50),
	DateHire						DATETIME, 
	EmployeeId						VARCHAR(50), 
	EmployeeType					VARCHAR(50), 
	OfficialName					VARCHAR(255),
	[State]							VARCHAR(50), 
	ManagerId						VARCHAR(50), 
	PRIMARY KEY (UserId)
)

DROP TABLE IF EXISTS gen.gcUserSkills;
CREATE TABLE gen.gcUserSkills(
	UserId							VARCHAR(50),
	UserName						VARCHAR(255),
	SkillId							VARCHAR(50),
	SkillName						VARCHAR(255),
	Proficiency						NUMERIC(10,2),
	State							VARCHAR(50)
)

--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcRoutingQueues;
CREATE TABLE gen.gcRoutingQueues(
	QueueId							VARCHAR(50),
	QueueName						VARCHAR(50),
	DivisionId						VARCHAR(50),
	DivisionName					VARCHAR(50),
	SkillEvaluationMethod			VARCHAR(50),
	DurationMs						INTEGER,
	Percentage						NUMERIC(10,2),
	PRIMARY KEY (QueueId)
)

DROP TABLE IF EXISTS gen.gcQueueMembers;
CREATE TABLE gen.gcQueueMembers(
	QueueId							VARCHAR(50),
	QueueName						VARCHAR(50),
	UserId							VARCHAR(50),
	UserName						VARCHAR(50),
	MemberId						VARCHAR(50),
	MemberName						VARCHAR(255),
	Joined							VARCHAR(10),
	MemberBy						VARCHAR(255),
	RingNumber						INTEGER,
	RoutingStatus					VARCHAR(20),
)

--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcRoutingSkills;
CREATE TABLE gen.gcRoutingSkills(
	SkillId							VARCHAR(50),
	SkillName						VARCHAR(50),
	State							VARCHAR(50),
	PRIMARY KEY (SkillId)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcWrapupCodes;
CREATE TABLE gen.gcWrapupCodes(
	WrapupCodeId					VARCHAR(50),
	WrapupCodeName					VARCHAR(255),
	CreatedBy						VARCHAR(255),
	DateCreated						DATETIME,
	ModifiedBy						VARCHAR(255),
	DateModified					DATETIME,
	PRIMARY KEY (WrapupCodeId)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcEvaluationForms;
CREATE TABLE gen.gcEvaluationForms(
	EvaluationFormId				VARCHAR(50),
	EvaluationFormName				VARCHAR(255),
	ContextId						VARCHAR(50),
	LatestVersion					VARCHAR(10),
	ModifiedDateUTC					DATETIME,
	Published						VARCHAR(10),
	MaxFormScore					INTEGER,
	MaxFormCriticalScore			INTEGER,
	PRIMARY KEY (EvaluationFormId)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcEvaluationQuestionGroups;
CREATE TABLE gen.gcEvaluationQuestionGroups(
	EvaluationFormId				VARCHAR(50),
	ContextId						VARCHAR(50),
	EvaluationQuestionGroupId		VARCHAR(50),
	EvaluationQuestionGroupName		VARCHAR(255),
	DefaultAnswersToHighest			VARCHAR(10),
	DefaultAnswersToNA				VARCHAR(10),
	ManualWeight					VARCHAR(10),
	NaEnabled						VARCHAR(10),
	Type							VARCHAR(50),
	VisibilityCondition				VARCHAR(255),
	Weight							NUMERIC(10,2),
	MaxQGScore						INTEGER,
	MaxQGCriticalScore				INTEGER,
	QuestionGroupOrder				INTEGER,
	PRIMARY KEY (EvaluationFormId, EvaluationQuestionGroupId)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcEvaluationQuestions;
CREATE TABLE gen.gcEvaluationQuestions(
	EvaluationFormId				VARCHAR(50),
	ContextId						VARCHAR(50),
	EvaluationQuestionGroupId		VARCHAR(50),
	EvaluationQuestionId			VARCHAR(50),
	Text							VARCHAR(512),
	CommentsRequired				VARCHAR(10),
	HelpText						VARCHAR(512),
	IsCritical						VARCHAR(10),
	IsKill							VARCHAR(10),
	NaEnabled						VARCHAR(10),
	Type							VARCHAR(50),
	VisibilityCondition				VARCHAR(255),
	MaxQScore						INTEGER,
	MaxQCriticalScore				INTEGER,
	QuestionOrder					INTEGER,
	PRIMARY KEY (EvaluationFormId, EvaluationQuestionGroupId, EvaluationQuestionId)
)
CREATE NONCLUSTERED INDEX idx_gcEvaluationQuestions ON gen.gcEvaluationQuestions (EvaluationQuestionId)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcEvaluationAnswerOptions;
CREATE TABLE gen.gcEvaluationAnswerOptions(
	EvaluationFormId				VARCHAR(50),
	ContextId						VARCHAR(50),
	EvaluationQuestionGroupId		VARCHAR(50),
	EvaluationQuestionId			VARCHAR(50),
	EvaluationAnswerOptionId		VARCHAR(50),
	Text							VARCHAR(512),
	Value							INTEGER,
	AnswerOrder						INTEGER,
	PRIMARY KEY (EvaluationFormId, EvaluationQuestionGroupId, EvaluationQuestionId, EvaluationAnswerOptionId)
)
CREATE NONCLUSTERED INDEX idx_gcEvaluationAnswerOptions ON gen.gcEvaluationAnswerOptions (EvaluationAnswerOptionId)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcOutboundCampaigns;
CREATE TABLE gen.gcOutboundCampaigns (
	CampaignId						VARCHAR(50),
	CampaignName					VARCHAR(255),
	AbandonRate						NUMERIC(10,2),
	AlwaysRunning					VARCHAR(10),
	CallableTimeSetId				VARCHAR(50),
	CallableTimeSetName				VARCHAR(255),
	CallAnalysisLanguage			VARCHAR(50),
	CallAnalysisResponseSet			VARCHAR(255),
	CallerAddress					VARCHAR(255),
	CallerName						VARCHAR(255),
	CampaignStatus					VARCHAR(50),
	ContactListId					VARCHAR(50),
	ContactListName					VARCHAR(255),
	ContactSortFieldName			VARCHAR(255),
	ContactSortDirection			VARCHAR(50),
	ContactSortNumeric				VARCHAR(10),
	DateCreatedUTC					DATETIME,
	DateModifiedUTC					DATETIME,
	DialingMode						VARCHAR(50),
	DivisionId						VARCHAR(50),
	DivisionName					VARCHAR(255),
	DncLists						VARCHAR(512),
	EdgeGroupId						VARCHAR(50),
	EdgeGroupName					VARCHAR(255),
	NoAnswerTimeout					INT,
	OutboundLineCount				INT,
	PreviewTimeOutSeconds			INT,
	Priority						INT,
	QueueId							VARCHAR(50),
	QueueName						VARCHAR(255),
	SiteId							VARCHAR(50),
	SiteName						VARCHAR(255),
	PRIMARY KEY (CampaignId)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcOutboundContactLists;
CREATE TABLE gen.gcOutboundContactLists (
	ContactListId					VARCHAR(50),
	ContactListName					VARCHAR(50),
	AttemptLimitsId					VARCHAR(50),
	AttemptLimitsName				VARCHAR(255),
	AutomaticTimeZoneMapping		VARCHAR(50),
	ColumnNames						VARCHAR(1024),
	DateCreated						DATETIME,
	DateModified					DATETIME,
	DivisionId						VARCHAR(50),
	DivisionName					VARCHAR(255),
	ImportCompletedRecords			INT,
	ImportFailureReason				VARCHAR(1024),
	ImportPercentComplete			INT,
	ImportState						VARCHAR(50),
	ImportTotalRecords				INT,
	PhoneColumns					VARCHAR(1024),
	PreviewModeAcceptedValues		VARCHAR(1024),
	PreviewModeColumnName			VARCHAR(255),
	Size							INT,
	[Version]						INT,
	ZipCodeColumnName				VARCHAR(255)
	PRIMARY KEY (ContactListId)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcOutboundContacts;
CREATE TABLE [gen].gcOutboundContacts (
	ContactListId					VARCHAR(50),
	[inin-outbound-id]				VARCHAR(50),
	CustID							VARCHAR(50),
	FirstName						VARCHAR(50),
	LastName						VARCHAR(50),
	Timezone						VARCHAR(50),
	ContactCallable					VARCHAR(50),
	Phone_1							VARCHAR(50),
	[Contact_Attempts]				INT,
	[CallRecordLastAttempt-Phone_1]	DATETIME,
	[CallRecordLastResult-Phone_1]	VARCHAR(50),
	[CallRecordLastAgentWrapup-Phone_1]	VARCHAR(50),
	[SmsLastAttempt-Phone_1]			VARCHAR(50),
	[SmsLastResult-Phone_1]			VARCHAR(50),
	[Callable-Phone_1]				VARCHAR(50),
	Phone_2							VARCHAR(50),
	[CallRecordLastAttempt-Phone_2]	DATETIME,
	[CallRecordLastResult-Phone_2]	VARCHAR(50),
	[CallRecordLastAgentWrapup-Phone_2]	VARCHAR(50),
	[SmsLastAttempt-Phone_2]			VARCHAR(50),
	[SmsLastResult-Phone_2]			VARCHAR(50),
	[Callable-Phone_2]				VARCHAR(50),
	Phone_3							VARCHAR(50),
	[CallRecordLastAttempt-Phone_3]	DATETIME,
	[CallRecordLastResult-Phone_3]	VARCHAR(50),
	[CallRecordLastAgentWrapup-Phone_3]	VARCHAR(50),
	[SmsLastAttempt-Phone_3]			VARCHAR(50),
	[SmsLastResult-Phone_3]			VARCHAR(50),
	[Callable-Phone_3]				VARCHAR(50),
	PRIMARY KEY (ContactListId, [inin-outbound-id])
)



DROP TABLE IF EXISTS gen.gcOutboundContactsExtraFields;
CREATE TABLE [gen].gcOutboundContactsExtraFields (
	ContactListId					VARCHAR(50),
	[inin-outbound-id]				VARCHAR(50),
	Field							VARCHAR(50),
	Value							VARCHAR(100),
	DateAdded						DATETIME,
	Latest							SMALLINT,
	PRIMARY KEY (ContactListId, [inin-outbound-id], Field, DateAdded)
)
CREATE NONCLUSTERED INDEX idx_gcOutboundContactsExtraFields ON gen.gcOutboundContactsExtraFields (Latest, ContactListId, [inin-outbound-id], Field); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcOutboundMappings;
CREATE TABLE gen.gcOutboundMappings (
	WrapupCodeId					VARCHAR(50),
	OutboundMappingName				VARCHAR(50)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcOutboundRulesets;
CREATE TABLE gen.gcOutboundRulesets (
	OutboundRulesetId				VARCHAR(50),
	OutboundRulesetName				VARCHAR(100),
	ContactListId					VARCHAR(50),
	ContactListName					VARCHAR(100),
	DateCreated						DATETIME,
	DateModified					DATETIME,
	QueueId							VARCHAR(50),
	QueueName						VARCHAR(100),
	Actions							VARCHAR(500),
	Version							INTEGER,
	PRIMARY KEY (OutboundRulesetId)
)


DROP TABLE IF EXISTS gen.gcOutboundCampaignRules;
CREATE TABLE gen.gcOutboundCampaignRules (
	OutboundCampaignRuleId			VARCHAR(50),
	OutboundCampaignRuleName		VARCHAR(100),
	DateCreated						DATETIME,
	DateModified					DATETIME,
	Enabled							VARCHAR(10),
	MatchAnyConditions				VARCHAR(10),
	OutboundCampaignId				VARCHAR(50),
	OutboundCampaignName			VARCHAR(100),
	SequenceId						VARCHAR(50),
	SequenceName					VARCHAR(100),
	Version							INTEGER
)

--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcFlows;
CREATE TABLE gen.gcFlows (
	FlowId							VARCHAR(50),
	FlowName						VARCHAR(50),
	Active							VARCHAR(10),
	CheckedInVersion				VARCHAR(50),
	DebugVersion					VARCHAR(50),
	Deleted							VARCHAR(50),
	[Description]					VARCHAR(512),
	Division						VARCHAR(50),
	PublishedBy						VARCHAR(255),
	PublishedVersion				VARCHAR(50),
	SavedVersion					VARCHAR(50),
	SupportedLanguages				VARCHAR(100),
	[Type]							VARCHAR(50),
	PRIMARY KEY (FlowId)
)


--------------------------------------------------------------------------------------------------------------------------


--DROP TABLE IF EXISTS gen.gcFlowMilestones;
--CREATE TABLE gen.gcFlowMilestones (
--	FlowMilestoneId					VARCHAR(50),
--	FlowMilestoneName				VARCHAR(50),
--	Description						VARCHAR(50),
--	PRIMARY KEY (FlowMilestoneId)
--)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcFlowOutcomes;
CREATE TABLE gen.gcFlowOutcomes (
	FlowOutcomeId					VARCHAR(50),
	FlowOutcomeName					VARCHAR(50),
	[Description]					VARCHAR(50),
	PRIMARY KEY (FlowOutcomeId)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcPresences;
CREATE TABLE gen.gcPresences (
	PresenceId						VARCHAR(50),
	PresenceName					VARCHAR(30),
	PRIMARY KEY (PresenceId)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcOrgPresences;
CREATE TABLE gen.gcOrgPresences (
	OrgPresenceId					VARCHAR(50),
	OrgPresenceName					VARCHAR(30),
	SystemPresence					VARCHAR(30),
	[Primary]						VARCHAR(10),
	Deactivated						VARCHAR(10),
	CreatedById  					VARCHAR(50),
	CreatedByName  					VARCHAR(255),
	CreatedByDateUTC 				DATETIME,
	ModifiedById  					VARCHAR(50),
	ModifiedByName  				VARCHAR(255),
	ModifiedByDateUTC 				DATETIME,
	PRIMARY KEY (OrgPresenceId)
)


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcEdges;
CREATE TABLE gen.gcEdges (
	EdgeId							VARCHAR(50),
	EdgeName						VARCHAR(50),
	Active							VARCHAR(10),
	ApiVersion						VARCHAR(50),
	CallDrainingState				VARCHAR(50),
	ConversationCount				INT,
	CreatedBy						VARCHAR(100),
	CreatedByApp					VARCHAR(100),
	CurrentVersion					VARCHAR(255),
	DateCreated						DATETIME,
	DateModified					DATETIME,
	[Description]					VARCHAR(512),
	EdgeDeploymentType				VARCHAR(50),
	EdgeGroupId						VARCHAR(50),
	EdgeGroupName					VARCHAR(255),
	Fingerprint						VARCHAR(100),
	FingerprintHint					VARCHAR(100),
	FullSoftwareVersion				VARCHAR(100),
	InterfacesCount					INT,
	Make							VARCHAR(100),
	Managed							VARCHAR(100),
	Model							VARCHAR(100),
	ModifiedBy						VARCHAR(100),
	ModifiedByApp					VARCHAR(100),
	OfflineConfigCalled				VARCHAR(100),
	OnlineStatus					VARCHAR(100),
	OsName							VARCHAR(255),
	PairingId						VARCHAR(50),
	Patch							VARCHAR(100),
	PhysicalEdge					VARCHAR(100),
	Proxy							VARCHAR(100),
	SerialNumber					VARCHAR(100),
	SiteId							VARCHAR(50),
	SiteName						VARCHAR(255),
	SiteDescription					VARCHAR(512),
	SiteLocationId					VARCHAR(50),
	SiteLocationName				VARCHAR(100),
	SoftwareVersion					VARCHAR(100),
	SoftwareVersionConfiguration	VARCHAR(100),
	SoftwareVersionPlatform			VARCHAR(100),
	SoftwareVersionTimestamp		VARCHAR(100),
	StagedVersion					VARCHAR(100),
	[State]							VARCHAR(100),
	StatusCode						VARCHAR(100),
	[Version]						INT,
	PRIMARY KEY (EdgeId)
)



DROP TABLE IF EXISTS gen.gcPhones;
CREATE TABLE gen.gcPhones (
	PhoneId							VARCHAR(50),
	PhoneName						VARCHAR(255),
	CapabilityAllowReboot			VARCHAR(10),
	CapabilityCdm					VARCHAR(10),
	CapabilityDualRegisters			VARCHAR(10),
	CapabilityHardwareIdType		VARCHAR(100),
	CapabilityMediaCodecs			VARCHAR(100),
	CapabilityNoCloudProvisioning	VARCHAR(10),
	CapabilityNoRebalance			VARCHAR(10),
	CapabilityProvisions			VARCHAR(10),
	CapabilityRegisters				VARCHAR(10),
	CreatedBy						VARCHAR(100),
	CreatedByApp					VARCHAR(100),
	DateCreated						DATETIME,
	DateModified					DATETIME,
	[Description]					VARCHAR(50),
	LineBaseSettingsId				VARCHAR(50),
	LineBaseSettingsName			VARCHAR(100),
	FirstLineId						VARCHAR(50),
	FirstLineName					VARCHAR(100),
	FirstLineDecription				VARCHAR(512),
	FirstLineLoggedInUserId			VARCHAR(50),
	FirstLineLoggedInUserName		VARCHAR(100),
	FirstLinePriEdgeId				VARCHAR(50),
	FirstLinePriEdgeName			VARCHAR(100),
	FirstLineSiteId					VARCHAR(50),
	FirstLineSiteName				VARCHAR(100),
	FirstLineState					VARCHAR(50),
	ModifiedBy						VARCHAR(100),
	ModifiedByApp					VARCHAR(100),
	PhoneBaseSettingsId				VARCHAR(50),
	PhoneBaseSettingsName			VARCHAR(100),
	PrimaryEdgeId					VARCHAR(50),
	PrimaryEdgeName					VARCHAR(100),
	Properties						VARCHAR(100),
	SecondaryEdgeId					VARCHAR(50),
	SecondaryEdgeName				VARCHAR(100),
	SecondaryStatus					VARCHAR(50),
	SiteId							VARCHAR(50),
	SiteName						VARCHAR(100),
	[State]							VARCHAR(50),
	OperationalStatus				VARCHAR(50),
	EdgesStatus						VARCHAR(50),
	UserAgentInfoFirmwareVersion	VARCHAR(100),
	UserAgentInfoManufacturer		VARCHAR(100),
	UserAgentInfoModel				VARCHAR(100),
	[Version]						INTEGER NULL,
	WebRtcUserId					VARCHAR(50),
	WebRtcUserName					VARCHAR(100),
	PRIMARY KEY (PhoneId)
)


--------------------------------------------------------------------------------------------------------------------------
-- -- PureCloud Extract Tables - Transactions
--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversations;
CREATE TABLE gen.gcConversations(
	ConversationId					VARCHAR(50),
	ConversationStart				DATETIME,
	ConversationEnd					DATETIME,
	dIntervalStart					DATETIME,
	dIntervalEnd					DATETIME,
	dIntervalStartUTC				DATETIME,
	dIntervalEndUTC					DATETIME,
	ConversationStartUTC			DATETIME,
	ConversationEndUTC				DATETIME,
	MediaStatsMinConversationMos	NUMERIC(10,2),
	MediaStatsMinConversationRFactor	NUMERIC(10,2),
	OriginatingDirection			VARCHAR(20)
)
CREATE CLUSTERED INDEX idx_gcConversations ON gen.gcConversations (ConversationStartUTC, ConversationId); 
CREATE NONCLUSTERED INDEX idx_gcConversationsId ON gen.gcConversations (ConversationId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationDivisions;
CREATE TABLE gen.gcConversationDivisions (
	ConversationId					VARCHAR(50),
	ConversationStartUTC			DATETIME,
	DivisionId						VARCHAR(50)
)
CREATE CLUSTERED INDEX idx_gcConversationDivisions ON gen.gcConversationDivisions (ConversationStartUTC); 
CREATE NONCLUSTERED INDEX idx_gcConversationDivisionsId ON gen.gcConversationDivisions (ConversationId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationParticipants;
CREATE TABLE gen.gcConversationParticipants (
	ConversationId					VARCHAR(50),
	ParticipantId					VARCHAR(50),
	ConversationStartUTC			DATETIME,
	Attributes						VARCHAR(512),
	ExternalContactId				VARCHAR(50),
	ExternalOrganizationId			VARCHAR(50),
	FlaggedReason					VARCHAR(50),
	ParticipantName					VARCHAR(255),
	Purpose							VARCHAR(50),
	TeamId							VARCHAR(50),
	UserId							VARCHAR(50)
)
CREATE CLUSTERED INDEX idx_gcConversationParticipants ON gen.gcConversationParticipants (ConversationStartUTC, ConversationId); 
CREATE NONCLUSTERED INDEX idx_gcConversationParticipantsId ON gen.gcConversationParticipants (ParticipantId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationSessions;
CREATE TABLE gen.gcConversationSessions (
	ConversationId					VARCHAR(50),
	ParticipantId					VARCHAR(50),
	UserId							VARCHAR(50),
	SessionId						VARCHAR(50),
	ConversationStartUTC			DATETIME,
	AcwSkipped						VARCHAR(10),
    AddressFROM 					VARCHAR(255),
    AddressOther					VARCHAR(255), 
    AddressSelf 					VARCHAR(255),
    AddressTo 						VARCHAR(255),
    AgentAssistantId 				VARCHAR(50),
    Ani 							VARCHAR(255),
    AssignerId 						VARCHAR(50),
    --CallbackNumbers 
    CallbackScheduledTimeUTC		DATETIME,
    CallbackUserName 				VARCHAR(255),
    CobrowseRole 					VARCHAR(50),
    CobrowseRoomId 					VARCHAR(50),
    Direction 						VARCHAR(20),
    DispositionAnalyzer 			VARCHAR(255),
    DispositionName 				VARCHAR(50),
    Dnis 							VARCHAR(255),
    EdgeId 							VARCHAR(50),
	FlowId							VARCHAR(50),
    FlowName 						VARCHAR(255),
	EndingLanguage					VARCHAR(50),
	EntryReason						VARCHAR(50),
	EntryType						VARCHAR(50),
	ExitReason						VARCHAR(50),
	FlowType						VARCHAR(50),
	FlowVersion						VARCHAR(20),
	IssuedCallback					VARCHAR(10),
	--Outcomes						VARCHAR(255),
	StartingLanguage				VARCHAR(50),
	TransferTargetAddress			VARCHAR(255),
	TransferTargetName				VARCHAR(255),
	TransferType					VARCHAR(50),
    FlowInType 						VARCHAR(50),
    FlowOutType 					VARCHAR(50),
    JourneyActionId 				VARCHAR(50),
    JourneyActionMapId 				VARCHAR(50),
    JourneyActionMapVersion			VARCHAR(20),
    JourneyCustomerId 				VARCHAR(50),
    JourneyCustomerIdType 			VARCHAR(50),
    JourneyCustomerSessionId		VARCHAR(50),
    JourneyCustomerSessionIdType 	VARCHAR(20),
    MediaBridgeId 					VARCHAR(50),
    MediaCount						INTEGER,
    --MediaEndpointStats
    MediaType 						VARCHAR(20),
    MessageType 					VARCHAR(20),
    MonitoredParticipantId 			VARCHAR(50),
    MonitoredSessionId 				VARCHAR(50),
    OutboundCampaignId 				VARCHAR(50),
    OutboundContactId 				VARCHAR(50),
    OutboundContactListId 			VARCHAR(50),
    PeerId 							VARCHAR(50),
    --ProposedAgents
    ProtocolCallId 					VARCHAR(50),
    Provider 						VARCHAR(50),
    Recording 						VARCHAR(10),
    Remote 							VARCHAR(255),
    RemoteNameDisplayable 			VARCHAR(255),
    --RequestedRoutings 
    RoomId 							VARCHAR(50),
    ScreenShareAddressSelf			VARCHAR(50),
    ScreenShareRoomId 				VARCHAR(50),
    ScriptId 						VARCHAR(50),
    SelectedAgentId 				VARCHAR(50),
    SelectedAgentRank				INTEGER,
    SessionDnis 					VARCHAR(255),
    SharingScreen					VARCHAR(10),
    SkipEnabled 					VARCHAR(10),
    TimeoutSeconds					INTEGER,
    UsedRouting 					VARCHAR(50),
    VideoAddressSelf				VARCHAR(50),
    VideoRoomId 					VARCHAR(50),
	
    FirstQueueId 					VARCHAR(50),
    LastWrapUpCode 					VARCHAR(50),
	LastDisconnectType				VARCHAR(50),
	StartTimeUTC					DATETIME,
	EndTimeUTC						DATETIME,

	nBlindTransferred				INT	,
	nCobrowseSessions				INT	,
	nConnected						INT	,
	nConsult						INT	,
	nConsultTransferred				INT	,
	nError							INT	,
	nOffered						INT	,
	nOutbound						INT	,
	nOutboundAbandoned				INT	,
	nOutboundAttempted				INT	,
	nOutboundConnected				INT	,
	nOverSla						INT	,
	nStateTransitionError			INT	,
	nTransferred					INT	,
	oExternalMediaCount				INT	,
	oMediaCount						INT	,
	oServiceLevel					NUMERIC(10,2),
	oServiceTarget					NUMERIC(10,2),
	tAbandon						INT	,
	nAbandon						INT,
	maxAbandon						INT,
	tAcd							INT,
	nAcd							INT,
	maxAcd							INT,
	tAcw							INT	,
	nAcw							INT	,
	maxAcw							INT	,
	tAgentResponseTime				INT	,
	nAgentResponseTime				INT	,
	maxAgentResponseTime			INT	,
	tAlert							INT	,
	nAlert							INT	,
	maxAlert						INT	,
	tAnswered						INT	,
	nAnswered						INT,
	maxAnswered						INT,
	tCallback						INT	,
	nCallback						INT	,
	maxallback						INT	,
	tCallbackComplete				INT	,
	nCallbackComplete				INT	,
	maxCallbackComplete				INT	,
	tContacting						INT	,
	nContacting						INT	,
	maxContacting					INT	,
	tDialing						INT	,
	nDialing						INT	,
	maxDialing						INT	,
	tFlowOut						INT	,
	nFlowOut						INT	,
	maxFlowOut						INT	,
	tHandle							INT	,
	nHandle							INT	,
	maxHandle						INT	,
	tHeld							INT	,
	nHeld							INT,
	maxHeld							INT,
	tHeldComplete					INT	,
	nHeldComplete					INT	,
	maxHeldComplete					INT	,
	tIvr							INT	,
	nIvr							INT	,
	maxIvr							INT	,
	tMonitoring						INT	,
	nMonitoring						INT	,
	maxMonitoring					INT	,
	tNotResponding					INT	,
	nNotResponding					INT	,
	maxNotResponding				INT	,
	tShortAbandon					INT	,
	nShortAbandon					INT	,
	maxShortAbandon					INT	,
	tTalk							INT	,
	nTalk							INT,
	maxTalk							INT,
	tTalkComplete					INT	,
	nTalkComplete					INT	,
	maxTalkComplete					INT	,
	tUserResponseTime				INT,
	nUserResponseTime				INT	,
	maxUserResponseTime				INT	,
	tVoicemail						INT,
	nVoicemail						INT,
	maxVoicemail					INT,
	tWait							INT,
	nWait							INT,
	maxWait							INT,
	nFlow							INT,
	nFlowMilestone					INT,
	nFlowOutcome					INT,
	nFlowOutcomeFailed				INT,
	tFlow							INT,	
	tFlowDisconnect					INT,
	tFlowExit						INT,
	tFlowOutcome					INT
)
CREATE CLUSTERED INDEX idx_gcConversationSessions ON gen.gcConversationSessions (ConversationStartUTC, ConversationId, SessionId); 
CREATE NONCLUSTERED INDEX idx_gcConversationSessionsId ON gen.gcConversationSessions (SessionID);


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationFlowOutcomes;
CREATE TABLE gen.gcConversationFlowOutcomes (
	ConversationId					VARCHAR(50),
	ParticipantId					VARCHAR(50),
	UserId							VARCHAR(50),
	SessionId						VARCHAR(512),
	FlowId							VARCHAR(50),
	ConversationStartUTC			DATETIME,
	FlowOutcomeStartTimestamp		DATETIME,
	FlowOutcomeEndTimestamp			DATETIME,
	FlowOutcomeId					VARCHAR(255),
	FlowOutcome						VARCHAR(100),
	FlowOutcomeValue				VARCHAR(100)
)
CREATE CLUSTERED INDEX idx_gcConversationFlowOutcomes ON gen.gcConversationFlowOutcomes (ConversationStartUTC, ConversationId); 
--CREATE NONCLUSTERED INDEX idx_gcConversationFlowOutcomesId ON gen.gcConversationFlowOutcomes (ConversationId); 
CREATE NONCLUSTERED INDEX idx_gcConversationFlowOutcomesId2 ON gen.gcConversationFlowOutcomes (SessionId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationSegments;
CREATE TABLE gen.gcConversationSegments (
	ConversationId					VARCHAR(50),
	ConversationSessionId			VARCHAR(50),
	ParticipantId					VARCHAR(50),
	ConversationStartUTC			DATETIME,
	SegmentStartUTC					DATETIME,
	SegmentEndUTC					DATETIME,
	AudioMuted						VARCHAR(10),
	Conference						VARCHAR(10),
	DestinationConversationId		VARCHAR(50),
	DestinationSessionId			VARCHAR(50),
	DisconnectType					VARCHAR(50),
	ErrorCode						VARCHAR(255),
	GroupId							VARCHAR(50),
	Properties						VARCHAR(255),
	QueueId							VARCHAR(50),
	RequestedLanguageId				VARCHAR(50),
	RequestedRoutingSkillIds		VARCHAR(255),
	RequestedRoutingUserIds			VARCHAR(255),
	ScoredAgents					VARCHAR(255),
	SegmentType						VARCHAR(50),
	SourceConversationId			VARCHAR(50),
	SourceSessionId					VARCHAR(50),
	Subject							VARCHAR(255),
	VideoMuted						VARCHAR(10),
	WrapUpCode						VARCHAR(50),
	WrapUpNote						VARCHAR(255),
	WrapUpTags						VARCHAR(255)
)
CREATE CLUSTERED INDEX idx_gcConversationSegments ON gen.gcConversationSegments (ConversationStartUTC, ConversationSessionId); 
CREATE NONCLUSTERED INDEX idx_gcConversationSegmentsId ON gen.gcConversationSegments (ConversationSessionId);


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationResolutions;
CREATE TABLE gen.gcConversationResolutions (
	ConversationId					VARCHAR(50),
	ConversationStartUTC			DATETIME,
	GetnNextContactAvoided			INTEGER,
	QueueId							VARCHAR(50),
	UserId							VARCHAR(50)
)
CREATE CLUSTERED INDEX idx_gcConversationResolutions ON gen.gcConversationResolutions (ConversationStartUTC, ConversationId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationRecordings;
CREATE TABLE gen.gcConversationRecordings (
	ConversationId					VARCHAR(50),
	SessionId						VARCHAR(50),
	RecordingId						VARCHAR(50),
	ConversationStartUTC			DATETIME,
	Media							VARCHAR(20),
	FileState						VARCHAR(50),
	Name							VARCHAR(50),
	Path							VARCHAR(255),
	StartTime						VARCHAR(50),
	EndTime							VARCHAR(50),
	SelfUri							VARCHAR(255),
	Annotations						VARCHAR(1024)
)
CREATE CLUSTERED INDEX idx_gcConversationRecordings ON gen.gcConversationRecordings (ConversationStartUTC); 
CREATE NONCLUSTERED INDEX idx_gcConversationRecordingsId ON gen.gcConversationRecordings (ConversationId);
CREATE NONCLUSTERED INDEX idx_gcConversationRecordingsId2 ON gen.gcConversationRecordings (SessionId);


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationEvaluations;
CREATE TABLE gen.gcConversationEvaluations (
	ConversationId					VARCHAR(50),
	ConversationEvaluationId		VARCHAR(50),
	CalibrationId					VARCHAR(50),
	ConversationStartUTC			DATETIME,
	AgentHasRead					VARCHAR(10),
	AssignedDateUTC					DATETIME,
	ChangedDateUTC					DATETIME,
	ConversationDateUTC				DATETIME,
	ConversationEndDateUTC			DATETIME,
	Comments						VARCHAR(512),
	AgentComments					VARCHAR(512),
	Status							VARCHAR(20),
	EvaluatorId						VARCHAR(50),
	EvaluatorName					VARCHAR(255),
	EvaluationFormId				VARCHAR(50),
	EvaluationFormName				VARCHAR(255),
	TotalCriticalScorePct			NUMERIC(10,2),
	TotalNonCriticalScorePct		NUMERIC(10,2),
	TotalScorePct					NUMERIC(10,2),
	AnyFailedKillQuestions			VARCHAR(10),
	QueueId							VARCHAR(50),
	QueueName						VARCHAR(255),
	Rescored						VARCHAR(10),
	UserId							VARCHAR(50),
	UserName						VARCHAR(255),
	TotalScore						NUMERIC(10,2),
	TotalCriticalScore				NUMERIC(10,2),
	TotalNonCriticalScore			NUMERIC(10,2),
	TotalScoreUnweighted			NUMERIC(10,2),
	TotalCriticalScoreUnweighted	NUMERIC(10,2),
	TotalNonCriticalScoreUnweighted NUMERIC(10,2),
	MaxTotalScore					NUMERIC(10,2),
	MaxTotalCriticalScore			NUMERIC(10,2),
	MaxTotalNonCriticalScore		NUMERIC(10,2),
	MaxTotalScoreUnweighted			NUMERIC(10,2),
	MaxTotalCriticalScoreUnweighted NUMERIC(10,2),
	MaxTotalNonCriticalScoreUnweighted NUMERIC(10,2),
	--TotalNumCriticalQuestions	INTEGER,
	--NumAnsweredCriticalQstns	INTEGER,
	--NumPosAnsweredCriticalQstns	INTEGER
)
CREATE CLUSTERED INDEX idx_gcConversationEvaluations ON gen.gcConversationEvaluations (ConversationStartUTC, ConversationId, ConversationEvaluationId); 
CREATE NONCLUSTERED INDEX idx_gcConversationEvaluationsId ON gen.gcConversationEvaluations (ConversationId); 
CREATE NONCLUSTERED INDEX idx_gcConversationEvaluationsId2 ON gen.gcConversationEvaluations (ConversationEvaluationId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationEvalQGScores;
CREATE TABLE gen.gcConversationEvalQGScores (
	ConversationId					VARCHAR(50),
	ConversationEvaluationId		VARCHAR(50),
	EvaluationFormId				VARCHAR(50),
	EvaluationQuestionGroupId		VARCHAR(50),
	ConversationStartUTC			DATETIME,
	MarkedNA						VARCHAR(10),
	TotalScore						NUMERIC(10,2),
	TotalCriticalScore				NUMERIC(10,2),
	TotalNonCriticalScore			NUMERIC(10,2),
	TotalScoreUnweighted			NUMERIC(10,2),
	TotalCriticalScoreUnweighted	NUMERIC(10,2),
	TotalNonCriticalScoreUnweighted NUMERIC(10,2),
	MaxTotalScore					NUMERIC(10,2),
	MaxTotalCriticalScore			NUMERIC(10,2),
	MaxTotalNonCriticalScore		NUMERIC(10,2),
	MaxTotalScoreUnweighted			NUMERIC(10,2),
	MaxTotalCriticalScoreUnweighted NUMERIC(10,2),
	MaxTotalNonCriticalScoreUnweighted NUMERIC(10,2)
)
CREATE CLUSTERED INDEX idx_gcConversationEvalQGScores ON gen.gcConversationEvalQGScores (ConversationStartUTC, ConversationId, ConversationEvaluationId, EvaluationQuestionGroupId); 
CREATE NONCLUSTERED INDEX idx_gcConversationEvalQGScoresId ON gen.gcConversationEvalQGScores (ConversationId); 
CREATE NONCLUSTERED INDEX idx_gcConversationEvalQGScoresId2 ON gen.gcConversationEvalQGScores (EvaluationQuestionGroupId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationEvalQScores;
CREATE TABLE gen.gcConversationEvalQScores (
	ConversationId					VARCHAR(50),
	ConversationEvaluationId		VARCHAR(50),
	EvaluationFormId				VARCHAR(50),
	EvaluationQuestionGroupId		VARCHAR(50),
	EvaluationQuestionId			VARCHAR(50),
	AnswerId						VARCHAR(50),
	ConversationStartUTC			DATETIME,
	Comments						VARCHAR(512),
	FailedKillQuestion				VARCHAR(10),
	MarkedNA						VARCHAR(10),
	Score							INT
)
CREATE CLUSTERED INDEX idx_gcConversationEvalQScores ON gen.gcConversationEvalQScores (ConversationStartUTC, ConversationId, ConversationEvaluationId, EvaluationQuestionGroupId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcConversationAggregates;
CREATE TABLE gen.gcConversationAggregates (

	conversationId					VARCHAR(50),
	interval  						VARCHAR(50),
	dIntervalStart					DATETIME,
	dIntervalStartUTC				DATETIME,
	nDuration 						INTEGER,

	-- Dimensions
	--activeSkillId					VARCHAR(50),
	--addressFrom					VARCHAR(50),
	--addressTo						VARCHAR(50),
	--agentAssistantId				VARCHAR(50),
	--agentBullseyeRing				INTEGER,
	--agentRank						INTEGER,
	--agentScore					INTEGER,
	ani								VARCHAR(255),
	--assignerId					VARCHAR(50),
	--bullseyeRing					INTEGER,
	--convertedFrom					VARCHAR(50),
	--convertedTo					VARCHAR(50),
	direction						VARCHAR(10),
	--disconnectType				VARCHAR(20),
	--divisionId					VARCHAR(50),
	dnis							VARCHAR(255),
	--edgeId						VARCHAR(50),
	--externalContactId				VARCHAR(50),
	--externalMediaCount			INTEGER,
	--externalOrganizationId		VARCHAR(50),
	--firstQueue					INT,
	--flaggedReason					VARCHAR(50),
	--flowInType					VARCHAR(20),
	--flowOutType					VARCHAR(20),
	--groupId							VARCHAR(50),
	interactionType					VARCHAR(20),
	--journeyActionId				VARCHAR(50),
	--journeyActionMapId			VARCHAR(50),
	--journeyActionMapVersion		INTEGER,
	--journeyCustomerId				VARCHAR(50),
	--journeyCustomerIdType			VARCHAR(20),
	--journeyCustomerSessionId		VARCHAR(50),
	--journeyCustomerSessionIdType	VARCHAR(20),
	--mediaCount					INTEGER,
	mediaType						VARCHAR(20),
	--messageType					VARCHAR(20),
	--originatingDirection			VARCHAR(10),
	outboundCampaignId				VARCHAR(50),
	outboundContactId				VARCHAR(50),
	outboundContactListId			VARCHAR(50),
	--participantName				VARCHAR(255),
	--peerId						VARCHAR(50),
	--proposedAgentId				VARCHAR(50),
	--provider						VARCHAR(50),
	purpose							VARCHAR(20),
	queueId							VARCHAR(50),
	remote							VARCHAR(255),
	--removedSkillId				VARCHAR(50),
	--reoffered						INTEGER,
	--requestedLanguageId			VARCHAR(50),
	--requestedRouting				VARCHAR(20),
	--requestedRoutingSkillId		VARCHAR(50),
	--roomId						VARCHAR(50),
	--routingPriority				BIGINT,
	--routingRule					INTEGER,
	--scoredAgentId					VARCHAR(50),
	--selectedAgentId				VARCHAR(50),
	--selectedAgentRank				INTEGER,
	--sessionDnis					VARCHAR(255),
	--sessionId						VARCHAR(50),
	--stationId						VARCHAR(50),
	--teamId						VARCHAR(50),
	--usedRouting					VARCHAR(20),
	userId							VARCHAR(50),
	--wrapUpCode					VARCHAR(50),

	-- Metrics
	nBlindTransferred				INT	,
	nCobrowseSessions				INT	,
	nConnected						INT	,
	nConsult						INT	,
	nConsultTransferred				INT	,
	nError							INT	,
	nOffered						INT	,
	nOutbound						INT	,
	nOutboundAbandoned				INT	,
	nOutboundAttempted				INT	,
	nOutboundConnected				INT	,
	nOverSla						INT	,
	nStateTransitionError			INT	,
	nTransferred					INT	,
	oExternalMediaCount				INT	,
	oMediaCount						INT	,
	oServiceLevel					NUMERIC(10,2),
	oServiceTarget					NUMERIC(10,2),
	tAbandon						INT	,
	nAbandon						INT,
	maxAbandon						INT,
	tAcd							INT,
	nAcd							INT,
	maxAcd							INT,
	tAcw							INT	,
	nAcw							INT	,
	maxAcw							INT	,
	tAgentResponseTime				INT	,
	nAgentResponseTime				INT	,
	maxAgentResponseTime			INT	,
	tAlert							INT	,
	nAlert							INT	,
	maxAlert						INT	,
	tAnswered						INT	,
	nAnswered						INT,
	maxAnswered						INT,
	tCallback						INT	,
	nCallback						INT	,
	maxallback						INT	,
	tCallbackComplete				INT	,
	nCallbackComplete				INT	,
	maxCallbackComplete				INT	,
	tContacting						INT	,
	nContacting						INT	,
	maxContacting					INT	,
	tDialing						INT	,
	nDialing						INT	,
	maxDialing						INT	,
	tFlowOut						INT	,
	nFlowOut						INT	,
	maxFlowOut						INT	,
	tHandle							INT	,
	nHandle							INT	,
	maxHandle						INT	,
	tHeld							INT	,
	nHeld							INT,
	maxHeld							INT,
	tHeldComplete					INT	,
	nHeldComplete					INT	,
	maxHeldComplete					INT	,
	tIvr							INT	,
	nIvr							INT	,
	maxIvr							INT	,
	tMonitoring						INT	,
	nMonitoring						INT	,
	maxMonitoring					INT	,
	tNotResponding					INT	,
	nNotResponding					INT	,
	maxNotResponding				INT	,
	tShortAbandon					INT	,
	nShortAbandon					INT	,
	maxShortAbandon					INT	,
	tTalk							INT	,
	nTalk							INT,
	maxTalk							INT,
	tTalkComplete					INT	,
	nTalkComplete					INT	,
	maxTalkComplete					INT	,
	tUserResponseTime				INT,
	nUserResponseTime				INT	,
	maxUserResponseTime				INT	,
	tVoicemail						INT,
	nVoicemail						INT,
	maxVoicemail					INT,
	tWait							INT,
	nWait							INT,
	maxWait							INT
)
CREATE CLUSTERED INDEX idx_gcConversationAggregates ON gen.gcConversationAggregates (dIntervalStartUTC, conversationId); 
CREATE NONCLUSTERED INDEX idx_gcConversationAggregatesId ON gen.gcConversationAggregates (conversationId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcQueueAggregates;
CREATE TABLE gen.gcQueueAggregates (

	id 								INT NOT NULL IDENTITY PRIMARY KEY,
	interval  						VARCHAR(50),
	dIntervalStart					DATETIME,
	dIntervalStartUTC				DATETIME,
	nDuration 						INTEGER,

	-- Dimensions
	mediaType						VARCHAR(20),
	participantName					VARCHAR(255),
	purpose							VARCHAR(20),
	queueId							VARCHAR(50),
	userId							VARCHAR(50),
	remote							VARCHAR(255),

	-- Metrics
	nBlindTransferred				INT	,
	nCobrowseSessions				INT	,
	nConnected						INT	,
	nConsult						INT	,
	nConsultTransferred				INT	,
	nError							INT	,
	nOffered						INT	,
	nOutbound						INT	,
	nOutboundAbandoned				INT	,
	nOutboundAttempted				INT	,
	nOutboundConnected				INT	,
	nOverSla						INT	,
	nStateTransitionError			INT	,
	nTransferred					INT	,
	oExternalMediaCount				INT	,
	oMediaCount						INT	,
	oServiceLevel					NUMERIC(10,2),
	oServiceTarget					NUMERIC(10,2),
	tAbandon						INT	,
	nAbandon						INT,
	maxAbandon						INT,
	tAcd							INT,
	nAcd							INT,
	maxAcd							INT,
	tAcw							INT	,
	nAcw							INT	,
	maxAcw							INT	,
	tAgentResponseTime				INT	,
	nAgentResponseTime				INT	,
	maxAgentResponseTime			INT	,
	tAlert							INT	,
	nAlert							INT	,
	maxAlert						INT	,
	tAnswered						INT	,
	nAnswered						INT,
	maxAnswered						INT,
	tCallback						INT	,
	nCallback						INT	,
	maxCallback						INT	,
	tCallbackComplete				INT	,
	nCallbackComplete				INT	,
	maxCallbackComplete				INT	,
	tContacting						INT	,
	nContacting						INT	,
	maxContacting					INT	,
	tDialing						INT	,
	nDialing						INT	,
	maxDialing						INT	,
	tFlowOut						INT	,
	nFlowOut						INT	,
	maxFlowOut						INT	,
	tHandle							INT	,
	nHandle							INT	,
	maxHandle						INT	,
	tHeld							INT	,
	nHeld							INT,
	maxHeld							INT,
	tHeldComplete					INT	,
	nHeldComplete					INT	,
	maxHeldComplete					INT	,
	tIvr							INT	,
	nIvr							INT	,
	maxIvr							INT	,
	tMonitoring						INT	,
	nMonitoring						INT	,
	maxMonitoring					INT	,
	tNotResponding					INT	,
	nNotResponding					INT	,
	maxNotResponding				INT	,
	tShortAbandon					INT	,
	nShortAbandon					INT	,
	maxShortAbandon					INT	,
	tTalk							INT	,
	nTalk							INT,
	maxTalk							INT,
	tTalkComplete					INT	,
	nTalkComplete					INT	,
	maxTalkComplete					INT	,
	tUserResponseTime				INT,
	nUserResponseTime				INT	,
	maxUserResponseTime				INT	,
	tVoicemail						INT,
	nVoicemail						INT,
	maxVoicemail					INT,
	tWait							INT,
	nWait							INT,
	maxWait							INT
)
CREATE NONCLUSTERED INDEX idx_gcQueueAggregates ON gen.gcQueueAggregates (dIntervalStartUTC, queueId); 
CREATE NONCLUSTERED INDEX idx_gcQueueAggregates2 ON gen.gcQueueAggregates (dIntervalStartUTC, userId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcUserStatusDetails;
CREATE TABLE gen.gcUserStatusDetails (
	UserId							VARCHAR(50),
	StartTimeUTC					DATETIME,
	EndTimeUTC						DATETIME,
	SystemPresence					VARCHAR(30),
	OrganizationPresenceId			VARCHAR(50)
)
CREATE CLUSTERED INDEX idx_gcUserStatusDetails ON gen.gcUserStatusDetails (StartTimeUTC, userId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcUserRoutingStatus;
CREATE TABLE gen.gcUserRoutingStatus (
	UserId							VARCHAR(50),
	StartTimeUTC					DATETIME,
	EndTimeUTC						DATETIME,
	RoutingStatus					VARCHAR(30)
)
CREATE CLUSTERED INDEX idx_gcUserRoutingStatus ON gen.gcUserRoutingStatus (StartTimeUTC, userId); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gcUserAggregates;
CREATE TABLE gen.gcUserAggregates (
	interval 						VARCHAR(50),
	dIntervalStart					DATETIME,
	dIntervalStartUTC				DATETIME,
	nDuration						INTEGER,
	userId							VARCHAR(50),
	userName						VARCHAR(255),
	OrgPresenceId					VARCHAR(50),
	OrgPresenceName					VARCHAR(50),
	tOrganizationPresence			INT,
	RoutingStatusName				VARCHAR(50),
	tAgentRoutingStatus				INT
)
CREATE CLUSTERED INDEX idx_gcUserAggregates ON gen.gcUserAggregates (dIntervalStartUTC); 


--------------------------------------------------------------------------------------------------------------------------
-- PureConnect Tables
--------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS gen.[IWrkgrpQueueStats];
CREATE TABLE [gen].[IWrkgrpQueueStats](
--	[StatisticsSet] 				INTEGER NOT NULL,
	[cName] 						nVARCHAR(255) NOT NULL,		-- changed form nVARCHAR(50)
	[cReportGroup] 					nVARCHAR(255) NOT NULL,		-- changed form nVARCHAR(50)
	[cHKey3]						nVARCHAR(100) NOT NULL,		-- changed form nVARCHAR(50)
	[cHKey4]						nVARCHAR(100) NOT NULL,		-- changed form nVARCHAR(50)
	[cType]							CHAR(1) NOT NULL,
	[dIntervalStart]				DATETIME NOT NULL,
	[dIntervalStartUTC]				DATETIME NULL,
	[nDuration] 					INTEGER NOT NULL,
	[nEnteredAcd] 					INTEGER NULL,
	[nAbandonedAcd]					INTEGER NULL,
	[nGrabbedAcd] 					INTEGER NULL,
	[nLocalDisconnectAcd]			INTEGER NULL,
	[nLocalDisconnectAgentAlertAcd]	INTEGER NULL,
	[nLocalDisconnectAgentAcd]		INTEGER NULL,
	[nAlertedAcd] 					INTEGER NULL,
	[nAnsweredAcd] 					INTEGER NULL,
	[nAnswered] 					INTEGER NULL,
	[nAcdSvcLvl]					INTEGER NULL,
	[nAnsweredAcdSvcLvl] 			INTEGER NULL,
	[nAnsweredAcdSvcLvl1]			INTEGER NULL,
	[nAnsweredAcdSvcLvl2]			INTEGER NULL,
	[nAnsweredAcdSvcLvl3]			INTEGER NULL,
	[nAnsweredAcdSvcLvl4]			INTEGER NULL,
	[nAnsweredAcdSvcLvl5]			INTEGER NULL,
	[nAnsweredAcdSvcLvl6]			INTEGER NULL,
	[nAbandonAcdSvcLvl]				INTEGER NULL,
	[nAbandonAcdSvcLvl1]			INTEGER NULL,
	[nAbandonAcdSvcLvl2]			INTEGER NULL,
	[nAbandonAcdSvcLvl3]			INTEGER NULL,
	[nAbandonAcdSvcLvl4]			INTEGER NULL,
	[nAbandonAcdSvcLvl5]			INTEGER NULL,
	[nAbandonAcdSvcLvl6]			INTEGER NULL,
	[tGrabbedAcd] 					INTEGER NULL,
	[tAnsweredAcd] 					INTEGER NULL,
	[mtAnsweredAcd] 				INTEGER NULL,
	[tAbandonedAcd] 				INTEGER NULL,
	[tTalkAcd] 						INTEGER NULL,
	[tTalkCompleteAcd] 				INTEGER NULL,
	[nHoldAcd] 						INTEGER NULL,
	[tHoldAcd] 						INTEGER NULL,
	[nSuspendedAcd] 				INTEGER NULL,
	[tSuspendedAcd] 				INTEGER NULL,
	[nHeldSpanAcd] 					INTEGER NULL,
	[nAcw] 							INTEGER NULL,
	[tAcw] 							INTEGER NULL,
	[tAcwComplete] 					INTEGER NULL,
	[nExternToInternCalls]			INTEGER NULL,
	[nExternToInternAcdCalls]		INTEGER NULL,
	[nInternToExternCalls]			INTEGER NULL,
	[nInternToExternAcdCalls]		INTEGER NULL,
	[nInternToInternCalls]			INTEGER NULL,
	[nInternToInternAcdCalls]		INTEGER NULL,
	[tExternToInternCalls]			INTEGER NULL,
	[tExternToInternAcdCalls]		INTEGER NULL,
	[tInternToExternCalls]			INTEGER NULL,
	[tInternToExternAcdCalls]		INTEGER NULL,
	[tInternToInternCalls]			INTEGER NULL,
	[tInternToInternAcdCalls]		INTEGER NULL,
	[nAcwCalls] 					INTEGER NULL,
	[tAcwCalls] 					INTEGER NULL,
	[nTransferedAcd]				INTEGER NULL,
	nConsult						INTEGER NULL,
	nBlindTransferred				INTEGER NULL,
	nConsultTransferred				INTEGER NULL,
	[nNotAnsweredAcd] 				INTEGER NULL,
	[tAlertedAcd] 					INTEGER NULL,
	[nFlowOutAcd] 					INTEGER NULL,
	[tFlowOutAcd] 					INTEGER NULL,
	[nStartWaitAlertAcdCalls] 		INTEGER NULL,
	[nStartActiveAcdCalls] 			INTEGER NULL,
	[nStartHeldAcdCalls] 			INTEGER NULL,
	[nEndWaitAlertAcdCalls] 		INTEGER NULL,
	[nEndActiveAcdCalls] 			INTEGER NULL,
	[nEndHeldAcdCalls] 				INTEGER NULL,
	[nTransferWithinAcdCalls]		INTEGER NULL,
	[nTransferOutAcdCalls]			INTEGER NULL,
	[nDisconnectAcd] 				INTEGER NULL,
	[tAgentLoggedIn] 				INTEGER NULL,
	[tAgentAvailable] 				INTEGER NULL,
	[tAgentTalk] 					INTEGER NULL,
	[tAgentOtherBusy] 				INTEGER NULL,
	[tAgentOnAcdCall] 				INTEGER NULL,
	[tAgentOnOtherAcdCall] 			INTEGER NULL,
	[tAgentInAcw]					INTEGER NULL,
	[tAgentOnNonAcdCall] 			INTEGER NULL,
	[tAgentDnd] 					INTEGER NULL,
	[tAgentNotAvailable] 			INTEGER NULL,
	[tAgentAcdLoggedIn] 			INTEGER NULL,
	[tAgentAcdLoggedIn2] 			INTEGER NULL,
	[tAgentStatusDnd] 				INTEGER NULL,
	[tAgentStatusAcw] 				INTEGER NULL,
	[tAgentLoggedInDiluted] 		INTEGER NULL,
	[tStatusGroupFollowup] 			INTEGER NOT NULL,
	[tStatusGroupBreak] 			INTEGER NOT NULL,
	[tStatusGroupTraining] 			INTEGER NOT NULL,
	[CustomValue1] 					INTEGER NULL,
	[CustomValue2]					INTEGER NULL,
	[CustomValue3] 					INTEGER NULL,
	[CustomValue4]					INTEGER NULL,
	[CustomValue5] 					INTEGER NULL,
	[CustomValue6] 					INTEGER NULL,
	[SiteId]						SMALLINT NOT NULL,
	[SubSiteId]						INTEGER NOT NULL,
	[I3TimeStampGMT] 				DATETIME NOT NULL,
	[DimensionSet] 					INTEGER NOT NULL,
	[SummDimensionSet] 				INTEGER NULL,
	[ConfigurationSet] 				INTEGER NOT NULL,
	[nServiceLevel] 				INTEGER NOT NULL,
	[cServiceLevels] 				VARCHAR(1024) NULL,
	[SchemaMajorVersion] 			TINYINT NOT NULL,
	[SchemaMinorVersion]			TINYINT NOT NULL
) 
CREATE CLUSTERED INDEX idx_IWrkgrpQueueStats ON gen.IWrkgrpQueueStats ([dIntervalStartUTC], [cName]); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.[AgentActivityLog];
CREATE TABLE [gen].[AgentActivityLog](
	[UserId] 						nVARCHAR(50) NOT NULL,
	[StatusDateTime] 				DATETIME NOT NULL,
	[StatusDateTimeGMT] 			DATETIME NOT NULL,
	[ChangedStatus] 				SMALLINT NOT NULL,
	[ChangedStatusGroup] 			SMALLINT NOT NULL,
	[ChangedLoggedIn] 				SMALLINT NOT NULL,
	[ChangedAcdLoggedIn] 			SMALLINT NOT NULL,
	[StatusKey] 					nVARCHAR(50) NOT NULL,
	[StatusGroup] 					nVARCHAR(50) NOT NULL,
	[LoggedIn] 						SMALLINT NOT NULL,
	[AcdLoggedIn] 					SMALLINT NOT NULL,
	[StatusDnd] 					SMALLINT NOT NULL,
	[StatusAcw] 					SMALLINT NOT NULL,
	[EndDateTime] 					DATETIME NOT NULL,
	[EndDateTimeGMT] 				DATETIME NOT NULL,
	[StateDuration] 				INTEGER NOT NULL,
	[SeqNo] 						SMALLINT NOT NULL,
	[I3TimeStampGMT] 				DATETIME NOT NULL,
	[SiteId] 						SMALLINT NOT NULL,
	[SubSiteId] 					SMALLINT NOT NULL,
 CONSTRAINT [PK_AgentActivityLog] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC,
	[SiteId] ASC,
	[StatusKey] ASC,
	[StatusDateTimeGMT] ASC,
	[EndDateTimeGMT] ASC,
	[SeqNo] ASC
)
) ON [PRIMARY]


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.[IAgentQueueStats];
CREATE TABLE [gen].[IAgentQueueStats](
--	[StatisticsSet] INTEGER NOT NULL,
	[cName] 						nVARCHAR(255) NOT NULL,
	[cReportGroup] 					nVARCHAR(255) NOT NULL,
	[cHKey3] 						nVARCHAR(100) NOT NULL,
	[cHKey4] 						nVARCHAR(100) NOT NULL,
	[cType] 						CHAR(1) NOT NULL,
	[dIntervalStart] 				DATETIME NOT NULL,
	[dIntervalStartUTC] 			DATETIME NULL,
	[nDuration] 					INTEGER NOT NULL,
	[nEnteredAcd] 					INTEGER NULL,
	[nAbandonedAcd] 				INTEGER NULL,
	[nGrabbedAcd] 					INTEGER NULL,
	[nLocalDisconnectAcd] 			INTEGER NULL,
	[nLocalDisconnectAgentAlertAcd] INTEGER NULL,
	[nLocalDisconnectAgentAcd]		INTEGER NULL,
	[nAlertedAcd] 					INTEGER NULL,
	[nAnsweredAcd] 					INTEGER NULL,
	[nAnswered] 					INTEGER NULL,
	[nAcdSvcLvl] 					INTEGER NULL,
	[nAnsweredAcdSvcLvl] 			INTEGER NULL,
	[nAnsweredAcdSvcLvl1]			INTEGER NULL,
	[nAnsweredAcdSvcLvl2]			INTEGER NULL,
	[nAnsweredAcdSvcLvl3]			INTEGER NULL,
	[nAnsweredAcdSvcLvl4]			INTEGER NULL,
	[nAnsweredAcdSvcLvl5]			INTEGER NULL,
	[nAnsweredAcdSvcLvl6]			INTEGER NULL,
	[nAbandonAcdSvcLvl]				INTEGER NULL,
	[nAbandonAcdSvcLvl1]			INTEGER NULL,
	[nAbandonAcdSvcLvl2]			INTEGER NULL,
	[nAbandonAcdSvcLvl3]			INTEGER NULL,
	[nAbandonAcdSvcLvl4]			INTEGER NULL,
	[nAbandonAcdSvcLvl5]			INTEGER NULL,
	[nAbandonAcdSvcLvl6]			INTEGER NULL,
	[tGrabbedAcd] 					INTEGER NULL,
	[tAnsweredAcd]					INTEGER NULL,
	[mtAnsweredAcd]					INTEGER NULL,
	[tAbandonedAcd]					INTEGER NULL,
	[tTalkAcd]						INTEGER NULL,
	[tTalkCompleteAcd]				INTEGER NULL,
	[nHoldAcd]						INTEGER NULL,
	[tHoldAcd]						INTEGER NULL,
	[nSuspendedAcd]					INTEGER NULL,
	[tSuspendedAcd]					INTEGER NULL,
	[nHeldSpanAcd]					INTEGER NULL,
	[nAcw]							INTEGER NULL,
	[tAcw]							INTEGER NULL,
	[tAcwComplete]					INTEGER NULL,
	[nExternToInternCalls] 			INTEGER NULL,
	[nExternToInternAcdCalls] 		INTEGER NULL,
	[nInternToExternCalls] 			INTEGER NULL,
	[nInternToExternAcdCalls] 		INTEGER NULL,
	[nInternToInternCalls] 			INTEGER NULL,
	[nInternToInternAcdCalls] 		INTEGER NULL,
	[tExternToInternCalls] 			INTEGER NULL,
	[tExternToInternAcdCalls] 		INTEGER NULL,
	[tInternToExternCalls] 			INTEGER NULL,
	[tInternToExternAcdCalls] 		INTEGER NULL,
	[tInternToInternCalls] 			INTEGER NULL,
	[tInternToInternAcdCalls] 		INTEGER NULL,
	[nAcwCalls] 					INTEGER NULL,
	[tAcwCalls] 					INTEGER NULL,
	[nTransferedAcd] 				INTEGER NULL,
	nConsult						INTEGER NULL,
	nBlindTransferred				INTEGER NULL,
	nConsultTransferred				INTEGER NULL,
	[nNotAnsweredAcd] 				INTEGER NULL,
	[tAlertedAcd] 					INTEGER NULL,
	[nFlowOutAcd] 					INTEGER NULL,
	[tFlowOutAcd] 					INTEGER NULL,
	[nStartWaitAlertAcdCalls] 		INTEGER NULL,
	[nStartActiveAcdCalls] 			INTEGER NULL,
	[nStartHeldAcdCalls] 			INTEGER NULL,
	[nEndWaitAlertAcdCalls] 		INTEGER NULL,
	[nEndActiveAcdCalls] 			INTEGER NULL,
	[nEndHeldAcdCalls] 				INTEGER NULL,
	[nTransferWithinAcdCalls] 		INTEGER NULL,
	[nTransferOutAcdCalls] 			INTEGER NULL,
	[nDisconnectAcd] 				INTEGER NULL,
	[tAgentLoggedIn] 				INTEGER NULL,
	[tAgentAvailable] 				INTEGER NULL,
	[tAgentTalk] 					INTEGER NULL,
	[tAgentOtherBusy] 				INTEGER NULL,
	[tAgentOnAcdCall] 				INTEGER NULL,
	[tAgentOnOtherAcdCall] 			INTEGER NULL,
	[tAgentInAcw] 					INTEGER NULL,
	[tAgentOnNonAcdCall] 			INTEGER NULL,
	[tAgentDnd] 					INTEGER NULL,
	[tAgentNotAvailable]			INTEGER NULL,
	[tAgentAcdLoggedIn] 			INTEGER NULL,
	[tAgentAcdLoggedIn2] 			INTEGER NULL,
	[tAgentStatusDnd] 				INTEGER NULL,
	[tAgentStatusAcw] 				INTEGER NULL,
	[tAgentLoggedInDiluted] 		INTEGER NULL,
	[tStatusGroupFollowup] 			INTEGER NULL,
	[tStatusGroupBreak] 			INTEGER NULL,
	[tStatusGroupTraining] 			INTEGER NULL,
	[CustomValue1] 					INTEGER NULL,
	[CustomValue2] 					INTEGER NULL,
	[CustomValue3] 					INTEGER NULL,
	[CustomValue4] 					INTEGER NULL,
	[CustomValue5] 					INTEGER NULL,
	[CustomValue6] 					INTEGER NULL,
	[nAllInternToExternCalls] 		INTEGER NULL,
	[nAllInternToExternAcdCalls]	INTEGER NULL,
	[tAllInternToExternCalls]		INTEGER NULL,
	[tAllInternToExternAcdCalls]	INTEGER NULL,
	[tAllTalkAcd] 					INTEGER NULL,
	[tAllHoldAcd] 					INTEGER NULL,
	[tAllAgentOnAcdCall] 			INTEGER NULL,
	[nAllInternToInternCalls]		INTEGER NULL,
	[nAllExternToInternCalls] 		INTEGER NULL,
	[nAllExternToInternAcdCalls]	INTEGER NULL,
	[tAllExternToInternCalls] 		INTEGER NULL,
	[tAllExternToInternAcdCalls]	INTEGER NULL,
	[tAllAgentAvailable] 			INTEGER NULL,
	[tAllAgentInAcw] 				INTEGER NULL,
	[tAllAgentStatusDnd] 			INTEGER NULL,
	[tAllAgentLoggedIn] 			INTEGER NULL,
	[tAllAgentAcdLoggedIn2] 		INTEGER NULL,
	[tAllAgentAcdLoggedIn] 			INTEGER NULL,
	[SiteId] 						SMALLINT NOT NULL,
	[SubSiteId] 					INTEGER NOT NULL,
	[I3TimeStampGMT] 				DATETIME NOT NULL,
	[DimensionSet] 					INTEGER NOT NULL,
	[SummDimensionSet] 				INTEGER NULL,
	[ConfigurationSet] 				INTEGER NOT NULL,
	[nServiceLevel] 				INTEGER NOT NULL,
	[cServiceLevels] 				VARCHAR(1) NULL,
	[SchemaMajorVersion] 			TINYINT NOT NULL,
	[SchemaMinorVersion] 			TINYINT NOT NULL
) ON [PRIMARY]
CREATE CLUSTERED INDEX idx_IAgentQueueStats ON gen.IAgentQueueStats ([dIntervalStartUTC], [cName]); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.[InteractionSummary];
CREATE TABLE [gen].[InteractionSummary](
	[InteractionIDKey] 				VARCHAR(50) NOT NULL,		--changed FROM char(18)
	[SiteID] 						SMALLINT NOT NULL,
	[SeqNo] 						TINYINT NOT NULL,
	[InteractionID] 				VARCHAR(50) NULL,		--changed FROM BIGINT
	[StartDateTimeUTC] 				DATETIME2(3) NOT NULL,
	[StartDTOffset] 				INTEGER NOT NULL,
	[Direction] 					TINYINT NOT NULL,
	[ConnectionType] 				TINYINT NOT NULL,
	[MediaType] 					TINYINT NOT NULL,
	[RemoteID] 						nVARCHAR(255) NULL,		--changed FROM nVARCHAR(50)
	[DNIS_LocalID] 					nVARCHAR(255) NULL,
	[tDialing] 						INTEGER NULL,
	[tIVRWait] 						INTEGER NULL,
	[tQueueWait] 					NUMERIC(19, 0) NULL,
	[tAlert] 						INTEGER NULL,
	[tConnected] 					NUMERIC(19, 0) NULL,
	[tHeld] 						NUMERIC(19, 0) NULL,
	[tSuspend] 						NUMERIC(19, 0) NULL,
	[tConference] 					NUMERIC(19, 0) NULL,
	[tExternal] 					NUMERIC(19, 0) NULL,
	[tACW] 							INTEGER NULL,
	[nIVR] 							SMALLINT NULL,
	[nQueueWait] 					SMALLINT NULL,
	[nTalk] 						SMALLINT NULL,
	[nConference] 					SMALLINT NULL,
	[nHeld] 						SMALLINT NULL,
	[nTransfer] 					SMALLINT NULL,
	[nExternal] 					SMALLINT NULL,
	[Disposition] 					TINYINT NOT NULL,
	[DispositionCode] 				TINYINT NULL,
	[WrapUpCode] 					nVARCHAR(200) NULL,
	[AccountCode] 					nVARCHAR(50) NULL,
	[IsRecorded] 					[bit] NOT NULL,
	[IsSurveyed] 					[bit] NOT NULL,
	[MediaServerID] 				nVARCHAR(100) NULL,
	[IndivID] 						CHAR(50) NULL,			--changed FROM char(22)
	[OrgID] 						CHAR(22) NULL,
	[LineId] 						nVARCHAR(50) NULL,
	[LastStationId] 				nVARCHAR(255) NULL,		--changed FROM nVARCHAR(50)
	[LastLocalUserId] 				nVARCHAR(50) NULL,
	[LastAssignedWorkgroupID] 		nVARCHAR(100) NULL,
	[LastLocalNumber] 				VARCHAR(200) NULL,
	[LastLocalName] 				nVARCHAR(50) NULL,
	[RemoteICUserID] 				nVARCHAR(255) NULL,		--changed FROM nVARCHAR(50)
	[RemoteNumberCountry] 			SMALLINT NULL,
	[RemoteNumberLoComp1] 			VARCHAR(10) NULL,
	[RemoteNumberLoComp2] 			VARCHAR(10) NULL,
	[RemoteNumberFmt] 				VARCHAR(50) NULL,
	[RemoteNumberCallId] 			VARCHAR(50) NULL,
	[RemoteName] 					nVARCHAR(50) NULL,
	[InitiatedDateTimeUTC] 			DATETIME2(3) NOT NULL,
	[ConnectedDateTimeUTC] 			DATETIME2(3) NOT NULL,
	[TerminatedDateTimeUTC] 		DATETIME2(3) NOT NULL,
	[LineDuration] 					NUMERIC(19, 0) NULL,
	[CallEventLog] 					nVARCHAR(2000) NOT NULL,
	[PurposeCode] 					INTEGER NULL,
	[CallNote] 						nVARCHAR(1024) NULL,
	[nSecuredIVR] 					SMALLINT NOT NULL,
	[tSecuredIVR] 					NUMERIC(19, 0) NULL,
	[FirstAssignedAcdSkillSet] 		nVARCHAR(100) NULL,
	[nPark] 						SMALLINT NULL,
	[tPark] 						NUMERIC(19, 0) NULL,
	CONSTRAINT [PK_InteractionSummary] PRIMARY KEY NONCLUSTERED 
	(
		[InteractionIDKey] ASC,
		[SiteID] ASC,
		[SeqNo] ASC
	) ON [PRIMARY]
) ON [PRIMARY]


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.[ScoringDetail_viw];
CREATE TABLE [gen].[ScoringDetail_viw](
	[RecordingId]					[uniqueidentifier] NOT NULL,
	[RecordingDate]					DATETIME NULL,
	[MediaType]						VARCHAR(15) NULL,
	[InteractionIdKey]				VARCHAR(36) NOT NULL,
	[RecordingLength]				INTEGER NULL,
	[InitiationPolicyName]			nVARCHAR(100) NULL,
	[Direction]						TINYINT NULL,
	[RecordedUserIndivId]			CHAR(50) NULL,
	[RecordedUserLastName]			nVARCHAR(100) NULL,
	[RecordedUserFirstName]			nVARCHAR(100) NULL,
	[RecordedUserMiddleName]		nVARCHAR(100) NULL,
	[RecordedUserDisplayName]		nVARCHAR(510) NULL,
	[RecordedUserICUserId]			nVARCHAR(100) NULL,
	[WorkgroupQueue]				nVARCHAR(510) NULL,
	[QuestionnaireId]				[uniqueidentifier] NOT NULL,
	[QuestionnaireName]				nVARCHAR(510) NOT NULL,
	[QuestionnaireNote]				nVARCHAR(2048) NULL,
	[QuestionnaireMinScore] 		NUMERIC(18, 0) NULL,
	[QuestionnaireMaxScore] 		NUMERIC(18, 0) NULL,
	[QDirectoryName] 				nVARCHAR(510) NULL,
	[QGroupId] 						[uniqueidentifier] NOT NULL,
	[QGroupSequence] 				INTEGER NOT NULL,
	[QGroupName] 					nVARCHAR(510) NOT NULL,
	[QGroupNote] 					nVARCHAR(2048) NULL,
	[IsOptional] 					TINYINT NOT NULL,
	[QuestionId]					[uniqueidentifier] NOT NULL,
	[QuestionSequence]				INTEGER NOT NULL,
	[QuestionText]					nVARCHAR(2048) NOT NULL,
	[QuestionType]					SMALLINT NOT NULL,
	[QuestionPromptType] 			SMALLINT NOT NULL,
	[QuestionWeight] 				NUMERIC(18, 0) NOT NULL,
	[QuestionMin] 					NUMERIC(18, 0) NOT NULL,
	[QuestionMax] 					NUMERIC(18, 0) NOT NULL,
	[QuestionMinAcceptableScore]	NUMERIC(18, 0) NOT NULL,
	[QuestionCanMarkNA] 			TINYINT NOT NULL,
	[FormId] 						[uniqueidentifier] NOT NULL,
	[ScoredUserLastName] 			nVARCHAR(100) NULL,
	[ScoredUserFirstName] 			nVARCHAR(100) NULL,
	[ScoredUserMiddleName] 			nVARCHAR(100) NULL,
	[ScoredUserICUserId] 			nVARCHAR(100) NULL,
	[ScoredUserIndivID] 			CHAR(50) NULL,
	[ScoringUserLastName] 			nVARCHAR(100) NULL,
	[ScoringUserFirstName] 			nVARCHAR(100) NULL,
	[ScoringUserMiddleName] 		nVARCHAR(100) NULL,
	[ScoringUserICUserId] 			nVARCHAR(100) NULL,
	[ScoringUserIndivId] 			CHAR(50) NULL,
	[CombinedMaxScore] 				NUMERIC(18, 0) NULL,
	[CombinedMinScore] 				NUMERIC(18, 0) NULL,
	[CombinedScore]					NUMERIC(18, 0) NOT NULL,
	[CombinedPercentileScore]		NUMERIC(18, 0) NULL,
	[RankName]						nVARCHAR(80) NULL,
	[NonCriticalMaxScore] 			NUMERIC(18, 0) NULL,
	[NonCriticalMinScore] 			NUMERIC(18, 0) NULL,
	[NonCriticalScore] 				NUMERIC(18, 0) NULL,
	[NonCriticalPercentileScore]	NUMERIC(18, 0) NULL,
	[TotalNumCriticalQuestions]		INTEGER NOT NULL,
	[NumAnsweredCriticalQstns]		INTEGER NULL,
	[NumPosAnsweredCriticalQstns]	INTEGER NULL,
	[CriticalScore]					NUMERIC(18, 0) NULL,
	[IsFailed]						TINYINT NULL,
	[UsingForCalibration]			TINYINT NOT NULL,
	[AnswerScore]					NUMERIC(18, 0) NULL,
	[RawAnswer]						nVARCHAR(2048) NULL,
	[DisplayAnswer]					nVARCHAR(510) NULL,
	[UserComments]					nVARCHAR(2048) NULL,
	[IsMarkedNA]					TINYINT NOT NULL,
	PRIMARY KEY ([QuestionnaireId],[QuestionId])
) 
CREATE NONCLUSTERED INDEX idx_ScoringDetail_viw ON gen.ScoringDetail_viw ([RecordingDate]); 
CREATE NONCLUSTERED INDEX idx_ScoringDetail_viwId ON gen.ScoringDetail_viw ([InteractionIdKey]); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.[scoringsummary_viw];
CREATE TABLE [gen].[scoringsummary_viw](
	[RecordingId]					[uniqueidentifier] NOT NULL,
	[RecordingDate]					DATETIME NULL,
	[MediaType]						VARCHAR(15) NULL,
	[InteractionIdKey]				VARCHAR(36) NOT NULL,
	[RecordingLength]				INTEGER NULL,
	[InitiationPolicyName]			nVARCHAR(100) NULL,
	[Direction]						TINYINT NULL,
	[RecordedUserIndivId]			CHAR(50) NULL,
	[RecordedUserLastName]			nVARCHAR(100) NULL,
	[RecordedUserFirstName]			nVARCHAR(100) NULL,
	[RecordedUserMiddleName]		nVARCHAR(100) NULL,
	[RecordedUserDisplayName]		nVARCHAR(510) NULL,
	[RecordedUserICUserId]			nVARCHAR(100) NULL,
	[WorkgroupQueue]				nVARCHAR(510) NULL,
	[QuestionnaireId]				[uniqueidentifier] NOT NULL,
	[QuestionnaireName] 			nVARCHAR(510) NOT NULL,
	[QuestionnaireNote] 			nVARCHAR(2048) NULL,
	[QDirectoryName] 				nVARCHAR(510) NULL,
	[ScoredUserLastName] 			nVARCHAR(100) NULL,
	[ScoredUserFirstName] 			nVARCHAR(100) NULL,
	[ScoredUserMiddleName] 			nVARCHAR(100) NULL,
	[ScoredUserICUserId] 			nVARCHAR(100) NULL,
	[ScoredUserIndivID] 			CHAR(50) NULL,
	[ScoringUserLastName] 			nVARCHAR(100) NULL,
	[ScoringUserFirstName] 			nVARCHAR(100) NULL,
	[ScoringUserMiddleName] 		nVARCHAR(100) NULL,
	[ScoringUserICUserId] 			nVARCHAR(100) NULL,
	[ScoringUserIndivId] 			CHAR(50) NULL,
	[FormId] 						[uniqueidentifier] NOT NULL,
	[CombinedMaxScore] 				NUMERIC(18, 0) NULL,
	[CombinedMinScore] 				NUMERIC(18, 0) NULL,
	[CombinedScore] 				NUMERIC(18, 0) NOT NULL,
	[CombinedPercentileScore] 		NUMERIC(18, 0) NULL,
	[RankName] 						nVARCHAR(80) NULL,
	[NonCriticalMaxScore] 			NUMERIC(18, 0) NULL,
	[NonCriticalMinScore] 			NUMERIC(18, 0) NULL,
	[NonCriticalScore] 				NUMERIC(18, 0) NULL,
	[NonCriticalPercentileScore]	NUMERIC(18, 0) NULL,
	[TotalNumCriticalQuestions]		INTEGER NOT NULL,
	[NumAnsweredCriticalQstns] 		INTEGER NULL,
	[NumPosAnsweredCriticalQstns]	INTEGER NULL,
	[CriticalScore] 				NUMERIC(18, 0) NULL,
	[IsFailed] 						TINYINT NULL,
	[UsingForCalibration]			TINYINT NOT NULL,
	PRIMARY KEY ([InteractionIdKey],[QuestionnaireId])
) 
CREATE NONCLUSTERED INDEX idx_scoringsummary_viw ON gen.scoringsummary_viw ([RecordingDate],[InteractionIdKey]); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.[vcssSurveyScoringDetail];
CREATE TABLE [gen].[vcssSurveyScoringDetail](
	[SurveyId]						VARCHAR(50) NOT NULL,
	[SurveyName]					nVARCHAR(510) NOT NULL,
	[SurveyNote]					nVARCHAR(510) NULL,
	[SurveyType]					INTEGER NULL,
	[SurveyPublished]				INTEGER NULL,
	[InteractionId]					VARCHAR(50) NOT NULL,		--change FROM VARCHAR(20)
	[RecordingId]					[uniqueidentifier] NULL,
	[RecordingDate] 				DATETIME2(7) NULL,
	[RecordingDateOffset] 			INTEGER NULL,
	[EventDate] 					DATETIME NOT NULL,
	[SurveyParticipant] 			nVARCHAR(256) NULL,
	[ICUserId]						nVARCHAR(100) NOT NULL,
	[LastName] 						nVARCHAR(100) NULL,
	[FirstName] 					nVARCHAR(100) NULL,
	[SurveyFormId] 					[uniqueidentifier] NULL,
	[IsCompleted] 					INTEGER NOT NULL,
	[MinAcceptableScore] 			NUMERIC(18, 0) NULL,
	[MinScore] 						NUMERIC(18, 0) NULL,
	[MaxScore] 						NUMERIC(18, 0) NULL,
	[Score] 						NUMERIC(18, 0) NULL,
	[QuestionId] 					VARCHAR(32) NOT NULL,
	[QuestionSequence] 				INTEGER NULL,
	[QuestionText] 					nVARCHAR(510) NULL,
	[QuestionWeight] 				NUMERIC(18, 0) NOT NULL,
	[NumericScore] 					NUMERIC(18, 0) NULL,
	[AnswerText] 					nVARCHAR(510) NULL,
	[FreeFormAnswerRecording] 		VARCHAR(255) NULL,
	PRIMARY KEY ([InteractionId],[SurveyId],[ICUserId],[QuestionId])
) 
CREATE NONCLUSTERED INDEX idx_vcssSurveyScoringDetail ON gen.vcssSurveyScoringDetail ([EventDate]); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.[vcssSurveyScoringSummary];
CREATE TABLE [gen].[vcssSurveyScoringSummary](
	[SurveyId]						VARCHAR(50) NOT NULL,
	[SurveyName]					nVARCHAR(510) NOT NULL,
	[SurveyNote]					nVARCHAR(510) NULL,
	[SurveyType] 					INTEGER NULL,
	[SurveyPublished] 				INTEGER NULL,
	[InteractionId] 				VARCHAR(50) NOT NULL,		--change FROM VARCHAR(20)
	[RecordingId] 					[uniqueidentifier] NULL,
	[SurveyParticipant] 			nVARCHAR(256) NULL,
	[ICUserId] 						nVARCHAR(100) NOT NULL,
	[LastName] 						nVARCHAR(100) NULL,
	[FirstName] 					nVARCHAR(100) NULL,
	[IsCompleted] 					INTEGER NOT NULL,
	[MinAcceptableScore] 			NUMERIC(18, 0) NULL,
	[MinScore] 						NUMERIC(18, 0) NULL,
	[MaxScore] 						NUMERIC(18, 0) NULL,
	[Score] 						NUMERIC(18, 0) NULL,
	[SurveyFormId] 					[uniqueidentifier] NOT NULL,
	[RecordingDate] 				DATETIME2(7) NULL,
	[RecordingDateOffset] 			INTEGER NULL,
	[EventDate] 					DATETIME NOT NULL,
	PRIMARY KEY ([InteractionId],[SurveyId],[ICUserId])
)
CREATE NONCLUSTERED INDEX idx_vcssSurveyScoringSummary ON gen.vcssSurveyScoringSummary ([EventDate]); 


--------------------------------------------------------------------------------------------------------------------------
-- Secondary Tables FROM transformed FROM Genesys Cloud Tables
--------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS gen.gciTaskTracker;
CREATE TABLE [gen].gciTaskTracker (
	ConversationId					VARCHAR(50) NOT NULL,
	UserId							VARCHAR(50) NOT NULL,
	Category						VARCHAR(50) NOT NULL,
	Task							VARCHAR(50) NOT NULL,
	StartTimeUTC					DATETIME,
	EndTimeUTC						DATETIME,
	tTalk							INTEGER,
	tHeld							INTEGER,
	tAcw							INTEGER,
	Duration						INTEGER
)
CREATE CLUSTERED INDEX idx_TaskTracker ON gen.gciTaskTracker (StartTimeUTC); 


--------------------------------------------------------------------------------------------------------------------------


DROP TABLE IF EXISTS gen.gciIVRSurveyResults;
CREATE TABLE [gen].gciIVRSurveyResults (
	ConversationId					VARCHAR(50) NOT NULL,
	SurveySessionId					VARCHAR(50) NOT NULL,
	ConversationStartUTC			DATETIME,
	SurveyStartUTC					DATETIME,
	SurveyEndUTC					DATETIME,
	UserId							VARCHAR(50),
	UserName						VARCHAR(50),
	AgentName						VARCHAR(255),
	FlowId							VARCHAR(50),
	FlowName						VARCHAR(255),
	FlowOutcomeId					VARCHAR(255),
	FlowOutcomeName					VARCHAR(255),
	Description						VARCHAR(255),
	PRIMARY KEY (ConversationId,SurveySessionId,UserId,FlowOutcomeId)
)
CREATE NONCLUSTERED INDEX idx_IVRSurveyResults ON gen.gciIVRSurveyResults (ConversationStartUTC, SurveySessionId); 
CREATE NONCLUSTERED INDEX idx_IVRSurveyResultsId ON gen.gciIVRSurveyResults (SurveySessionId); 


DROP TABLE IF EXISTS gen.gciOutboundAttempts;
CREATE TABLE [gen].gciOutboundAttempts (
	CampaignId						VARCHAR(50),
	CampaignName					VARCHAR(100),
	DialingMode						VARCHAR(50),
	ContactListId					VARCHAR(50),
	ContactListName					VARCHAR(100),
	ContactId						VARCHAR(50),	
	ConversationId					VARCHAR(50),
	DNIS							VARCHAR(200),
	CallbackScheduledUTC			DATETIME,
	ConversationStartUTC			DATETIME,
	ConversationEndUTC				DATETIME,
	QueueId							VARCHAR(50),
	QueueName						VARCHAR(100),
	QueueStartUTC					DATETIME,
	QueueEndUTC						DATETIME,
	UserId							VARCHAR(50),
	UserName						VARCHAR(200),
	ANI								VARCHAR(200),
	CustomerStartUTC				DATETIME,
	CustomerEndUTC					DATETIME,
	AgentPreviewStartUTC			DATETIME,
	AgentCallStartUTC				DATETIME,
	AgentCallEndUTC					DATETIME,
	WrapupCodeId					VARCHAR(50),
	WrapupCodeName					VARCHAR(50),
	Contact_UnCallable				INT,
	Number_UnCallable				INT,
	Right_Party_Contact				INT,
	Success							INT,
	DispositionName					VARCHAR(100),
	nAbandon						INTEGER,
	tAcd							INTEGER,
	tPreview						INTEGER,
	tTalk							INTEGER,
	tHeld							INTEGER,
	tAcw							INTEGER,
	tDialing						INTEGER,
	tAlert							INTEGER,
	tNotResponding					INTEGER,
	nOutboundAbandoned				INTEGER,
	nOutboundAttempted				INTEGER,
	nOutboundConnected				INTEGER,
	nError							INTEGER,
)
CREATE CLUSTERED INDEX idx_gciOutboundAttempts ON gen.gciOutboundAttempts (ConversationStartUTC); 





