CREATE OR ALTER PROCEDURE gen.sp_gciIVRSurveyResults 
	@procStartDate	DATETIME,
	@procEndDate		DATETIME
AS

delete from gen.gciIVRSurveyResults where ConversationStartUTC between @procStartDate and @procEndDate;

insert into gen.gciIVRSurveyResults

select 
	sess.ConversationId						AS ConversationId,
	sess.SessionId							AS SessionId,
	sess.ConversationStartUTC				AS ConversationStartUTC,
	sess.StartTimeUTC						AS SurveyStartUTC,
	sess.EndTimeUTC							AS SurveyEndUTC,
	part.UserId								AS UserId,
	COALESCE(us.UserName,'USER_NOT_FOUND')	AS UserName,
	us.Name									AS AgentName,
	COALESCE(flows.FlowId,'N/A')			AS FlowId,
	COALESCE(flows.FlowName,'N/A')			AS FlowName,
	COALESCE(outc.FlowOutcomeId,'N/A')		AS FlowOutcomeId,
	COALESCE(outc.FlowOutcomeName,'N/A')	AS FlowOutcomeName,
	COALESCE(outc.Description,'N/A')		AS Description
FROM gen.gcConversationSessions sess
	LEFT JOIN gen.gcConversationFlowOutcomes coutc on coutc.SessionId = sess.SessionId
	LEFT JOIN gen.gcFlowOutcomes outc on outc.FlowOutcomeId = coutc.FlowOutcomeId
	LEFT JOIN gen.gcFlows flows on flows.FlowId = sess.FlowId
	LEFT JOIN gen.gcConversations conv on conv.ConversationId = sess.ConversationId
	LEFT JOIN gen.gcConversationRecordings rec on rec.ConversationId = conv.ConversationId
	LEFT JOIN (
		SELECT 
			DISTINCT ConversationId, UserId
		FROM gen.gcConversationParticipants 
		WHERE 1=1
			and ConversationStartUTC between @procStartDate and @procEndDate
			and Purpose = 'Agent'
	) part on part.ConversationId = conv.ConversationId
	LEFT JOIN gen.gcUsers us on us.UserId = part.UserId
	LEFT JOIN (
		select 
			coutc.SessionId, 
			COUNT(coutc.SessionId) as NoOfAnswers,
			SUM(CASE WHEN outc.Description like '%Rating%' then SUBSTRING(outc.Description, 4, 1) else 0 END) as [Score]
		FROM gen.gcConversationFlowOutcomes coutc 
			LEFT JOIN gen.gcFlowOutcomes outc on outc.FlowOutcomeId = coutc.FlowOutcomeId
		WHERE coutc.FlowOutcomeStartTimestamp between @procStartDate and @procEndDate
		group by coutc.SessionId
	) tot on tot.SessionId = sess.SessionId
where 1=1
	and sess.ConversationStartUTC between @procStartDate and @procEndDate
	and sess.FlowName like 'IVR_Survey%'
	and not(part.UserId is null or part.UserId = '')

;